// The main package of the snot executable.
//
package main

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Note: Subcommands will register themselves with the subcmd package during init(). No direct
// access to the packages is needed, they just have to be loaded.

import (

   "fmt"
   "os"

   "snot/cmd/snot/common"

   "snot/lib/cleaner"
   "snot/lib/multierror"
   "snot/lib/subcmd"

_  "snot/cmd/snot/create"
_  "snot/cmd/snot/delete"
_  "snot/cmd/snot/help"
_  "snot/cmd/snot/list"
_  "snot/cmd/snot/restore"
_  "snot/cmd/snot/version"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Time of build. Set by a linker option in the Makefile. Unused at the moment.
//
var buildTime string

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The main() function of the snot executable.
//
func main () {

   exitCode, err := subcmd.Instance ().Run (common.ReadConfig, os.Args [1:] ...)

   if err != nil {

      if m, ok := err.(multierror.MultiError) ; ok && len (m) > 1 {
         fmt.Fprintf (os.Stderr, "%s\n%s\n", common.MultipleErrors (len (m)), err)
      } else {
         fmt.Fprintf (os.Stderr, "Error: %s\n", err)
      }

   }

   if exitCode < 0 {
      exitCode = - exitCode
   }

   cleaner.Instance ().Exit (exitCode)

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————