// The list package implements the 'snot -v' subcommand.
//
// Maybe we should do this differently, but it works…
//
package version

//—————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "fmt"

   "snot/cmd/snot/common"

   "snot/lib/subcmd"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

const name    = "-v"          // This "subcommand".

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The init() function registers the subcommand and its options with the subcmd package.
//
func init () {

   if err := subcmd.Instance ().Add (name, main, nil) ; err != nil {
      panic (err)
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The main() function is the subcommand's main handler.
//
func main () (int, error) {

   fmt.Printf ("snot     Version: %s     Licensed under the GPL version 3.\n", common.Version)
   fmt.Printf ("Copyright © 2018-2019 Stefan Göbel < snot ʇɐ subtype ˙ de >\n")

   if common.BuildTime != "" {
      fmt.Printf ("This version of snot has been built at %s.\n", common.BuildTime)
   }

   return 0, nil

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————