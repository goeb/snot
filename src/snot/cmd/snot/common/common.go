// Common functions and constants used by the subcommands.
//
package common

//—————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "bufio"
   "fmt"
   "io/ioutil"
   "math/rand"
   "os"
   "path/filepath"
   "strconv"
   "strings"
   "syscall"
   "time"
   "unsafe"

   "snot/cmd/snot/common/options"

   "snot/lib/btrfs"
   "snot/lib/fs"
   "snot/lib/cleaner"
   "snot/lib/multierror"
   "snot/lib/snapshots"
   "snot/lib/snapshots/fsfinder"

)

import (

   bff   "snot/lib/btrfs/finder"
   fsf   "snot/lib/snapshots/metadata/filestorage/factory"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

const (

   // These constants are used by ListSnapshot(…).

   idDateLen = len ("[12345678]  2002/02/20 02:20:02  ")
   idOnlyLen = len ("[12345678]  "                     )
   moreSpace = 2

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// In case we need some (pseudo-)random numbers.
//
var rnd *rand.Rand = rand.New (rand.NewSource (time.Now ().UnixNano ()))

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The ListSnapshot(…) function prints some of the the specified snapshot's data to STDOUT.
//
// Creation time will be listed as local time.
//
// Note: This is probably not 100% unicode safe, combining characters or whatever other nasty stuff
// there is in the description may not be printed perfectly if the terminal is too small.
//
func ListSnapshot (snap *snapshots.Snapshot) {

   data := snap.Data ()

   descr := data.Description
   if descr == "" {
      descr = "-"
   }

   format := "[%s]  %s  %s\n"

   w, _ := TTYSize ()

   if w > 0 {

      descWidth := w - idDateLen - moreSpace                      // Maximum description width.

      if w < (idDateLen + moreSpace) {                            // Terminal is too small, so…
         format    = "[%s]  %s\n            %s\n"                 // … split the line.
         descWidth = w - idOnlyLen - moreSpace                    // … modify the maximum width.
      }

      if len ([] rune (descr)) > descWidth + moreSpace {          // Cut description if too long.
         descr = string ([] rune (descr) [:descWidth]) + "…"
      }

   }

   fmt.Printf (format, data.ID, data.Created.Local ().Format ("2006/01/02 15:04:05"), descr)

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The MountAll(…) function mounts all specified filesystems.
//
// Every filesystem in the filesystems list (the first parameter) will be mounted to a temporary
// directory with the mount options specified as second parameter (this list may be empty or nil if
// no mount options are required). The mount points will be created in the temporary directory
// specified by the third parameter (tempDir), which may be an empty string to use the system
// default (usually "/tmp"). The temporary mount points will start with the prefix specified as
// fourth parameter (tempPrefix), followed by a random part. If the fifth parameter (cleaner) is
// not nil, MountAll(…) will register cleanup handlers with this instance to unmount the
// filesystems and delete the temporary directories. If it is nil the caller has to perform the
// cleanup itself, i.e. all filesystems will stay mounted unless the caller takes care of it. The
// last parameter indicates whether to immediately return with an error in case something goes
// wrong (if false) or to continue and try to mount the remaining filesystems (if any).
//
// MountAll(…) will return nil on success. If ignoreErrors is true, and something went wrong, the
// function will return a MultiError instance at the end, if ignoreErrors is false it may return a
// MultiError error, or another error type, depending on where the error occurs. In case mounting
// fails the temporary directory created as mount point will be removed immediately.
//
func MountAll (

   filesystems    [] fs.MountableFS,
   options        [] string,
   tempDir           string,
   tempPrefix        string,
   cleaner        *  cleaner.Cleaner,
   ignoreErrors      bool,

) error {

   mErr := multierror.New ()

   for _, filesystem := range filesystems {

      filesystem  := filesystem
      suffix      := ""
      tmpdir, err := ioutil.TempDir (tempDir, tempPrefix)

      if err != nil {
         if ignoreErrors {
            mErr.Append (err)
            continue
         } else {
            return err
         }
      }

      if cleaner != nil {
         handler := func () {
            if err := os.Remove (tmpdir) ; err != nil {
               fmt.Fprintf (os.Stderr, "Error removing temporary directory %s.\n%v\n", tmpdir, err)
            }
         }
         for ! cleaner.Register ("rmdir " + tmpdir + suffix, handler) {
            suffix = strconv.FormatUint (rnd.Uint64 (), 36)
         }
      }

      mntdir := filepath.Join (tmpdir, "mnt")
      if err := os.Mkdir (mntdir, 0700) ; err != nil {
         if ignoreErrors {
            mErr.Append (err)
            continue
         } else {
            return err
         }
      }

      if cleaner != nil {
         handler := func () {
            if err := os.Remove (mntdir) ; err != nil {
               fmt.Fprintf (os.Stderr, "Error removing mount point %s.\n%v\n", mntdir, err)
            }
         }
         for ! cleaner.Register ("rmdir " + mntdir + suffix, handler) {
            suffix = strconv.FormatUint (rnd.Uint64 (), 36)
         }
      }

      if err := filesystem.Mount (mntdir, options ...) ; err != nil {

         mErr.Append (err)

         if err := os.Remove (mntdir) ; err != nil {
            mErr.Append (err)
         } else {
            if cleaner != nil {
               cleaner.Unregister ("rmdir " + mntdir + suffix)
            }
            if err := os.Remove (tmpdir) ; err != nil {
               mErr.Append (err)
            } else {
               if cleaner != nil {
                  cleaner.Unregister ("rmdir " + tmpdir + suffix)
               }
            }
         }

         if ignoreErrors {
            continue
         } else {
            return mErr.Get ()
         }

      }

      if cleaner != nil {
         handler := func () {
            if err := filesystem.Umount () ; err != nil {
               fmt.Fprintf (os.Stderr, "Could not umount %s.\n%v\n", mntdir, err)
            }
         }
         suffix = ""
         for ! cleaner.Register ("umount " + mntdir + suffix, handler) {
            suffix = strconv.FormatUint (rnd.Uint64 (), 36)
         }
      }

   }

   return mErr.Get ()

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The PrepareSnapshottables(…) function will do most of the initialization work required for the
// snapshot operations.
//
// It will register the FSFinders, get a list of Snapshottable filesystems and mount those
// filesystems that require mounting.
//
// The first parameter must be an Options struct containing the command line options' values. The
// second parameter must be a list of prefixes to filter snapshots (if applicable, this should be
// the remaining command line arguments after the options have been parsed), or nil if no filter
// should be installed. The third parameter must be a list of additional options for the
// BtrfsFinder that will be created. BtrfsFinderOptions that are common to all subcommands will be
// set automatically, only those specific to a subcommand have to be added here. This list may be
// nil if no additional options are necessary.
//
// It will return the list of Snapshottable filesystems, an exit code and an error. On success, the
// list will contain the Snapshottables found (it may be empty), the exit code will be 0 and the
// error will be nil. On failure, the list will be nil (unless IgnoreErrors is true), the exit code
// will be set to whatever the application should use as its exit code (also 0 if IgnoreErrors is
// true), and the error will be set to the error(s) encountered. The error may be a MultiError
// instance. Exit codes returned by this function in case of an error are between 1 and 5 (incl.).
//
// Filesystems that require mounting but couldn't be mounted will not be returned (only applicable
// if IgnoreErrors is true, otherwise no list will be returned anyway).
//
func PrepareSnapshottables (

   opts           options.Options,
   prefixes    [] string,
   finderOpts  [] bff.BtrfsFinderOption,

) ([] snapshots.Snapshottable, int, error) {

   // Use a FileStorageFactory to create MetaStorage instances to deal with the snapshot meta data.
   // JSON encoded files are the only supported MetaStorage implementation at the moment. PathUtils
   // will be set to a DefaultPathUtils instance unless overridden, default is fine.

   mStorageFact, err := fsf.NewFileStorageFactory (fsf.WithFileTemplate (opts.MetaJson))

   if err != nil {
      return nil, 1, err
   }

   // The following BtrfsFinderOptions are valid for every subcommand.

   fOpts := [] bff.BtrfsFinderOption {
      bff.WithMetaStorageConfig  (btrfs.MetaStorageConfigSnapshotData),
      bff.WithMetaStorageFactory (mStorageFact                       ),
      bff.WithSnapshotDir        (opts.SnapDir                       ),
   }

   if opts.MountedOnly {
      fOpts = append (fOpts, bff.WithFilesystemFilters (bff.MountedOnlyFilter ("/proc/mounts")))
   }

   // Snapshot filters.

   ssFilters := make ([] btrfs.SnapshotFilter, 0)

   // If the prefix list is not nil or empty, add the prefix filter. Note that no prefix filter
   // differs from a filter using an empty list: with no prefix filter all snapshots will be
   // included, a filter with an empty list would not match any snapshot.

   if prefixes != nil && len (prefixes) > 0 {
      ssFilter, err := btrfs.IncludePrefixFilter (prefixes ...)
      if err != nil {
         return nil, 2, err
      }
      ssFilters = append (ssFilters, ssFilter)
   }

   // Add the time based filter if required.

   if opts.From != "" || opts.Until != "" {
      ssFilter, err := btrfs.TimeIntervalFilter (opts.From, opts.Until)
      if err != nil {
         return nil, 2, err
      }
      ssFilters = append (ssFilters, ssFilter)
   }

   // Add all filters to the BtrfsFinderOption list (if any filters are set).

   if len (ssFilters) > 0 {
      fOpts = append (fOpts, bff.WithSnapshotFilters (ssFilters ...))
   }

   // Register the BtrfsFinder with both the common options and the options specified as parameters
   // to this function.

   finder, err := bff.NewBtrfsFinder (append (fOpts, finderOpts ...) ...)

   if err != nil {
      return nil, 3, err
   }

   fsfinder.Registry ().Add ("btrfs", 0, finder)

   // Use all the registered filesystem finders to get a list of Snapshottable filesystems. Also
   // keep track of those filesystems that have to be mounted before any snapshot operation.
   //
   // Note that up to this point errors are fatal and nothing will be done. The FSFinder may return
   // a partial list on error, so if IgnoreErrors is true, we may continue with the filesystems
   // found before the error occured.

   mErr    := multierror.New ()
   mountFS := make ([] fs.MountableFS,          0)
   snapsFS := make ([] snapshots.Snapshottable, 0)

   for _, finder := range fsfinder.Registry ().All () {

      fsystems, err := finder.Get ()

      if err != nil {
         if opts.IgnoreErrors {
            mErr.Append (err)
         } else {
            return nil, 4, err
         }
      }

      for _, fsystem := range fsystems {

         if fsystem.MountRequired () {
            switch fsystem.(type) {
               case fs.MountableFS:
                  mountFS = append (mountFS, fsystem.(fs.MountableFS))
               default:
                  panic ("Non-mountable filesystem requires to be mounted.")
            }
         }

         snapsFS = append (snapsFS, fsystem)

      }

   }

   // Mount those filesystem that need to be mounted.
   //
   // MountAll(…) will register cleanup handlers to unmount and remove the temporary directories if
   // a Cleaner instance is specified. Only set the clr variable if the opts.NoUmount command line
   // option is not set, to keep the filesystems mounted after exit with --no-umount. MountAll(…)
   // may also return a MultiError.

   var clr *cleaner.Cleaner

   if ! opts.NoUmount {
      clr = cleaner.Instance ()
   }

   err = MountAll (
      mountFS,
      strings.Split (opts.MountOptions, ","),
      opts.TempDir,
      opts.TempPrefix,
      clr,
      opts.IgnoreErrors,
   )

   if err != nil {

      mErr.Append (err)

      if ! opts.IgnoreErrors {
         return nil, 5, mErr.Get ()
      }

      // If there was an error during mounting (and IgnoreErrors is set), rebuild the Snapshottable
      // list to remove filesystems that require mounting but couldn't be mounted.

      newSnaps := make ([] snapshots.Snapshottable, 0)

      for _, fsystem := range snapsFS {
         if fsystem.MountRequired () && fsystem.(fs.MountableFS).MountPoint () == "" {
            continue
         }
         newSnaps = append (newSnaps, fsystem)
      }

      snapsFS = newSnaps

   }

   return snapsFS, 0, mErr.Get ()

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The ReadConfig(…) function will read the default command line options for the specified
// subcommand.
//
// The configuration file has to be located at /etc/snot/<subcommand>.conf. It must consist of one
// command line option per line. Options and their arguments must be split into separate lines!
// Empty lines, lines containing only whitespace and lines starting with "#" will be ignored. There
// is no need to quote spaces, one line will always be one argument! Options must start with the
// dashes as on the command line.
//
// This function will only be called for valid subcommands.
//
// A non-existing file will not cause an error, any other error will be returned (in this case the
// list of default options will be nil). On success the default options will be returned (the list
// may be empty), and the error will be nil.
//
func ReadConfig (cmd string) ([] string, error) {

   lines := make ([] string, 0)

   file, err := os.Open (filepath.Join ("/etc/snot", cmd + ".conf"))

   if err != nil {
      if os.IsNotExist (err) {
         return lines, nil
      } else {
         return nil, err
      }
   }

   defer file.Close ()

   scanner := bufio.NewScanner (file)

   for scanner.Scan () {
      line := strings.TrimSpace (scanner.Text ())
      if line == "" || strings.HasPrefix (line, "#") {
         continue
      }
      lines = append (lines, line)
   }

   if err := scanner.Err () ; err != nil {
      return nil, err
   }

   return lines, file.Close ()

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The TTYSize() function returns the size of the terminal.
//
// Return values are terminal width (columns) and height (rows), in that order, or (-1, -1) in case
// of an error.
//
func TTYSize () (int, int) {

   ws := &struct {                                 // struct winsize {
      ws_row      uint16                           //    unsigned short ws_row;
      ws_col      uint16                           //    unsigned short ws_col;
      ws_xpixel   uint16                           //    unsigned short ws_xpixel;   /* unused */
      ws_ypixel   uint16                           //    unsigned short ws_ypixel;   /* unused */
   } {}                                            // };

   retVal, _, _ := syscall.Syscall (
      syscall.SYS_IOCTL,
      uintptr ( syscall.Stdin       ),
      uintptr ( syscall.TIOCGWINSZ  ),
      uintptr ( unsafe.Pointer (ws) ),
   )

   if int (retVal) == -1 {
      return -1, -1
   }

   return int (ws.ws_col), int (ws.ws_row)

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————