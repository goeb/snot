// Common constants.

package common

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Version number of snot.
//
const Version = "0.3"

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Time of building the executable. Set via linker option, see the Makefile.
//
var BuildTime string

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————