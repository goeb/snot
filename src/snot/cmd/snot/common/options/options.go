// The options package defines the command line options used by the subcommands.
//
// This is the place to define any command line option, i.e. the option itself as it's used on the
// command line (e.g. --something), its help/usage message and its default value. All subcommands can
// then refer to the options defined here, keeping names etc. consistent across subcommands.
//
package options

//———————————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "flag"
   "fmt"

)

//———————————————————————————————————————————————————————————————————————————————————————————————————————

// The options are indexed by the following constants:

const ( _ = iota

   Description
   From
   IgnoreErrors
   KeepOrig
   MetaJson
   MountedOnly
   MountOptions
   NoParallel
   NoRestFile
   NoSnapFile
   NoUmount
   RW
   ShowHelp
   ShowVersion
   SnapDir
   SVolDir
   TempDir
   TempPrefix
   Until

)

//———————————————————————————————————————————————————————————————————————————————————————————————————————

// The Options type contains fields for all known command line options. Field names match the constants.
//
type Options struct {

   Description    string
   From           string
   IgnoreErrors   bool
   KeepOrig       bool
   MetaJson       string
   MountedOnly    bool
   MountOptions   string
   NoParallel     bool
   NoRestFile     string
   NoSnapFile     string
   NoUmount       bool
   RW             bool
   ShowHelp       bool
   ShowVersion    bool
   SnapDir        string
   SVolDir        string
   TempDir        string
   TempPrefix     string
   Until          string

}

// The opt type defines a single command line option.
//
type opt struct {

   option         string            // The command line option (long), without leading dashes.
   short          string            // Short (one-letter) option (no leading dash), empty for none.
   description    string            // A description of the option for the help messages.
   defaultValue   interface {}      // The default value of the option.

}

//———————————————————————————————————————————————————————————————————————————————————————————————————————

// The opts map contains all valid command line options (for all subcommands).
//
// The keys are the constants defined above, values are the opt values for the option. Short options are
// unused at the moment (they won't be set, and could be left empty anyway).
//
// Make sure that all options (short and long) are unique!
//
var opts = map [int] opt {

   //    ID               Long option   Short              Help text                  Default value
   // ========          ===============  ===  ===================================   =================
   //
   Description  : opt { "description",   "d", "Snapshot description.",              ""                },
   From         : opt { "from",          "f", "Earliest snapshot date/time.",       ""                },
   IgnoreErrors : opt { "ignore-errors", "i", "Continue if an error occurs.",       false             },
   KeepOrig     : opt { "keep-orig",     "k", "Keep old subvolumes after restore.", false             },
   MetaJson     : opt { "meta-json",     "m", "Meta data file name.",               "<.SD>/snap.json" },
   MountedOnly  : opt { "mounted-only",  "y", "Only process mounted filesystems.",  false             },
   MountOptions : opt { "mount-options", "o", "Additional mount options.",          ""                },
   NoParallel   : opt { "no-parallel",   "p", "Create snapshots sequentially.",     false             },
   NoRestFile   : opt { "norest-file",   "r", "Path of the .norest files.",         "<.LF>.norest"    },
   NoSnapFile   : opt { "nosnap-file",   "n", "Path of the .nosnap files.",         "<.LF>.nosnap"    },
   NoUmount     : opt { "no-umount",     "u", "Do not unmount the filesystems.",    false             },
   RW           : opt { "rw",            "w", "Create writable snapshots.",         false             },
   ShowHelp     : opt { "help",          "h", "Show usage information.",            false             },
   ShowVersion  : opt { "version",       "v", "Show version information.",          false             },
   SnapDir      : opt { "snap-dir",      "s", "Snapshot directory.",                "@snap"           },
   SVolDir      : opt { "svol-dir",      "l", "Subvolume directory.",               "@live"           },
   TempDir      : opt { "temp-dir",      "t", "Temporary directory.",               ""                },
   TempPrefix   : opt { "temp-prefix",   "x", "Temporary mount point prefix.",      "snot."           },
   Until        : opt { "until",         "u", "Latest snapshot date/time.",         ""                },

}

//———————————————————————————————————————————————————————————————————————————————————————————————————————

// The Add(…) function adds a command line option to the specified FlagSet.
//
// The first parameter is the FlagSet the option should be added to. The second parameter specifies the
// option, via the constants defined in this package (Description, MetaJson…). The third parameter must
// be a pointer to the variable that will store the command line option's value, either a pointer to a
// string variable or a pointer to a bool variable.
//
// This function will panic(…) if anything goes wrong, this includes trying to add an unknown option or
// using something other than *string or *bool for the target parameter.
//
func Add (flagset *flag.FlagSet, option int, target interface {}) {

   if opt, ok := opts [option] ; ok {

      if defaultValue, ok := opt.defaultValue.(string) ; ok {
         flagset.StringVar (target.(*string), opt.option, defaultValue, opt.description)
         return
      } else if defaultValue, ok := opt.defaultValue.(bool) ; ok {
         flagset.BoolVar (target.(*bool), opt.option, defaultValue, opt.description)
         return
      }

   }

   panic (fmt.Sprintf ("Could not add command line option %d for command %s.", option, flagset.Name ()))

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=105: ——————————————————————————————————————————