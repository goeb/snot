// Error types for the common package.

package common

//—————————————————————————————————————————————————————————————————————————————————————————————————

import "fmt"

//—————————————————————————————————————————————————————————————————————————————————————————————————

// An InvalidCommandLineError will be returned when the command line is invalid (with valid options
// as fas as FlagSet.Parse(…) is concerned, but something missing, or mutually exclusive options
// used etc.).
//
type InvalidCommandLineError struct {
   Msg string
}

// The Error() function returns the error message.
//
func (err InvalidCommandLineError) Error () string {
   return err.Msg
}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// A simple error type that holds the amount of errors that have occurred.
//
// The error message of this type just contains the number of errors, see Error(). It will also
// return a message for zero errors.
//
type MultipleErrors uint

// The Error() method returns the error message of a MultipleErrors error.
//
func (err MultipleErrors) Error () (string) {

   if err == 0 {
      return "There have been no errors."
   } else if err == 1 {
      return "There has been an error."
   } else {
      return fmt.Sprintf ("There have been %d errors.", err)
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// A MultipleSnapshotsError will be returned when a subcommand requires exactly one snapshot to be
// selected, but multiple snapshots match the selector.
//
type MultipleSnapshotsError struct {}

// The Error() function returns the error message.
//
func (err MultipleSnapshotsError) Error () string {
   return "More than one matching snapshot."
}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// A NoSnapshotsError will be returned when one or more specified snapshots could not be found.
//
type NoSnapshotsError struct {}

// The Error() function returns the error message.
//
func (err NoSnapshotsError) Error () string {
   return "No matching snapshot found."
}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————