// The list package implements the 'snot list' subcommand.
//
package list

//—————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "flag"

   "snot/cmd/snot/common"
   "snot/cmd/snot/common/options"

   "snot/lib/multierror"
   "snot/lib/snapshots"
   "snot/lib/subcmd"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The name of this subcommand.
//
const name = "list"

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The opts variable stores the command line options. See the defaults package for default values.
//
var opts options.Options

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The init() function registers the subcommand and its options with the subcmd package.
//
func init () {

   flags := flag.NewFlagSet (name, flag.ContinueOnError)

   options.Add ( flags,  options.From,          &opts.From         )
   options.Add ( flags,  options.IgnoreErrors,  &opts.IgnoreErrors )
   options.Add ( flags,  options.MetaJson,      &opts.MetaJson     )
   options.Add ( flags,  options.MountedOnly,   &opts.MountedOnly  )
   options.Add ( flags,  options.MountOptions,  &opts.MountOptions )
   options.Add ( flags,  options.NoUmount,      &opts.NoUmount     )
   options.Add ( flags,  options.SnapDir,       &opts.SnapDir      )
   options.Add ( flags,  options.TempDir,       &opts.TempDir      )
   options.Add ( flags,  options.TempPrefix,    &opts.TempPrefix   )
   options.Add ( flags,  options.Until,         &opts.Until        )

   if err := subcmd.Instance ().Add (name, main, flags) ; err != nil {
      panic (err)
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The main() function is the subcommand's main handler.
//
func main () (int, error) {

   mErr := multierror.New ()

   // Get the Snapshottable filesystems.

   snapsFS, exit, err := common.PrepareSnapshottables (opts, subcmd.Instance ().Args (), nil)

   if exit != 0 {
      return exit, err
   } else if err != nil && opts.IgnoreErrors {
      mErr.Append (err)
   }

   // Get the (filtered) list of snapshots from all filesystems and print their basic information.

   snaps, err := snapshots.Snapshots (snapsFS ...)

   if err != nil {
      mErr.Append (err)
      if ! opts.IgnoreErrors {
         return 6, mErr.Get ()
      }
   }

   for _, snap := range snaps {
      common.ListSnapshot (snap)
   }

   return 0, mErr.Get ()

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————