// The list package implements the 'snot -h' subcommand.
//
// Maybe we should do this differently, but it works…
//
package help

//—————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "fmt"

   "snot/lib/subcmd"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// This "subcommand".
//
const name = "-h"

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The init() function registers the subcommand and its options with the subcmd package.
//
func init () {

   if err := subcmd.Instance ().Add (name, main, nil) ; err != nil {
      panic (err)
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The main() function is the subcommand's main handler.
//
func main () (int, error) {

   fmt.Print ("Run `snot <command> --help` to get help for the <command>.\n\n")
   fmt.Print ("Available commands are:\n\n")

   for _, subcmd := range subcmd.Instance ().Registered () {
      if subcmd [0] == '-' {
         continue
      }
      fmt.Println ("   ", subcmd)
   }

   return 0, nil

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————