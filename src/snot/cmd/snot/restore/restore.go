// The restore package implements the 'snot retore' subcommand.
//
package restore

//—————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "flag"

   "snot/cmd/snot/common"
   "snot/cmd/snot/common/options"

   "snot/lib/btrfs"
   "snot/lib/multierror"
   "snot/lib/snapshots"
   "snot/lib/subcmd"

)

import bff "snot/lib/btrfs/finder"

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The name of this subcommand.
//
const name = "restore"

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The opts variable stores the command line options. See the defaults package for default values.
//
var opts options.Options

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The init() function registers the subcommand and its options with the subcmd package.
//
func init () {

   flags := flag.NewFlagSet (name, flag.ContinueOnError)

   options.Add ( flags,  options.IgnoreErrors,  &opts.IgnoreErrors )
   options.Add ( flags,  options.KeepOrig,      &opts.KeepOrig     )
   options.Add ( flags,  options.MetaJson,      &opts.MetaJson     )
   options.Add ( flags,  options.MountedOnly,   &opts.MountedOnly  )
   options.Add ( flags,  options.MountOptions,  &opts.MountOptions )
   options.Add ( flags,  options.NoRestFile,    &opts.NoRestFile   )
   options.Add ( flags,  options.NoUmount,      &opts.NoUmount     )
   options.Add ( flags,  options.SnapDir,       &opts.SnapDir      )
   options.Add ( flags,  options.SVolDir,       &opts.SVolDir      )
   options.Add ( flags,  options.TempDir,       &opts.TempDir      )
   options.Add ( flags,  options.TempPrefix,    &opts.TempPrefix   )

   if err := subcmd.Instance ().Add (name, main, flags) ; err != nil {
      panic (err)
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The main() function is the subcommand's main handler.
//
func main () (int, error) {

   mErr := multierror.New ()

   // Don't do anything if not one snapshot ID has been specified.

   if len (subcmd.Instance ().Args ()) != 1 {
      return 6, common.InvalidCommandLineError { Msg: "One snapshot has to be specified." }
   }

   // Get the Snapshottable filesystems.

   finderOpts := [] bff.BtrfsFinderOption {
      bff.WithKeepLiveSubvolumes (opts.KeepOrig                            ),
      bff.WithLiveDir            (opts.SVolDir                             ),
      bff.WithSubvolumeFilters   (btrfs.ExcludeFileFilter (opts.NoRestFile)),
   }

   snapsFS, exit, err := common.PrepareSnapshottables (
      opts, subcmd.Instance ().Args (), finderOpts,
   )

   if exit != 0 {
      return exit, err
   } else if err != nil && opts.IgnoreErrors {
      mErr.Append (err)
   }

   // Get the (filtered) list of snapshots from all filesystems, abort if more than one matches,
   // or if none could be found, otherwise restore the snapshot.

   snaps, err := snapshots.Snapshots (snapsFS ...)

   if err != nil {
      mErr.Append (err)
      if ! opts.IgnoreErrors {
         return 7, mErr.Get ()
      }
   }

   if len (snaps) == 0 { return 8, mErr.Get (common.NoSnapshotsError       {}) }
   if len (snaps) != 1 { return 9, mErr.Get (common.MultipleSnapshotsError {}) }

   err = snaps [0].Restore (opts.IgnoreErrors, snapshots.PrintErrors ("Error: "))

   if err != nil {
      return 8, mErr.Get (err)
   }

   return 0, mErr.Get ()

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————