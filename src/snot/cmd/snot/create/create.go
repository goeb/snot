// The create package implements the 'snot create' subcommand.
//
package create

//—————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "flag"

   "snot/cmd/snot/common"
   "snot/cmd/snot/common/options"

   "snot/lib/btrfs"
   "snot/lib/multierror"
   "snot/lib/snapshots"
   "snot/lib/subcmd"

)

import bff "snot/lib/btrfs/finder"

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The name of this subcommand.
//
const name = "create"

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The opts variable stores the command line options. See the options package for default values.
//
var opts options.Options

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The init() function registers the subcommand and its options with the subcmd package.
//
func init () {

   flags := flag.NewFlagSet (name, flag.ContinueOnError)

   options.Add ( flags,  options.Description,   &opts.Description  )
   options.Add ( flags,  options.IgnoreErrors,  &opts.IgnoreErrors )
   options.Add ( flags,  options.MetaJson,      &opts.MetaJson     )
   options.Add ( flags,  options.MountedOnly,   &opts.MountedOnly  )
   options.Add ( flags,  options.MountOptions,  &opts.MountOptions )
   options.Add ( flags,  options.NoParallel,    &opts.NoParallel   )
   options.Add ( flags,  options.NoSnapFile,    &opts.NoSnapFile   )
   options.Add ( flags,  options.NoUmount,      &opts.NoUmount     )
   options.Add ( flags,  options.RW,            &opts.RW           )
   options.Add ( flags,  options.SnapDir,       &opts.SnapDir      )
   options.Add ( flags,  options.SVolDir,       &opts.SVolDir      )
   options.Add ( flags,  options.TempDir,       &opts.TempDir      )
   options.Add ( flags,  options.TempPrefix,    &opts.TempPrefix   )

   if err := subcmd.Instance ().Add (name, main, flags) ; err != nil {
      panic (err)
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The main() function is the subcommand's main handler.
//
func main () (int, error) {

   mErr := multierror.New ()

   // Get the Snapshottable filesystems.

   finderOpts := [] bff.BtrfsFinderOption {
      bff.WithLiveDir          (opts.SVolDir                             ),
      bff.WithWritable         (opts.RW                                  ),
      bff.WithSubvolumeFilters (btrfs.ExcludeFileFilter (opts.NoSnapFile)),
   }

   snapsFS, exit, err := common.PrepareSnapshottables (opts, nil, finderOpts)

   if exit != 0 {
      return exit, err
   } else if err != nil && opts.IgnoreErrors {
      mErr.Append (err)
   }

   // Create the snapshot.

   snot, err := snapshots.NewSnapshot (
      snapshots.WithDescriptionTemplate (opts.Description),
      snapshots.WithFilesystems         (snapsFS ...     ),
   )

   if err != nil {
      return 6, mErr.Get (err)
   }

   err = snot.Create (! opts.NoParallel, opts.IgnoreErrors, snapshots.PrintErrors ("Error: "))

   if err != nil {
      return 7, mErr.Get (err)
   }

   // Success. Print the snapshot data (as with `snot list`) for the new snapshot and exit.

   common.ListSnapshot (snot)

   return 0, mErr.Get ()

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————