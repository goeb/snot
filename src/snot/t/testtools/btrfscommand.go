// +build test

// A BtrfsCommand implementation for testing.

package testtools

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "fmt"
   "io/ioutil"
   "math/rand"
   "os"
   "os/exec"
   "path/filepath"
   "time"

   "snot/lib/btrfs/backend/btrfstool/command"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

// The TestBtrfsCommand type implements the BtrfsCommand interface.
//
type TestBtrfsCommand struct {

   FuncName       string            // Function to run in the external process.
   Args           [] string         // Stores the parameters the Cmd(…) method is called with.
   Counter        int               // Count how often the Cmd(…) method is called.
   NonExisting    int               // Whether to create a non-existing command (see Cmd(…)).
   BlockStdout    bool              // Whether to connect STDOUt before anything else does.

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

// Make sure we implement the BtrfsCommand interface (compile time check).
//
var _ command.BtrfsCommand = (*TestBtrfsCommand) (nil)

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

// The Cmd(…) method returns an os/exec.Cmd instance that runs the function set for the TestBtrfsCommand as an external
// process via the already existing test executable.
//
// All supplied parameters will be stored in the Args field of the TestBtrfsCommand.
//
// If the NonExisting property is set to 1, a non-existing command will be used (with a full path), if it is set to 2 a
// non-existing command will be used (with no full path but $PATH set to an empty string). The command will be executed
// with the environment variables LC_ALL=C and MOCK_COMMAND=1 set. Every time Cmd(…) is called the Counter field will
// be incremented.
//
func (tc *TestBtrfsCommand) Cmd (args ... string) *exec.Cmd {

   name := os.Args [0]

   if tc.NonExisting > 0 {
      name = name + ".non.existing"
   }

   path := os.Getenv ("PATH")
   if tc.NonExisting > 1 {
      os.Setenv ("PATH", "")
      name = filepath.Base (name)
   }

   tc.Args    = args
   tc.Counter = tc.Counter + 1

   opts      := [] string { "-test.run=" + tc.FuncName, "--" }
   opts       = append (opts, args ...)

   comm      := exec.Command (name, opts ...)
   comm.Env   = append (os.Environ (), "LC_ALL=C")
   comm.Env   = append (os.Environ (), "MOCK_COMMAND=1")

   if tc.NonExisting > 1 {
      os.Setenv ("PATH", path)
   }

   if tc.BlockStdout {
      comm.Stdout = ioutil.Discard
   }

   return comm

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

// The TestBtrfsCommand_FilesystemShow(…) function generates output like the `btrfs filesystem show` command.
//
// The parameter must be in the same format as the return value of BtrfsTool.GetBtrfsFilesystems(…), this function will
// try to create output that will result in the input parameters when parsed by the BtrfsTool.GetBtrfsFilesystems(…)
// method.
//
// For each filesystem:
//
//    * If the UUID is empty, no Label… line will be printed.
//    * Devices may be omitted to not print the device lines.
//    * Empty label will result in the correct output.
//
func TestBtrfsCommand_FilesystemShow (data [][] string) {

   for _, f := range data {

      // Print the Label… line that starts a filesystem block.
      //
      if f [0] != "" {

         l := "none"

         if f [1] != "" {
            l = "'" + f [1] + "'"
         }

         fmt.Printf ("Label: %s  uuid: %s\n", l, f [0])

      }

      // The summary line.
      //
      fmt.Printf ("\tTotal devices %d FS bytes used 112.00KiB\n", len (f) - 2)

      // Insert an empty line for coverage purposes:
      //
      fmt.Println ()

      // The device lines.
      //
      for j, d := range f [2:] {
         fmt.Printf ("\tdevid %4d size 128.00MiB used 88.00MiB path %s\n", j, d)
      }

   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

// The TestBtrfsCommand_SubvolumeList(…) function generates output like the `btrfs subvolume list` command.
//
// The parameters must be a list os subvolume paths to generate output for, and a boolean value to switch between
// output for snapshot listings (with -o parameter to btrfs, if snapshotsOnly is true) or regular listings (if it is
// false).
//
// Empty lines will be added to the output just to see if they're handled properly. IDs and timestamps will be random.
//
func TestBtrfsCommand_SubvolumeList (data [] string, snapshotsOnly bool) {

   rand.Seed (time.Now ().UnixNano ())

   for _, d := range data {

      i := rand.Int31 ()
      g := rand.Int31 ()
      t := rand.Int31 ()

      if snapshotsOnly {

         o := time.Unix (rand.Int63n (1000000000), rand.Int63 ()).Format ("2006-01-02 15:04:05")
         c := rand.Int31 ()

         fmt.Printf ("ID %d gen %d cgen %d top level %d otime %s path %s\n", i, g, c, t, o, d)
         fmt.Println ()

      } else {

         fmt.Printf ("ID %d gen %d top level %d path %s\n", i, g, t, d)
         fmt.Println ()

      }

   }

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=119: ————————————————————————————————————————————————————————