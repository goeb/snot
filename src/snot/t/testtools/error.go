// +build test

// An error type for testing.

package testtools

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

import "fmt"

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

// An error type for testing.
//
type TestError struct {
   Data interface {}
}

// The Error() method returns the error message.
//
func (err TestError) Error () string {
   return fmt.Sprintf ("%#v", err.Data)
}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=119: ————————————————————————————————————————————————————————