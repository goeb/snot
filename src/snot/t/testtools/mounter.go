// +build test

// A Mounter and MounterFactory implementation for testing.

package testtools

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

import "snot/lib/fs/mounter"

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

// The TestMounter type implements the Mounter interface. Parameters for method calls will be stored in the struct, and
// all methods will return the RetVal value.
//
type TestMounter struct {

   RetVal         error             // Will be returned by Mount(…) and Umount(…).

   Device         string            // Device when Mount(…) was called.
   Path           string            // Path when Mount(…)/Umount(…) was called.
   FSType         string            // Filesystem type when Mount(…) was called.
   MFlags         uint              // Flags when Mount(…) was called.
   UFlags         int               // Flags when Umount(…) was called.
   Options     [] string            // Mount options when Mount(…) was called.

   MountCnt       uint              // Calling Mount(…) will increase this.
   UmountCnt      uint              // Calling Umount(…) will increase this.

}

// Implements the MounterFactory interface.
//
type TestMounterFactory struct {

   Error          error             // Error value to return.
   Counter        int               // Count the Mounter instances this factory instance creates.

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

// Make sure we implement the Mounter and MounterFactory interfaces (compile time check).
//
var _ mounter.Mounter        = (*TestMounter       ) (nil)
var _ mounter.MounterFactory = (*TestMounterFactory) (nil)

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

// Stores all parameters in the struct, clears the UFlags field. Returns the RetVal set in the struct.
//
func (tm *TestMounter) Mount (device, path, fstype string, flags uint, options ... string) error {

   tm.MountCnt ++

   tm.Device   = device
   tm.Path     = path
   tm.FSType   = fstype
   tm.Options  = options
   tm.MFlags   = flags

   tm.UFlags   = 0

   return tm.RetVal

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

// Clears all values currently set in the struct, except the UFlags field which is set to the specified flags, and the
// Path, which is set to the specified path. The RetVal will be left as is and it will be returned.
//
func (tm *TestMounter) Umount (path string, flags int) error {

   tm.UmountCnt ++

   tm.Path     = path
   tm.UFlags   = flags

   tm.Device   = ""
   tm.FSType   = ""
   tm.Options  = nil
   tm.MFlags   = 0

   return tm.RetVal

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

// Get a new TestMounter instance and increase the factory's counter. The fileds will have their default (zero) values.
//
func (tf *TestMounterFactory) NewMounter () (mounter.Mounter, error) {

   tf.Counter ++

   if tf.Error != nil {
      return nil, tf.Error
   } else {
      return &TestMounter {}, nil
   }

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=119: ————————————————————————————————————————————————————————