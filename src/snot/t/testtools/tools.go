// +build test

// A collection of helper functions etc. to be used in the tests.
//
// Note: All the Is*(…) functions return a boolean value, true on success, false if the condition is not satisfied.
// These functions will print the error message, but the execution of the test function will continue. The Assert*(…)
// functions will internally call the appripriate Is*(…) functions (e.g. AssertEq(…) will call IsEq(…)), and fail the
// test if the check fails, aborting the test function.
//
package testtools

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "fmt"
   "path/filepath"
   "reflect"
   "runtime"
   "strings"
   "testing"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

// LogError(…) will be used to log the error message and the source file and line number.
//
func LogError (t *testing.T, message string, args ... interface {}) {

   _, this, _, _ := runtime.Caller (0)
   this = filepath.Base (this)

   pref := ""
   skip := 1
   for {
      if _, file, line, ok := runtime.Caller (skip) ; ok {
         if filepath.Base (file) == this {
            skip ++
            continue
         }
         pref = fmt.Sprintf ("Test failure in file %s line %d: ", filepath.Base (file), line)
      }
      break
   }

   t.Errorf (pref + message, args ...)

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

// IsCn(…) checks if b is contained in the slice a (comparison with reflect.DeepEqual(…)).
//
// The message will be formatted with b, a, followed by the args.
//
// Note: a has to be a slice, and its elements must be of the same type as b.
//
func IsCn (t *testing.T, a, b interface {}, message string, args ... interface {}) bool {

   if reflect.TypeOf (a).Kind () == reflect.Slice {

      s := reflect.ValueOf (a)
      v := reflect.ValueOf (b).Interface ()

      for i := 0 ; i < s.Len () ; i ++ {
         if reflect.DeepEqual (s.Index (i).Interface (), v) {
            return true
         }
      }

   }

   f := [] interface {} { b, a }
   f  = append (f, args ...)

   if message == "" {
      message = "Expected %#v in %#v."
   }

   LogError (t, message, f ...)

   return false

}

// AssertCn(…) stops the execution of the test function if IsCn(…) fails.
//
func AssertCn (t *testing.T, a, b interface {}, message string, args ... interface {}) {

   if ! IsCn (t, a, b, message, args ...) {
      t.FailNow ()
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

// IsEq(…) checks if a and b are equal using reflect.DeepEqual(…).
//
// The message will be formatted with b, a, followed by the args.
//
func IsEq (t *testing.T, a, b interface {}, message string, args ... interface {}) bool {

   if ! reflect.DeepEqual (a, b) {

      f := [] interface {} { b, a }
      f  = append (f, args ...)

      if message == "" {
         message = "Expected %#v, got %#v."
      }

      LogError (t, message, f ...)

      return false

   }

   return true

}

// AssertEq(…) stops the execution of the test function if IsEq(…) fails.
//
func AssertEq (t *testing.T, a, b interface {}, message string, args ... interface {}) {

   if ! IsEq (t, a, b, message, args ...) {
      t.FailNow ()
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

// IsLn(…) checks if the length of a is b. a has to be a slice, a map or a string!
//
// The message will be formatted with b, length of a, followed by the args.
//
func IsLn (t *testing.T, a interface {}, b int, message string, args ... interface {}) bool {

   switch reflect.TypeOf (a).Kind () {

      case reflect.Slice:
         fallthrough

      case reflect.Map:
         fallthrough

      case reflect.String:

         if l := reflect.ValueOf (a).Len () ; l != b {

            f := [] interface {} { b, l }
            f  = append (f, args ...)

            if message == "" {
               message = "Expected length %d, got %d."
            }

            LogError (t, message, f ...)

            return false

         }

   }

   return true

}

// AssertLn(…) stops the execution of the test function if IsLn(…) fails.
//
func AssertLn (t *testing.T, a interface {}, b int, message string, args ... interface {}) {

   if ! IsLn (t, a, b, message, args ...) {
      t.FailNow ()
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

// IsNe(…) checks if a and b are not equal using reflect.DeepEqual(…).
//
// The message will be formatted with a (which will be equal to b), followed be the args.
//
func IsNe (t *testing.T, a, b interface {}, message string, args ... interface {}) bool {

   if reflect.DeepEqual (a, b) {

      f := [] interface {} { a }
      f  = append (f, args ...)

      if message == "" {
         message = "Expected something else than %#v."
      }

      LogError (t, message, f ...)

      return false

   }

   return true

}

// AssertNe(…) stops the execution of the test function if IsNe(…) fails.
//
func AssertNe (t *testing.T, a, b interface {}, message string, args ... interface {}) {

   if ! IsNe (t, a, b, message, args ...) {
      t.FailNow ()
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

// IsNl(…) checks if a is nil.
//
// The message will be formatted with a, followed by the args.
//
func IsNl (t *testing.T, a interface {}, message string, args ... interface {}) bool {

   validKind := map [reflect.Kind] bool {
      reflect.Ptr    : true,
      reflect.Slice  : true,
   }

   v := reflect.ValueOf (a)

   if a == nil || v.IsValid () && validKind [v.Kind ()] && v.IsNil () {
      return true
   }

   f := [] interface {} { a }
   f  = append (f, args ...)

   if message == "" {
      message = "Expected nil, got %#v."
   }

   LogError (t, message, f ...)

   return false

}

// AssertNl(…) stops the execution of the test function if IsNl(…) fails.
//
func AssertNl (t *testing.T, a interface {}, message string, args ... interface {}) {

   if ! IsNl (t, a, message, args ...) {
      t.FailNow ()
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

// IsNn(…) checks if a is not nil.
//
// The message will be formatted with the args.
//
func IsNn (t *testing.T, a interface {}, message string, args ... interface {}) bool {

   validKind := map [reflect.Kind] bool {
      reflect.Ptr : true,
   }

   v := reflect.ValueOf (a)

   if v.IsValid () && validKind [v.Kind ()] && ! v.IsNil () {
      return true
   } else if a != nil {
      return true
   }

   if message == "" {
      message = "Expected something else than nil."
   }

   LogError (t, message, args ...)

   return false

}

// AssertNn(…) stops the execution of the test function if IsNn(…) fails.
//
func AssertNn (t *testing.T, a interface {}, message string, args ... interface {}) {

   if ! IsNn (t, a, message, args ...) {
      t.FailNow ()
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

// IsSs(…) checks if string a contains the substring b.
//
// The message will be formatted with b, a, followed by the args.
//
func IsSs (t *testing.T, a, b string, message string, args ... interface {}) bool {

   if ! strings.Contains (a, b) {

      f := [] interface {} { b, a }
      f  = append (f, args ...)

      if message == "" {
         message = "Expected substring %#v in %#v."
      }

      LogError (t, message, f ...)

      return false

   }

   return true

}

// AssertSs(…) stops the execution of the test function if IsSs(…) fails.
//
func AssertSs (t *testing.T, a, b string, message string, args ... interface {}) {

   if ! IsSs (t, a, b, message, args ...) {
      t.FailNow ()
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

// IsTp checks if the name of the type of a is b (only for pointers and values).
//
// The message will be formatted with b, the name of the type of a, followed by the args.
//
func IsTp (t *testing.T, a interface {}, b string, message string, args ... interface {}) bool {

   p := reflect.TypeOf (a)
   n := ""

   if p.Kind () == reflect.Ptr {
      n = p.Elem ().Name ()
      if n == b {
         return true
      }
   } else {
      n = p.Name ()
      if n == b {
         return true
      }
   }

   f := [] interface {} { b, n }
   f  = append (f, args ...)

   if message == "" {
      message = "Expected type %s, got %s."
   }

   LogError (t, message, f ...)

   return false

}

// AssertTp(…) stops the execution of the test function if IsTp(…) fails.
//
func AssertTp (t *testing.T, a interface {}, b string, message string, args ... interface {}) {

   if ! IsTp (t, a, b, message, args ...) {
      t.FailNow ()
   }

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=119: ————————————————————————————————————————————————————————