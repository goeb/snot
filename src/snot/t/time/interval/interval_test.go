// +build test

// Tests for the time.interval type.
//
package interval

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "os"
   "testing"
   "time"

   "snot/lib/time/interval"

 . "snot/t/testtools"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

type P      struct {    // Strings to parse with ParseDateTime(…):
   dateTime string      //    the string to parse
   parsed   time.Time   //    result with fullPeriod = false
   period   time.Time   //    result with fullPeriod = true
}

type M      struct {    // A time to match against an interval:
   match    time.Time   //    the time to check
   orig     bool        //    result from the match against the [start, stop] interval
   invert   bool        //    result from the match against the [stop, start] interval
}

type I      struct {    // An interval to test:
   start    string      //    start time
   stop     string      //    stop time
   matches  [] M        //    matches to test against the interval
}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

// Just some aliases used below…
//
var L *time.Location = time.UTC
var D func (int, time.Month, int, int, int, int, int, *time.Location) time.Time = time.Date

var parsedDates = [] P {

   P { "2019",                D (2019,  1,  1,  0,  0,  0, 0, L), D (2019, 12, 31, 23, 59, 59, 999999999, L) },
   P { "2019 01",             D (2019,  1,  1,  0,  0,  0, 0, L), D (2019,  1, 31, 23, 59, 59, 999999999, L) },
   P { "2019.01:01",          D (2019,  1,  1,  0,  0,  0, 0, L), D (2019,  1,  1, 23, 59, 59, 999999999, L) },
   P { "2019/01_01-00.00",    D (2019,  1,  1,  0,  0,  0, 0, L), D (2019,  1,  1,  0,  0, 59, 999999999, L) },
   P { "2019:01/01_00-00 00", D (2019,  1,  1,  0,  0,  0, 0, L), D (2019,  1,  1,  0,  0, 00, 999999999, L) },

   P { "2019-02",             D (2019,  2,  1,  0,  0,  0, 0, L), D (2019,  2, 28, 23, 59, 59, 999999999, L) },
   P { "2019 03.04",          D (2019,  3,  4,  0,  0,  0, 0, L), D (2019,  3,  4, 23, 59, 59, 999999999, L) },
   P { "2019:05_06-07 08",    D (2019,  5,  6,  7,  8,  0, 0, L), D (2019,  5,  6,  7,  8, 59, 999999999, L) },
   P { "2019.09:10_11-12 13", D (2019,  9, 10, 11, 12, 13, 0, L), D (2019,  9, 10, 11, 12, 13, 999999999, L) },

   // Leap year (non-leap year checked above):
   //
   P { "2020/02",             D (2020,  2,  1,  0,  0,  0, 0, L), D (2020,  2, 29, 23, 59, 59, 999999999, L) },

   // Zero time.Time:
   //
   P { "",                    D (   1,  1,  1,  0,  0,  0, 0, L), D (   1,  1,  1,  0,  0,  0,         0, L) },

}

var invalidDates = [] string {

   "2019-02-03 04",                 // Missing minutes.
   "2019/09/10_11:12:13 -00:00",    // Too much information.
   "2019+02",                       // Invalid separator.
   "garbage",                       // Not a date.
   "2019-02-29-12-13-14",           // Invalid date.

}

var intervals = [] I {

   I { "2018", "2019",
      [] M {
         M { D (2015,  5, 15, 15, 15, 15, 151515151, L), false, true  },
         M { D (2017, 12, 31, 23, 59, 59, 999999999, L), false, true  },
         M { D (2018,  1,  1,  0,  0,  0,         0, L), true,  true  },
         M { D (2018, 12, 31, 23, 59, 59, 999999999, L), true,  true  },
         M { D (2019, 12, 31, 23, 59, 59, 999999999, L), true,  true  },
         M { D (2020,  1,  1,  0,  0,  0,         0, L), false, true  },
         M { D (2025,  5, 25, 25, 25, 25, 252525252, L), false, true  },
      },
   },

   I { "2019-03", "2019-09",
      [] M {
         M { D (2015,  5, 15, 15, 15, 15, 151515151, L), false, true  },
         M { D (2019,  2, 28, 23, 59, 59, 999999999, L), false, true  },
         M { D (2019,  3,  1,  0,  0,  0,         0, L), true,  true  },
         M { D (2019,  3, 31, 23, 59, 59, 999999999, L), true,  true  },
         M { D (2019,  4,  1,  0,  0,  0,         0, L), true,  false },
         M { D (2019,  6, 15, 12,  0,  0,         0, L), true,  false },
         M { D (2019,  8, 31, 23, 59, 59, 999999999, L), true,  false },
         M { D (2019,  9,  1,  0,  0,  0,         0, L), true,  true  },
         M { D (2019,  9, 30, 23, 59, 59, 999999999, L), true,  true  },
         M { D (2019, 10,  1,  0,  0,  0,         0, L), false, true  },
         M { D (2025,  5, 25, 25, 25, 25, 252525252, L), false, true  },
      },
   },

   I { "", "2019-02-03 04:05:06",
      [] M {
         M { D (2015,  5, 15, 15, 15, 15, 151515151, L), true,  false },
         M { D (2019,  2,  3,  4,  5,  5, 999999999, L), true,  false },
         M { D (2019,  2,  3,  4,  5,  6,         0, L), true,  true  },
         M { D (2019,  2,  3,  4,  5,  6, 999999999, L), true,  true  },
         M { D (2019,  2,  3,  4,  5,  7,         0, L), false, true  },
         M { D (2025,  5, 25, 25, 25, 25, 252525252, L), false, true  },
      },
   },

   I { "2019-12-11", "2019-12-11",
      [] M {
         M { D (2015,  5, 15, 15, 15, 15, 151515151, L), false, false },
         M { D (2019, 12, 10, 23, 59, 59, 999999999, L), false, false },
         M { D (2019, 12, 11,  0,  0,  0,         0, L), true,  true  },
         M { D (2019, 12, 11, 12, 24, 36, 486072849, L), true,  true  },
         M { D (2019, 12, 11, 23, 59, 59, 999999999, L), true,  true  },
         M { D (2019, 12, 12,  0,  0,  0,         0, L), false, false },
         M { D (2025,  5, 25, 25, 25, 25, 252525252, L), false, false },
      },
   },

   I { "", "",
      [] M {
         M { D (2015,  5, 15, 15, 15, 15, 151515151, L), true,  true  },
         M { D (2019,  2, 28, 23, 59, 59, 999999999, L), true,  true  },
         M { D (2019,  3,  1,  0,  0,  0,         0, L), true,  true  },
         M { D (2019,  3, 31, 23, 59, 59, 999999999, L), true,  true  },
         M { D (2019,  4,  1,  0,  0,  0,         0, L), true,  true  },
         M { D (2019,  6, 15, 12,  0,  0,         0, L), true,  true  },
         M { D (2019,  8, 31, 23, 59, 59, 999999999, L), true,  true  },
         M { D (2019,  9,  1,  0,  0,  0,         0, L), true,  true  },
         M { D (2019,  9, 30, 23, 59, 59, 999999999, L), true,  true  },
         M { D (2019, 10,  1,  0,  0,  0,         0, L), true,  true  },
         M { D (2025,  5, 25, 25, 25, 25, 252525252, L), true,  true  },
      },
   },

   I { "2019", "2019-05-06",
      [] M {
         M { D (2018, 12, 31, 23, 59, 59, 999999999, L), false, false },
         M { D (2019,  1,  1,  0,  0,  0,         0, L), true,  false },
         M { D (2019,  5,  6, 23, 59, 59, 999999999, L), true,  true  },
         M { D (2019,  5,  7,  0,  0,  0,         0, L), false, true  },
         M { D (2019, 12, 31, 23, 59, 59, 999999999, L), false, true  },
         M { D (2020,  1,  1,  0,  0,  0,         0, L), false, false },
      },
   },

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func TestMain(m *testing.M) {

   os.Setenv ("TZ", "CEST")
   os.Exit (m.Run ())

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_ParseDateTime (t *testing.T) {

   for _, d := range parsedDates {

      p1, e1 := interval.ParseDateTime (d.dateTime, false)
      p2, e2 := interval.ParseDateTime (d.dateTime, true )

      AssertNl ( t, e1,                        "Expected no error, got %#v."                )
      AssertNl ( t, e2,                        "Expected no error, got %#v."                )

      AssertEq ( t, p1.Equal (d.parsed), true, "Expected %t, got %t for %#v.", p1, d.parsed )
      AssertEq ( t, p2.Equal (d.period), true, "Expected %t, got %t for %#v.", p2, d.period )

   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_ParseDateTime_Error (t *testing.T) {

   for _, d := range invalidDates {

      p1, e1 := interval.ParseDateTime (d, false)
      p2, e2 := interval.ParseDateTime (d, true )

      AssertNn ( t, e1,                         "Expected error, got nil."                     )
      AssertNn ( t, e2,                         "Expected error, got nil."                     )

      AssertTp ( t, e1,           "ParseError", "Expected %s, got %s."                         )
      AssertTp ( t, e2,           "ParseError", "Expected %s, got %s."                         )

      AssertSs ( t, e1.Error (),  d,            "Expected substring %s in %s."                 )
      AssertSs ( t, e2.Error (),  d,            "Expected substring %s in %s."                 )

      AssertEq ( t, p1.IsZero (), true,         "Expected zero time = %t, got %t for %#v.", p1 )
      AssertEq ( t, p2.IsZero (), true,         "Expected zero time = %t, got %t for %#v.", p2 )

   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_Interval (t *testing.T) {

   for _, in := range intervals {

      i, e1 := interval.New (in.start, in.stop)
      j, e2 := interval.New (in.stop, in.start)

      AssertNl ( t, e1,             "Expected no error, got %#v." )
      AssertNl ( t, e2,             "Expected no error, got %#v." )

      AssertTp ( t, i,  "Interval", "Expected %s, got %s."        )
      AssertTp ( t, j,  "Interval", "Expected %s, got %s."        )

      if in.start != in.stop {
         AssertNe ( t, i, j, "Expected different intervals." )
      } else {
         AssertEq ( t, i, j, "Expected %#v, got %#v."        )
      }

      for _, m := range in.matches {
         AssertEq ( t, i.Match (m.match), m.orig,   "Expected %s, got %s for %#v.", m.match )
         AssertEq ( t, j.Match (m.match), m.invert, "Expected %s, got %s for %#v.", m.match )
      }

   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_Interval_Error (t *testing.T) {

   z := interval.Interval {}

   for _, d := range invalidDates {

      i, e1 := interval.New ("2019", d)
      j, e2 := interval.New (d, "2019")

      AssertNn ( t, e1,                         "Expected error, got nil."             )
      AssertNn ( t, e2,                         "Expected error, got nil."             )

      AssertTp ( t, e1,           "ParseError", "Expected %s, got %s."                 )
      AssertTp ( t, e2,           "ParseError", "Expected %s, got %s."                 )

      AssertSs ( t, e1.Error (),  d,            "Expected substring %s in %s."         )
      AssertSs ( t, e2.Error (),  d,            "Expected substring %s in %s."         )

      AssertEq ( t, i,            z,            "Expected zero interval %#v, got %#v." )
      AssertEq ( t, j,            z,            "Expected zero interval %#v, got %#v." )

   }

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=119: ————————————————————————————————————————————————————————