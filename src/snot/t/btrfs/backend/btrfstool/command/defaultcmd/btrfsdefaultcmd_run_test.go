// +build testall

// Additional tests for the BtrfsDefaultCommand type. These tests require the `btrfs` executable to be available in the
// $PATH.

package defaultcmd

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "os/exec"
   "testing"

   "snot/lib/btrfs/backend/btrfstool/command/defaultcmd"

 . "snot/t/testtools"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsDefaultCommand_Default_Version (t *testing.T) {

   if _, e := exec.LookPath ("btrfs") ; e != nil {
      t.Skip ("btrfs not found in $PATH.")
   }

   b, e := defaultcmd.NewBtrfsDefaultCommand ()

   AssertNl ( t, e,                        "Expected nil, got %#v."                 )
   AssertNn ( t, b,                        "Expected BtrfsDefaultCommand, got nil." )
   AssertTp ( t, b, "BtrfsDefaultCommand", "Expected %s, got %s."                   )

   c := b.Cmd ("--version")

   AssertNl ( t, e,        "Expected nil, got %#v."      )
   AssertNn ( t, c,        "Expected exec.Cmd, got nil." )
   AssertTp ( t, c, "Cmd", "Expected %s, got %s."        )

   s, e := c.CombinedOutput ()

   AssertNl ( t, e,                           "Expected nil, got %#v."    )
   AssertSs ( t, string (s), "btrfs-progs v", "Expected %s in output %s." )

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsDefaultCommand_FullPath_Version (t *testing.T) {

   p, e := exec.LookPath ("btrfs")

   if e != nil {
      t.Skip ("btrfs not found in $PATH.")
   }

   b, e := defaultcmd.NewBtrfsDefaultCommand (defaultcmd.WithBtrfsPath (p))

   AssertNl ( t, e,                        "Expected nil, got %#v."                 )
   AssertNn ( t, b,                        "Expected BtrfsDefaultCommand, got nil." )
   AssertTp ( t, b, "BtrfsDefaultCommand", "Expected %s, got %s."                   )

   c := b.Cmd ("--version")

   AssertNl ( t, e,             "Expected nil, got %#v."      )
   AssertNn ( t, c,             "Expected exec.Cmd, got nil." )
   AssertTp ( t, c,      "Cmd", "Expected %s, got %s."        )
   AssertEq ( t, c.Path, p,     "Expected %s, got %s."        )

   s, e := c.CombinedOutput ()

   AssertNl ( t, e,                           "Expected nil, got %#v."    )
   AssertSs ( t, string (s), "btrfs-progs v", "Expected %s in output %s." )

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsDefaultCommand_NameOnly_Version (t *testing.T) {

   if _, e := exec.LookPath ("btrfs") ; e != nil {
      t.Skip ("btrfs not found in $PATH.")
   }

   b, e := defaultcmd.NewBtrfsDefaultCommand (defaultcmd.WithBtrfsPath ("btrfs"))

   AssertNl ( t, e,                        "Expected nil, got %#v."                 )
   AssertNn ( t, b,                        "Expected BtrfsDefaultCommand, got nil." )
   AssertTp ( t, b, "BtrfsDefaultCommand", "Expected %s, got %s."                   )

   c := b.Cmd ("--version")

   AssertNl ( t, e,        "Expected nil, got %#v."      )
   AssertNn ( t, c,        "Expected exec.Cmd, got nil." )
   AssertTp ( t, c, "Cmd", "Expected %s, got %s."        )

   s, e := c.CombinedOutput ()

   AssertNl ( t, e,                           "Expected nil, got %#v."    )
   AssertSs ( t, string (s), "btrfs-progs v", "Expected %s in output %s." )

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=119: ————————————————————————————————————————————————————————