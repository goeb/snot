// +build test

// Tests for the btrfs.BtrfsDefaultCmd type.
//
package defaultcmd

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "testing"

   "snot/lib/btrfs/backend/btrfstool/command"
   "snot/lib/btrfs/backend/btrfstool/command/defaultcmd"

 . "snot/t/testtools"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

var _ command.BtrfsCommand = (*defaultcmd.BtrfsDefaultCommand) (nil)

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_Btrfs_NewBtrfsDefaultCommand_Default (t *testing.T) {

   b, e := defaultcmd.NewBtrfsDefaultCommand ()

   AssertNl ( t, e,                        "Expected nil, got %#v."                 )
   AssertNn ( t, b,                        "Expected BtrfsDefaultCommand, got nil." )
   AssertTp ( t, b, "BtrfsDefaultCommand", "Expected %s, got %s."                   )

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_Btrfs_NewBtrfsDefaultCommand_Basic (t *testing.T) {

   b, e := defaultcmd.NewBtrfsDefaultCommand (nil, nil, defaultcmd.WithBtrfsPath ("btrfs"), nil)

   AssertNl ( t, e,                        "Expected nil, got %#v."                 )
   AssertNn ( t, b,                        "Expected BtrfsDefaultCommand, got nil." )
   AssertTp ( t, b, "BtrfsDefaultCommand", "Expected %s, got %s."                   )

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_Btrfs_NewBtrfsDefaultCommand_Error (t *testing.T) {

   o := func (*defaultcmd.BtrfsDefaultCommand) error {
      return TestError { "Error in constructor." }
   }

   b, e := defaultcmd.NewBtrfsDefaultCommand (o)

   AssertNl ( t, b,              "Expected nil, got %#v."   )
   AssertNn ( t, e,              "Expected error, got nil." )
   AssertTp ( t, e, "TestError", "Expected %s, got %s."     )

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsDefaultCommand_Cmd (t *testing.T) {

   b, e := defaultcmd.NewBtrfsDefaultCommand ()

   AssertNl ( t, e,                        "Expected nil, got %#v."                 )
   AssertNn ( t, b,                        "Expected BtrfsDefaultCommand, got nil." )
   AssertTp ( t, b, "BtrfsDefaultCommand", "Expected %s, got %s."                   )

   c := b.Cmd ()

   AssertNn ( t, c,                  "Expected exec.Cmd, got nil."       )
   AssertTp ( t, c,      "Cmd",      "Expected %s, got %s."              )
   AssertSs ( t, c.Path, "btrfs",    "Expected %s, got %s."              )
   AssertCn ( t, c.Env,  "LC_ALL=C", "Expected %s to be in %#v."         )

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=119: ————————————————————————————————————————————————————————