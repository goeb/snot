// +build test

// Tests for the btrfstool.BtrfsTool type.

package btrfstool

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "os"
   "testing"

   "snot/lib/btrfs/backend/btrfstool"

 . "snot/t/testtools"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsTool_GetBtrfsSubvolumes_Empty (t *testing.T) {

   if os.Getenv ("MOCK_COMMAND") == "1" {
      os.Exit (0)
   }

   c := &TestBtrfsCommand { FuncName : "Test_BtrfsTool_GetBtrfsSubvolumes_Empty" }

   b, e := btrfstool.NewBtrfsTool (btrfstool.WithBtrfsCommand (c))

   AssertNl ( t, e,              "Expected no error, got %#v."  )
   AssertNn ( t, b,              "Expected BtrfsTool, got nil." )
   AssertTp ( t, b, "BtrfsTool", "Expected %s, got %s."         )

   s, e := b.GetBtrfsSubvolumes ("path", false)

   AssertNl ( t, e,                                                             "Expected no error, got %#v."       )
   AssertNn ( t, s,                                                             "Expected subvolume list, got nil." )
   AssertLn ( t, s,                          0,                                 "Expected %d filesystems, got %d."  )
   AssertEq ( t, c.Counter,                  1,                                 "Expected %d calls, got %d."        )
   AssertEq ( t, c.Args [0:2],               [] string { "subvolume", "list" }, "Expected parameters %#v, got %#v." )
   AssertEq ( t, c.Args [len (c.Args) - 2:], [] string { "--", "path"        }, "Expected parameters %#v, got %#v." )

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsTool_GetBtrfsSubvolumes_Empty_Snapshots (t *testing.T) {

   if os.Getenv ("MOCK_COMMAND") == "1" {
      os.Exit (0)
   }

   c := &TestBtrfsCommand { FuncName : "Test_BtrfsTool_GetBtrfsSubvolumes_Empty_Snapshots" }

   b, e := btrfstool.NewBtrfsTool (btrfstool.WithBtrfsCommand (c))

   AssertNl ( t, e,              "Expected no error, got %#v."  )
   AssertNn ( t, b,              "Expected BtrfsTool, got nil." )
   AssertTp ( t, b, "BtrfsTool", "Expected %s, got %s."         )

   s, e := b.GetBtrfsSubvolumes ("path", true)

   AssertNl ( t, e,                                                             "Expected no error, got %#v."       )
   AssertNn ( t, s,                                                             "Expected subvolume list, got nil." )
   AssertLn ( t, s,         0,                                                  "Expected %d filesystems, got %d."  )
   AssertEq ( t, c.Counter, 1,                                                  "Expected %d calls, got %d."        )
   AssertEq ( t, c.Args [0:2],               [] string { "subvolume", "list" }, "Expected parameters %#v, got %#v." )
   AssertEq ( t, c.Args [len (c.Args) - 2:], [] string { "--", "path"        }, "Expected parameters %#v, got %#v." )
   AssertCn ( t, c.Args,                     "-s",                              "Expected %s in parameters."        )

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsTool_GetBtrfsSubvolumes_ErrorExit (t *testing.T) {

   d := [] string { "sub/volume/1", "sub/volume/2", "sub/volume/3", "sub/volume/4" }

   if os.Getenv ("MOCK_COMMAND") == "1" {
      TestBtrfsCommand_SubvolumeList (d, false)
      os.Exit (1)
   }

   c := &TestBtrfsCommand { FuncName : "Test_BtrfsTool_GetBtrfsSubvolumes_ErrorExit" }

   b, e := btrfstool.NewBtrfsTool (btrfstool.WithBtrfsCommand (c))

   AssertNl ( t, e,              "Expected no error, got %#v."  )
   AssertNn ( t, b,              "Expected BtrfsTool, got nil." )
   AssertTp ( t, b, "BtrfsTool", "Expected %s, got %s."         )

   s, e := b.GetBtrfsSubvolumes ("path", false)

   AssertNn ( t, e,              "Expected an error, got nil."       )
   AssertTp ( t, e, "ExitError", "Expected %s, got %s."              )
   AssertNn ( t, s,              "Expected subvolume list, got nil." )
   AssertLn ( t, s, len (d),     "Expected %d subvolumes, got %d."   )

   for i, s := range s {
      AssertEq ( t, s, d [i], "Expected subvolume %s, got %s." )
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsTool_GetBtrfsSubvolumes_StdoutError (t *testing.T) {

   d := [] string { "sub/volume/1", "sub/volume/2", "sub/volume/3", "sub/volume/4" }

   if os.Getenv ("MOCK_COMMAND") == "1" {
      TestBtrfsCommand_SubvolumeList (d, true)
      os.Exit (0)
   }

   c := &TestBtrfsCommand { FuncName : "Test_BtrfsTool_GetBtrfsSubvolumes_StdoutError", BlockStdout : true }

   b, e := btrfstool.NewBtrfsTool (btrfstool.WithBtrfsCommand (c))

   AssertNl ( t, e,              "Expected no error, got %#v."  )
   AssertNn ( t, b,              "Expected BtrfsTool, got nil." )
   AssertTp ( t, b, "BtrfsTool", "Expected %s, got %s."         )

   s, e := b.GetBtrfsSubvolumes ("path", true)

   AssertNl ( t, s,                "Expected subvolume list to be nil, got %#v." )
   AssertNn ( t, e,                "Expected an error, got nil."                 )
   AssertTp ( t, e, "errorString", "Expected %s, got %s."                        )

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsTool_GetBtrfsSubvolumes_NoCommand (t *testing.T) {

   c := &TestBtrfsCommand { FuncName : "Test_BtrfsTool_GetBtrfsSubvolumes_NoCommand", NonExisting : 1 }

   b, e := btrfstool.NewBtrfsTool (btrfstool.WithBtrfsCommand (c))

   AssertNl ( t, e,              "Expected no error, got %#v."  )
   AssertNn ( t, b,              "Expected BtrfsTool, got nil." )
   AssertTp ( t, b, "BtrfsTool", "Expected %s, got %s."         )

   s, e := b.GetBtrfsSubvolumes ("path", false)

   AssertNl ( t, s,                      "Expected filesystem list to be nil, got %#v." )
   AssertNn ( t, e,                      "Expected an error, got nil."                  )
   AssertTp ( t, e,         "PathError", "Expected %s, got %s."                         )
   AssertEq ( t, c.Counter, 1,           "Expected %d command calls, got %d."           )

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsTool_GetBtrfsSubvolumes_CommandNotFound (t *testing.T) {

   c := &TestBtrfsCommand { FuncName : "Test_BtrfsTool_GetBtrfsSubvolumes_CommandNotFound", NonExisting : 2 }

   b, e := btrfstool.NewBtrfsTool (btrfstool.WithBtrfsCommand (c))

   AssertNl ( t, e,              "Expected no error, got %#v."  )
   AssertNn ( t, b,              "Expected BtrfsTool, got nil." )
   AssertTp ( t, b, "BtrfsTool", "Expected %s, got %s."         )

   s, e := b.GetBtrfsSubvolumes ("path", true)

   AssertNl ( t, s,                  "Expected filesystem list to be nil, got %#v." )
   AssertNn ( t, e,                  "Expected an error, got nil."                  )
   AssertTp ( t, e,         "Error", "Expected %s, got %s."                         )
   AssertEq ( t, c.Counter, 1,       "Expected %d command calls, got %d."           )

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsTool_GetBtrfsSubvolumes_Success (t *testing.T) {

   d := [] string { "sub/volume/1", "sub/volume/2", "sub/volume/3", "sub/volume/4" }

   if os.Getenv ("MOCK_COMMAND") == "1" {
      TestBtrfsCommand_SubvolumeList (d, false)
      os.Exit (0)
   }

   c := &TestBtrfsCommand { FuncName : "Test_BtrfsTool_GetBtrfsSubvolumes_Success" }

   b, e := btrfstool.NewBtrfsTool (btrfstool.WithBtrfsCommand (c))

   AssertNl ( t, e,              "Expected no error, got %#v."  )
   AssertNn ( t, b,              "Expected BtrfsTool, got nil." )
   AssertTp ( t, b, "BtrfsTool", "Expected %s, got %s."         )

   s, e := b.GetBtrfsSubvolumes ("path", false)

   AssertNl ( t, e,              "Expected no error, got %#v."       )
   AssertNn ( t, s,              "Expected subvolume list, got nil." )
   AssertLn ( t, s, len (d),     "Expected %d subvolumes, got %d."   )

   for i, s := range s {
      AssertEq ( t, s, d [i], "Expected subvolume %s, got %s." )
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsTool_GetBtrfsSubvolumes_Success_Snapshots (t *testing.T) {

   d := [] string { "sub/volume/1", "sub/volume/2", "sub/volume/3", "sub/volume/4" }

   if os.Getenv ("MOCK_COMMAND") == "1" {
      TestBtrfsCommand_SubvolumeList (d, true)
      os.Exit (0)
   }

   c := &TestBtrfsCommand { FuncName : "Test_BtrfsTool_GetBtrfsSubvolumes_Success_Snapshots" }

   b, e := btrfstool.NewBtrfsTool (btrfstool.WithBtrfsCommand (c))

   AssertNl ( t, e,              "Expected no error, got %#v."  )
   AssertNn ( t, b,              "Expected BtrfsTool, got nil." )
   AssertTp ( t, b, "BtrfsTool", "Expected %s, got %s."         )

   s, e := b.GetBtrfsSubvolumes ("path", true)

   AssertNl ( t, e,          "Expected no error, got %#v."       )
   AssertNn ( t, s,          "Expected subvolume list, got nil." )
   AssertLn ( t, s, len (d), "Expected %d subvolumes, got %d."   )

   for i, s := range s {
      AssertEq ( t, s, d [i], "Expected subvolume %s, got %s." )
   }

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=119: ————————————————————————————————————————————————————————