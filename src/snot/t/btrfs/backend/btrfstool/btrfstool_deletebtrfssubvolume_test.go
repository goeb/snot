// +build test

// Tests for the btrfstool.BtrfsTool type.

package btrfstool

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "os"
   "testing"

   "snot/lib/btrfs/backend/btrfstool"

 . "snot/t/testtools"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsTool_DeleteBtrfsSubvolume_ErrorExit (t *testing.T) {

   if os.Getenv ("MOCK_COMMAND") == "1" {
      os.Exit (1)
   }

   c := &TestBtrfsCommand { FuncName : "Test_BtrfsTool_DeleteBtrfsSubvolume_ErrorExit" }

   b, e := btrfstool.NewBtrfsTool (btrfstool.WithBtrfsCommand (c))

   AssertNl ( t, e,              "Expected no error, got %#v."  )
   AssertNn ( t, b,              "Expected BtrfsTool, got nil." )
   AssertTp ( t, b, "BtrfsTool", "Expected %s, got %s."         )

   e = b.DeleteBtrfsSubvolume ("/some/path")

   AssertNn ( t, e,              "Expected an error, got nil." )
   AssertTp ( t, e, "ExitError", "Expected an error, got nil." )

   AssertEq ( t, c.Args [0:2],               [] string { "subvolume", "delete" }, "Expected parameters %#v, got %#v." )
   AssertEq ( t, c.Args [len (c.Args) - 2:], [] string { "--", "/some/path"    }, "Expected parameters %#v, got %#v." )

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsTool_DeleteBtrfsSubvolume_NoCommand (t *testing.T) {

   c := &TestBtrfsCommand { FuncName : "Test_BtrfsTool_DeleteBtrfsSubvolume_NoCommand", NonExisting : 1 }

   b, e := btrfstool.NewBtrfsTool (btrfstool.WithBtrfsCommand (c))

   AssertNl ( t, e,              "Expected no error, got %#v."  )
   AssertNn ( t, b,              "Expected BtrfsTool, got nil." )
   AssertTp ( t, b, "BtrfsTool", "Expected %s, got %s."         )

   e = b.DeleteBtrfsSubvolume ("/some/path")

   AssertNn ( t, e,                      "Expected an error, got nil."        )
   AssertTp ( t, e,         "PathError", "Expected %s, got %s."               )
   AssertEq ( t, c.Counter, 1,           "Expected %d command calls, got %d." )

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsTool_DeleteBtrfsSubvolume_CommandNotFound (t *testing.T) {

   c := &TestBtrfsCommand { FuncName : "Test_BtrfsTool_DeleteBtrfsSubvolume_CommandNotFound", NonExisting : 2 }

   b, e := btrfstool.NewBtrfsTool (btrfstool.WithBtrfsCommand (c))

   AssertNl ( t, e,              "Expected no error, got %#v."  )
   AssertNn ( t, b,              "Expected BtrfsTool, got nil." )
   AssertTp ( t, b, "BtrfsTool", "Expected %s, got %s."         )

   e = b.DeleteBtrfsSubvolume ("/some/path")

   AssertNn ( t, e,                  "Expected an error, got nil."        )
   AssertTp ( t, e,         "Error", "Expected %s, got %s."               )
   AssertEq ( t, c.Counter, 1,       "Expected %d command calls, got %d." )

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsTool_DeleteBtrfsSubvolume_Success (t *testing.T) {

   if os.Getenv ("MOCK_COMMAND") == "1" {
      os.Exit (0)
   }

   c := &TestBtrfsCommand { FuncName : "Test_BtrfsTool_DeleteBtrfsSubvolume_Success" }

   b, e := btrfstool.NewBtrfsTool (btrfstool.WithBtrfsCommand (c))

   AssertNl ( t, e,              "Expected no error, got %#v."  )
   AssertNn ( t, b,              "Expected BtrfsTool, got nil." )
   AssertTp ( t, b, "BtrfsTool", "Expected %s, got %s."         )

   e = b.DeleteBtrfsSubvolume ("/some/path")

   AssertNl ( t, e, "Expected no error, got %#v." )

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=119: ————————————————————————————————————————————————————————