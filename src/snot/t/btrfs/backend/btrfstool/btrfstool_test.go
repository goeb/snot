// +build test

// Tests for the btrfstool.BtrfsTool type.
//
package btrfstool

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "testing"

   "snot/lib/btrfs/backend"
   "snot/lib/btrfs/backend/btrfstool"

 . "snot/t/testtools"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

var _ backend.BtrfsBackend = (*btrfstool.BtrfsTool) (nil)

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsTool_NewBtrfsTool_NoParameters (t *testing.T) {

   b, e := btrfstool.NewBtrfsTool ()

   AssertNl ( t, e,              "Expected no error, got %#v."  )
   AssertNn ( t, b,              "Expected BtrfsTool, got nil." )
   AssertTp ( t, b, "BtrfsTool", "Expected %s, got %s."         )

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsTool_NewBtrfsTool_NilParameters (t *testing.T) {

   b, e := btrfstool.NewBtrfsTool (nil, nil)

   AssertNl ( t, e,              "Expected no error, got %#v."  )
   AssertNn ( t, b,              "Expected BtrfsTool, got nil." )
   AssertTp ( t, b, "BtrfsTool", "Expected %s, got %s."         )

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsTool_NewBtrfsTool_ErrorParameters (t *testing.T) {

   o := func (*btrfstool.BtrfsTool) error {
      return TestError { "Error in constructor." }
   }

   b, e := btrfstool.NewBtrfsTool (o)

   AssertNl ( t, b,              "Expected nil, got %#v."      )
   AssertNn ( t, e,              "Expected an error, got nil." )
   AssertTp ( t, e, "TestError", "Expected %s, got %s."        )

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsTool_NewBtrfsTool_WithBtrfsCommand (t *testing.T) {

   b, e := btrfstool.NewBtrfsTool (btrfstool.WithBtrfsCommand (&TestBtrfsCommand {}))

   AssertNl ( t, e,              "Expected no error, got %#v."  )
   AssertNn ( t, b,              "Expected BtrfsTool, got nil." )
   AssertTp ( t, b, "BtrfsTool", "Expected %s, got %s."         )

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsTool_NewBtrfsTool_WithBtrfsCommand_Nil (t *testing.T) {

   b, e := btrfstool.NewBtrfsTool (btrfstool.WithBtrfsCommand (nil))

   AssertNl ( t, e,              "Expected no error, got %#v."  )
   AssertNn ( t, b,              "Expected BtrfsTool, got nil." )
   AssertTp ( t, b, "BtrfsTool", "Expected %s, got %s."         )

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=119: ————————————————————————————————————————————————————————