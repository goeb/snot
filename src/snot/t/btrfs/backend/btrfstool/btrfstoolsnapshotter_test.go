// +build test

// Tests for the btrfstool.BtrfsTool type.

package btrfstool

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "os"
   "testing"

   "snot/lib/btrfs/backend"
   "snot/lib/btrfs/backend/btrfstool"

 . "snot/t/testtools"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

var _ backend.BtrfsSnapshotter = (*btrfstool.BtrfsToolSnapshotter) (nil)

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsToolSnapshotter (t *testing.T) {

   b, e := btrfstool.NewBtrfsTool ()

   AssertNl ( t, e,              "Expected no error, got %#v."  )
   AssertNn ( t, b,              "Expected BtrfsTool, got nil." )
   AssertTp ( t, b, "BtrfsTool", "Expected %s, got %s."         )

   s, e := b.GetSnapshotter ("source", "target", true)

   AssertNl ( t, e,                         "Expected no error, got %#v."             )
   AssertNn ( t, s,                         "Expected BtrfsToolSnapshotter, got nil." )
   AssertTp ( t, s, "BtrfsToolSnapshotter", "Expected %s, got %s."                    )

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsToolSnapshotter_RW (t *testing.T) {

   if os.Getenv ("MOCK_COMMAND") == "1" {
      os.Exit (0)
   }

   c := &TestBtrfsCommand { FuncName : "Test_BtrfsToolSnapshotter_RW" }

   b, e := btrfstool.NewBtrfsTool (btrfstool.WithBtrfsCommand (c))

   AssertNl ( t, e,              "Expected no error, got %#v."  )
   AssertNn ( t, b,              "Expected BtrfsTool, got nil." )
   AssertTp ( t, b, "BtrfsTool", "Expected %s, got %s."         )

   src := "/sou/rce"
   tgt := "/tar/get"

   s, e := b.GetSnapshotter (src, tgt, true)

   AssertNl ( t, e,                         "Expected no error, got %#v."             )
   AssertNn ( t, s,                         "Expected BtrfsToolSnapshotter, got nil." )
   AssertTp ( t, s, "BtrfsToolSnapshotter", "Expected %s, got %s."                    )

   e = s.Prepare ()

   AssertNl ( t, e,                                                                 "Expected no error, got %#v." )
   AssertEq ( t, c.Args [0:2],               [] string { "subvolume", "snapshot" }, "Expected %v, got %v."        )
   AssertEq ( t, c.Args [len (c.Args) - 3:], [] string { "--", src, tgt },          "Expected %v, got %v."        )

   e = s.Snapshot ()

   AssertNl ( t, e, "Expected no error, got %#v." )

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsToolSnapshotter_RO (t *testing.T) {

   if os.Getenv ("MOCK_COMMAND") == "1" {
      os.Exit (0)
   }

   c := &TestBtrfsCommand { FuncName : "Test_BtrfsToolSnapshotter_RO" }

   b, e := btrfstool.NewBtrfsTool (btrfstool.WithBtrfsCommand (c))

   AssertNl ( t, e,              "Expected no error, got %#v."  )
   AssertNn ( t, b,              "Expected BtrfsTool, got nil." )
   AssertTp ( t, b, "BtrfsTool", "Expected %s, got %s."         )

   src := "/sou/rce"
   tgt := "/tar/get"

   s, e := b.GetSnapshotter (src, tgt, false)

   AssertNl ( t, e,                         "Expected no error, got %#v."             )
   AssertNn ( t, s,                         "Expected BtrfsToolSnapshotter, got nil." )
   AssertTp ( t, s, "BtrfsToolSnapshotter", "Expected %s, got %s."                    )

   e = s.Prepare ()

   AssertNl ( t, e,                                                                 "Expected no error, got %#v." )
   AssertEq ( t, c.Args [0:2],               [] string { "subvolume", "snapshot" }, "Expected %v, got %v."        )
   AssertEq ( t, c.Args [len (c.Args) - 3:], [] string { "--", src, tgt },          "Expected %v, got %v."        )
   AssertCn ( t, c.Args,                     "-r",                                  "Expected %s in parameters."  )

   e = s.Snapshot ()

   AssertNl ( t, e, "Expected no error, got %#v." )

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsToolSnapshotter_ErrorExit (t *testing.T) {

   if os.Getenv ("MOCK_COMMAND") == "1" {
      os.Exit (1)
   }

   c := &TestBtrfsCommand { FuncName : "Test_BtrfsToolSnapshotter_ErrorExit" }

   b, e := btrfstool.NewBtrfsTool (btrfstool.WithBtrfsCommand (c))

   AssertNl ( t, e,              "Expected no error, got %#v."  )
   AssertNn ( t, b,              "Expected BtrfsTool, got nil." )
   AssertTp ( t, b, "BtrfsTool", "Expected %s, got %s."         )

   src := "/sou/rce"
   tgt := "/tar/get"

   s, e := b.GetSnapshotter (src, tgt, false)

   AssertNl ( t, e,                         "Expected no error, got %#v."             )
   AssertNn ( t, s,                         "Expected BtrfsToolSnapshotter, got nil." )
   AssertTp ( t, s, "BtrfsToolSnapshotter", "Expected %s, got %s."                    )

   e = s.Prepare ()

   AssertNl ( t, e,                                                                 "Expected no error, got %#v." )
   AssertEq ( t, c.Args [0:2],               [] string { "subvolume", "snapshot" }, "Expected %v, got %v."        )
   AssertEq ( t, c.Args [len (c.Args) - 3:], [] string { "--", src, tgt },          "Expected %v, got %v."        )
   AssertCn ( t, c.Args,                     "-r",                                  "Expected %s in parameters."  )

   e = s.Snapshot ()

   AssertNn ( t, e,              "Expected an error, got nil." )
   AssertTp ( t, e, "ExitError", "Expected an error, got nil." )

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsToolSnapshotter_NoCommand (t *testing.T) {

   c := &TestBtrfsCommand { FuncName : "Test_BtrfsToolSnapshotter_ErrorExit", NonExisting : 1 }

   b, e := btrfstool.NewBtrfsTool (btrfstool.WithBtrfsCommand (c))

   AssertNl ( t, e,              "Expected no error, got %#v."  )
   AssertNn ( t, b,              "Expected BtrfsTool, got nil." )
   AssertTp ( t, b, "BtrfsTool", "Expected %s, got %s."         )

   src := "/sou/rce"
   tgt := "/tar/get"

   s, e := b.GetSnapshotter (src, tgt, true)

   AssertNl ( t, e,                         "Expected no error, got %#v."             )
   AssertNn ( t, s,                         "Expected BtrfsToolSnapshotter, got nil." )
   AssertTp ( t, s, "BtrfsToolSnapshotter", "Expected %s, got %s."                    )

   e = s.Prepare ()

   AssertNl ( t, e,                                                                 "Expected no error, got %#v." )
   AssertEq ( t, c.Args [0:2],               [] string { "subvolume", "snapshot" }, "Expected %v, got %v."        )

   AssertEq ( t, c.Args [len (c.Args) - 3:], [] string { "--", src, tgt },          "Expected %v, got %v."        )

   e = s.Snapshot ()

   AssertNn ( t, e,              "Expected an error, got nil." )
   AssertTp ( t, e, "PathError", "Expected error %s, got %s."  )

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsToolSnapshotter_CommandNotFound (t *testing.T) {

   c := &TestBtrfsCommand { FuncName : "Test_BtrfsToolSnapshotter_ErrorExit", NonExisting : 2 }

   b, e := btrfstool.NewBtrfsTool (btrfstool.WithBtrfsCommand (c))

   AssertNl ( t, e,              "Expected no error, got %#v."  )
   AssertNn ( t, b,              "Expected BtrfsTool, got nil." )
   AssertTp ( t, b, "BtrfsTool", "Expected %s, got %s."         )

   src := "/sou/rce"
   tgt := "/tar/get"

   s, e := b.GetSnapshotter (src, tgt, false)

   AssertNl ( t, e,                         "Expected no error, got %#v."             )
   AssertNn ( t, s,                         "Expected BtrfsToolSnapshotter, got nil." )
   AssertTp ( t, s, "BtrfsToolSnapshotter", "Expected %s, got %s."                    )

   e = s.Prepare ()

   AssertNl ( t, e,                                                                 "Expected no error, got %#v." )
   AssertEq ( t, c.Args [0:2],               [] string { "subvolume", "snapshot" }, "Expected %v, got %v."        )
   AssertEq ( t, c.Args [len (c.Args) - 3:], [] string { "--", src, tgt },          "Expected %v, got %v."        )
   AssertCn ( t, c.Args,                     "-r",                                  "Expected %s in parameters."  )

   e = s.Snapshot ()

   AssertNn ( t, e,          "Expected an error, got nil." )
   AssertTp ( t, e, "Error", "Expected error %s, got %s."  )

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=119: ————————————————————————————————————————————————————————