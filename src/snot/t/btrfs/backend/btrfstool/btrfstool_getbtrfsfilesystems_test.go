// +build test

// Tests for the btrfstool.BtrfsTool type.

package btrfstool

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "fmt"
   "os"
   "testing"

   "snot/lib/btrfs/backend/btrfstool"

 . "snot/t/testtools"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsTool_GetBtrfsFilesystems_Empty (t *testing.T) {

   if os.Getenv ("MOCK_COMMAND") == "1" {
      os.Exit (0)
   }

   c := &TestBtrfsCommand { FuncName : "Test_BtrfsTool_GetBtrfsFilesystems_Empty" }

   b, e := btrfstool.NewBtrfsTool (btrfstool.WithBtrfsCommand (c))

   AssertNl ( t, e,              "Expected no error, got %#v."  )
   AssertNn ( t, b,              "Expected BtrfsTool, got nil." )
   AssertTp ( t, b, "BtrfsTool", "Expected %s, got %s."         )

   f, e := b.GetBtrfsFilesystems ()

   AssertNl ( t, e,                                             "Expected no error, got %#v."        )
   AssertNn ( t, f,                                             "Expected filesystem list, got nil." )
   AssertLn ( t, f,         0,                                  "Expected %d filesystems, got %d."   )
   AssertEq ( t, c.Counter, 1,                                  "Expected %d command calls, got %d." )
   AssertEq ( t, c.Args,    [] string { "filesystem", "show" }, "Expected parameters %#v, got %#v."  )

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsTool_GetBtrfsFilesystems_ErrorExit (t *testing.T) {

   d := [][] string {
      [] string { "890abcde-f123-4567-890a-bcdef1234567", "label1", "/dev/dev1"                           },
      [] string { "890abcde-f123-4567-890a-bcdef1234567", "none",   "/dev/dev2", "/dev/dev3"              },
      [] string { "890abcde-f123-4567-890a-bcdef1234567", "",       "/dev/dev4", "/dev/dev5", "/dev/dev6" },
      [] string { "890abcde-f123-4567-890a-bcdef1234567", "'test'", "/dev/dev7"                           },
   }

   if os.Getenv ("MOCK_COMMAND") == "1" {
      TestBtrfsCommand_FilesystemShow (d)
      os.Exit (1)
   }

   c := &TestBtrfsCommand { FuncName : "Test_BtrfsTool_GetBtrfsFilesystems_ErrorExit" }

   b, e := btrfstool.NewBtrfsTool (btrfstool.WithBtrfsCommand (c))

   AssertNl ( t, e,              "Expected no error, got %#v."  )
   AssertNn ( t, b,              "Expected BtrfsTool, got nil." )
   AssertTp ( t, b, "BtrfsTool", "Expected %s, got %s."         )

   f, e := b.GetBtrfsFilesystems ()

   AssertNn ( t, f,                                             "Expected filesystem list, got nil." )
   AssertNn ( t, e,                                             "Expected an error, got nil."                  )
   AssertTp ( t, e,         "ExitError",                        "Expected %s, got %s."                         )
   AssertLn ( t, f,         len (d),                            "Expected %d filesystems, got %d."   )
   AssertEq ( t, c.Counter, 1,                                  "Expected %d command calls, got %d."           )
   AssertEq ( t, c.Args,    [] string { "filesystem", "show" }, "Expected parameters %#v, got %#v."            )

   for i, f := range f {

      AssertEq ( t, f [0],  d [i] [0],  "Expected UUID %s, got %s."      )
      AssertEq ( t, f [1],  d [i] [1],  "Expected label %s, got %s."     )
      AssertEq ( t, f [2:], d [i] [2:], "Expected devices %#v, got %#v." )

   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsTool_GetBtrfsFilesystems_StdoutError (t *testing.T) {

   d := [][] string {
      [] string { "890abcde-f123-4567-890a-bcdef1234567", "label1", "/dev/dev1"                           },
      [] string { "890abcde-f123-4567-890a-bcdef1234567", "none",   "/dev/dev2", "/dev/dev3"              },
      [] string { "890abcde-f123-4567-890a-bcdef1234567", "",       "/dev/dev4", "/dev/dev5", "/dev/dev6" },
      [] string { "890abcde-f123-4567-890a-bcdef1234567", "'test'", "/dev/dev7"                           },
   }

   if os.Getenv ("MOCK_COMMAND") == "1" {
      TestBtrfsCommand_FilesystemShow (d)
      os.Exit (1)
   }

   c := &TestBtrfsCommand { FuncName : "Test_BtrfsTool_GetBtrfsFilesystems_StdoutError", BlockStdout : true }

   b, e := btrfstool.NewBtrfsTool (btrfstool.WithBtrfsCommand (c))

   AssertNl ( t, e,              "Expected no error, got %#v."  )
   AssertNn ( t, b,              "Expected BtrfsTool, got nil." )
   AssertTp ( t, b, "BtrfsTool", "Expected %s, got %s."         )

   f, e := b.GetBtrfsFilesystems ()

   AssertNl ( t, f,                                             "Expected filesystem list to be nil, got %#v." )
   AssertNn ( t, e,                                             "Expected an error, got nil."                  )
   AssertTp ( t, e,         "errorString",                      "Expected %s, got %s."                         )
   AssertEq ( t, c.Counter, 1,                                  "Expected %d command calls, got %d."           )
   AssertEq ( t, c.Args,    [] string { "filesystem", "show" }, "Expected parameters %#v, got %#v."            )

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsTool_GetBtrfsFilesystems_NoCommand (t *testing.T) {

   c := &TestBtrfsCommand { FuncName : "Test_BtrfsTool_GetBtrfsFilesystems_NoCommand", NonExisting : 1 }

   b, e := btrfstool.NewBtrfsTool (btrfstool.WithBtrfsCommand (c))

   AssertNl ( t, e,              "Expected no error, got %#v."  )
   AssertNn ( t, b,              "Expected BtrfsTool, got nil." )
   AssertTp ( t, b, "BtrfsTool", "Expected %s, got %s."         )

   f, e := b.GetBtrfsFilesystems ()

   AssertNl ( t, f,                                             "Expected filesystem list to be nil, got %#v." )
   AssertNn ( t, e,                                             "Expected an error, got nil."                  )
   AssertTp ( t, e,         "PathError",                        "Expected %s, got %s."                         )
   AssertEq ( t, c.Counter, 1,                                  "Expected %d command calls, got %d."           )
   AssertEq ( t, c.Args,    [] string { "filesystem", "show" }, "Expected parameters %#v, got %#v."            )

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsTool_GetBtrfsFilesystems_CommandNotFound (t *testing.T) {

   c := &TestBtrfsCommand { FuncName : "Test_BtrfsTool_GetBtrfsFilesystems_CommandNotFound", NonExisting : 2 }

   b, e := btrfstool.NewBtrfsTool (btrfstool.WithBtrfsCommand (c))

   AssertNl ( t, e,              "Expected no error, got %#v."  )
   AssertNn ( t, b,              "Expected BtrfsTool, got nil." )
   AssertTp ( t, b, "BtrfsTool", "Expected %s, got %s."         )

   f, e := b.GetBtrfsFilesystems ()

   AssertNl ( t, f,                                             "Expected filesystem list to be nil, got %#v." )
   AssertNn ( t, e,                                             "Expected an error, got nil."                  )
   AssertTp ( t, e,         "Error",                            "Expected %s, got %s."                         )
   AssertEq ( t, c.Counter, 1,                                  "Expected %d command calls, got %d."           )
   AssertEq ( t, c.Args,    [] string { "filesystem", "show" }, "Expected parameters %#v, got %#v."            )

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsTool_GetBtrfsFilesystems_MultipleParameters (t *testing.T) {

   if os.Getenv ("MOCK_COMMAND") == "1" {
      fmt.Fprintf (os.Stderr, "btrfs wrong usage message.\n")
      os.Exit (1)
   }

   c := &TestBtrfsCommand { FuncName : "Test_BtrfsTool_GetBtrfsFilesystems_MultipleParameters" }

   b, e := btrfstool.NewBtrfsTool (btrfstool.WithBtrfsCommand (c))

   AssertNl ( t, e,              "Expected no error, got %#v."  )
   AssertNn ( t, b,              "Expected BtrfsTool, got nil." )
   AssertTp ( t, b, "BtrfsTool", "Expected %s, got %s."         )

   f, e := b.GetBtrfsFilesystems ("1", "2")

   AssertLn ( t, f,         0,                                                "Expected list to be empty, got %#v."  )
   AssertNn ( t, e,                                                           "Expected an error, got nil."          )
   AssertTp ( t, e,         "ExitError",                                      "Expected %s, got %s."                 )
   AssertEq ( t, c.Counter, 1,                                                "Expected %d command calls, got %d."   )
   AssertEq ( t, c.Args,    [] string {"filesystem", "show", "--", "1", "2"}, "Expected parameters %#v, got %#v."    )

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsTool_GetBtrfsFilesystems_Success (t *testing.T) {

   d := [][] string {
      [] string { "890abcde-f123-4567-890a-bcdef1234567", "label1", "/dev/dev1"                           },
      [] string { "890abcde-f123-4567-890a-bcdef1234568", "none",   "/dev/dev2", "/dev/dev3"              },
      [] string { "890abcde-f123-4567-890a-bcdef1234569", "",       "/dev/dev4", "/dev/dev5", "/dev/dev6" },
      [] string { "890abcde-f123-4567-890a-bcdef1234560", "'test'", "/dev/dev7"                           },
   }

   if os.Getenv ("MOCK_COMMAND") == "1" {
      TestBtrfsCommand_FilesystemShow (d)
      os.Exit (0)
   }

   c := &TestBtrfsCommand { FuncName : "Test_BtrfsTool_GetBtrfsFilesystems_Success" }

   b, e := btrfstool.NewBtrfsTool (btrfstool.WithBtrfsCommand (c))

   AssertNl ( t, e,              "Expected no error, got %#v."  )
   AssertNn ( t, b,              "Expected BtrfsTool, got nil." )
   AssertTp ( t, b, "BtrfsTool", "Expected %s, got %s."         )

   f, e := b.GetBtrfsFilesystems ()

   AssertNl ( t, e,                                             "Expected no error, got %#v."        )
   AssertNn ( t, f,                                             "Expected filesystem list, got nil." )
   AssertLn ( t, f,         len (d),                            "Expected %d filesystems, got %d."   )
   AssertEq ( t, c.Counter, 1,                                  "Expected %d command calls, got %d." )
   AssertEq ( t, c.Args,    [] string { "filesystem", "show" }, "Expected parameters %#v, got %#v."  )

   for i, f := range f {

      AssertEq ( t, f [0],  d [i] [0],  "Expected UUID %s, got %s."      )
      AssertEq ( t, f [1],  d [i] [1],  "Expected label %s, got %s."     )
      AssertEq ( t, f [2:], d [i] [2:], "Expected devices %#v, got %#v." )

   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsTool_GetBtrfsFilesystems_InvalidUUID_1 (t *testing.T) {

   d := [][] string {
      [] string { "890abcde-f123-4567-890a-bcdef1234567", "label1", "/dev/dev1"                           },
      [] string { "890abcde-f123-4567-890a-bcdef1234568", "none",   "/dev/dev2", "/dev/dev3"              },
      [] string { "890abcde-f123-4567-890a-bcdef123456X", "",       "/dev/dev4", "/dev/dev5", "/dev/dev6" },
      [] string { "890abcde-f123-4567-890a-bcdef1234560", "'test'", "/dev/dev7"                           },
   }

   if os.Getenv ("MOCK_COMMAND") == "1" {
      TestBtrfsCommand_FilesystemShow (d)
      os.Exit (0)
   }

   c := &TestBtrfsCommand { FuncName : "Test_BtrfsTool_GetBtrfsFilesystems_InvalidUUID_1" }

   b, e := btrfstool.NewBtrfsTool (btrfstool.WithBtrfsCommand (c))

   AssertNl ( t, e,              "Expected no error, got %#v."  )
   AssertNn ( t, b,              "Expected BtrfsTool, got nil." )
   AssertTp ( t, b, "BtrfsTool", "Expected %s, got %s."         )

   f, e := b.GetBtrfsFilesystems ()

   AssertLn ( t, f,         2,                                  "Expected filesystem list size 2, got %#v." )
   AssertNn ( t, e,                                             "Expected an error, got nil."               )
   AssertTp ( t, e,         "NoUUIDOrLabelError",               "Expected %s, got %s."                      )
   AssertEq ( t, c.Counter, 1,                                  "Expected %d command calls, got %d."        )
   AssertEq ( t, c.Args,    [] string { "filesystem", "show" }, "Expected parameters %#v, got %#v."         )

   for i, f := range f {

      AssertEq ( t, f [0],  d [i] [0],  "Expected UUID %s, got %s."      )
      AssertEq ( t, f [1],  d [i] [1],  "Expected label %s, got %s."     )
      AssertEq ( t, f [2:], d [i] [2:], "Expected devices %#v, got %#v." )

   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsTool_GetBtrfsFilesystems_InvalidUUID_2 (t *testing.T) {

   d := [][] string {
      [] string { "890abcde-f123-4567-890a-bcdef1234567", "label1", "/dev/dev1"                           },
      [] string { "890abcde-f123-4567-890a-bcdef1234568", "none",   "/dev/dev2", "/dev/dev3"              },
      [] string { "890abcdef-1234-5678-90ab-cdef1234569", "",       "/dev/dev4", "/dev/dev5", "/dev/dev6" },
      [] string { "890abcde-f123-4567-890a-bcdef1234560", "'test'", "/dev/dev7"                           },
   }

   if os.Getenv ("MOCK_COMMAND") == "1" {
      TestBtrfsCommand_FilesystemShow (d)
      os.Exit (0)
   }

   c := &TestBtrfsCommand { FuncName : "Test_BtrfsTool_GetBtrfsFilesystems_InvalidUUID_2" }

   b, e := btrfstool.NewBtrfsTool (btrfstool.WithBtrfsCommand (c))

   AssertNl ( t, e,              "Expected no error, got %#v."  )
   AssertNn ( t, b,              "Expected BtrfsTool, got nil." )
   AssertTp ( t, b, "BtrfsTool", "Expected %s, got %s."         )

   f, e := b.GetBtrfsFilesystems ()

   AssertLn ( t, f,         2,                                  "Expected filesystem list size %d, got %d." )
   AssertNn ( t, e,                                             "Expected an error, got nil."               )
   AssertTp ( t, e,         "NoUUIDOrLabelError",               "Expected %s, got %s."                      )
   AssertEq ( t, c.Counter, 1,                                  "Expected %d command calls, got %d."        )
   AssertEq ( t, c.Args,    [] string { "filesystem", "show" }, "Expected parameters %#v, got %#v."         )

   for i, f := range f {

      AssertEq ( t, f [0],  d [i] [0],  "Expected UUID %s, got %s."      )
      AssertEq ( t, f [1],  d [i] [1],  "Expected label %s, got %s."     )
      AssertEq ( t, f [2:], d [i] [2:], "Expected devices %#v, got %#v." )

   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsTool_GetBtrfsFilesystems_NoDevicesError_Multiple (t *testing.T) {

   d := [][] string {
      [] string { "890abcde-f123-4567-890a-bcdef1234567", "label1", "/dev/dev1"                           },
      [] string { "890abcde-f123-4567-890a-bcdef1234568", "none"                                          },
      [] string { "890abcde-f123-4567-890a-bcdef1234569", "",       "/dev/dev4", "/dev/dev5", "/dev/dev6" },
      [] string { "890abcde-f123-4567-890a-bcdef1234560", "'test'", "/dev/dev7"                           },
   }

   if os.Getenv ("MOCK_COMMAND") == "1" {
      TestBtrfsCommand_FilesystemShow (d)
      os.Exit (0)
   }

   c := &TestBtrfsCommand { FuncName : "Test_BtrfsTool_GetBtrfsFilesystems_NoDevicesError_Multiple" }

   b, e := btrfstool.NewBtrfsTool (btrfstool.WithBtrfsCommand (c))

   AssertNl ( t, e,              "Expected no error, got %#v."  )
   AssertNn ( t, b,              "Expected BtrfsTool, got nil." )
   AssertTp ( t, b, "BtrfsTool", "Expected %s, got %s."         )

   f, e := b.GetBtrfsFilesystems ()

   AssertLn ( t, f,         1,                                  "Expected filesystem list size %d, got %d." )
   AssertNn ( t, e,                                             "Expected an error, got nil."               )
   AssertTp ( t, e,         "NoDevicesError",                   "Expected %s, got %s."                      )
   AssertEq ( t, c.Counter, 1,                                  "Expected %d command calls, got %d."        )
   AssertEq ( t, c.Args,    [] string { "filesystem", "show" }, "Expected parameters %#v, got %#v."         )

   for i, f := range f {

      AssertEq ( t, f [0],  d [i] [0],  "Expected UUID %s, got %s."      )
      AssertEq ( t, f [1],  d [i] [1],  "Expected label %s, got %s."     )
      AssertEq ( t, f [2:], d [i] [2:], "Expected devices %#v, got %#v." )

   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsTool_GetBtrfsFilesystems_NoDevicesError_Single (t *testing.T) {

   d := [][] string { [] string { "890abcde-f123-4567-890a-bcdef1234567", "label1" } }

   if os.Getenv ("MOCK_COMMAND") == "1" {
      TestBtrfsCommand_FilesystemShow (d)
      os.Exit (0)
   }

   c := &TestBtrfsCommand { FuncName : "Test_BtrfsTool_GetBtrfsFilesystems_NoDevicesError_Single" }

   b, e := btrfstool.NewBtrfsTool (btrfstool.WithBtrfsCommand (c))

   AssertNl ( t, e,              "Expected no error, got %#v."  )
   AssertNn ( t, b,              "Expected BtrfsTool, got nil." )
   AssertTp ( t, b, "BtrfsTool", "Expected %s, got %s."         )

   f, e := b.GetBtrfsFilesystems ()

   AssertLn ( t, f,         0,                                  "Expected filesystem list size %d, got %d." )
   AssertNn ( t, e,                                             "Expected an error, got nil."               )
   AssertTp ( t, e,         "NoDevicesError",                   "Expected %s, got %s."                      )
   AssertEq ( t, c.Counter, 1,                                  "Expected %d command calls, got %d."        )
   AssertEq ( t, c.Args,    [] string { "filesystem", "show" }, "Expected parameters %#v, got %#v."         )

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsTool_GetBtrfsFilesystems_NoUUIDOrLabelError (t *testing.T) {

   d := [][] string {
      [] string { "",                                     "label1", "/dev/dev1"                           },
      [] string { "890abcde-f123-4567-890a-bcdef1234568", "none",   "/dev/dev2", "/dev/dev3"              },
      [] string { "890abcde-f123-4567-890a-bcdef123456X", "",       "/dev/dev4", "/dev/dev5", "/dev/dev6" },
      [] string { "890abcde-f123-4567-890a-bcdef1234560", "'test'", "/dev/dev7"                           },
   }

   if os.Getenv ("MOCK_COMMAND") == "1" {
      TestBtrfsCommand_FilesystemShow (d)
      os.Exit (0)
   }

   c := &TestBtrfsCommand { FuncName : "Test_BtrfsTool_GetBtrfsFilesystems_NoUUIDOrLabelError" }

   b, e := btrfstool.NewBtrfsTool (btrfstool.WithBtrfsCommand (c))

   AssertNl ( t, e,              "Expected no error, got %#v."  )
   AssertNn ( t, b,              "Expected BtrfsTool, got nil." )
   AssertTp ( t, b, "BtrfsTool", "Expected %s, got %s."         )

   f, e := b.GetBtrfsFilesystems ()

   AssertLn ( t, f,         0,                                  "Expected filesystem list size %d, got %d." )
   AssertNn ( t, e,                                             "Expected an error, got nil."               )
   AssertTp ( t, e,         "NoUUIDOrLabelError",               "Expected %s, got %s."                      )
   AssertEq ( t, c.Counter, 1,                                  "Expected %d command calls, got %d."        )
   AssertEq ( t, c.Args,    [] string { "filesystem", "show" }, "Expected parameters %#v, got %#v."         )

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsTool_GetBtrfsFilesystems_MultipleMatchesError (t *testing.T) {

   d := [][] string {
      [] string { "890abcde-f123-4567-890a-bcdef1234567", "label1", "/dev/dev1"                           },
      [] string { "890abcde-f123-4567-890a-bcdef1234568", "none",   "/dev/dev2", "/dev/dev3"              },
      [] string { "890abcde-f123-4567-890a-bcdef1234569", "",       "/dev/dev4", "/dev/dev5", "/dev/dev6" },
      [] string { "890abcde-f123-4567-890a-bcdef1234560", "'test'", "/dev/dev7"                           },
   }

   if os.Getenv ("MOCK_COMMAND") == "1" {
      TestBtrfsCommand_FilesystemShow (d)
      os.Exit (0)
   }

   c := &TestBtrfsCommand { FuncName : "Test_BtrfsTool_GetBtrfsFilesystems_MultipleMatchesError" }

   b, e := btrfstool.NewBtrfsTool (btrfstool.WithBtrfsCommand (c))

   AssertNl ( t, e,              "Expected no error, got %#v."  )
   AssertNn ( t, b,              "Expected BtrfsTool, got nil." )
   AssertTp ( t, b, "BtrfsTool", "Expected %s, got %s."         )

   f, e := b.GetBtrfsFilesystems ("filter")

   AssertNn ( t, e,                                                             "Expected an error, got nil."        )
   AssertTp ( t, e,         "MultipleMatchesError",                             "Expected %s, got %s."               )
   AssertNn ( t, f,                                                             "Expected filesystem list, got nil." )
   AssertLn ( t, f,         len (d),                                            "Expected %d filesystems, got %d."   )
   AssertEq ( t, c.Counter, 1,                                                  "Expected %d command calls, got %d." )
   AssertEq ( t, c.Args,    [] string { "filesystem", "show", "--", "filter" }, "Expected parameters %#v, got %#v."  )

   for i, f := range f {

      AssertEq ( t, f [0 ], d [i] [0 ], "Expected UUID %s, got %s."      )
      AssertEq ( t, f [1 ], d [i] [1 ], "Expected label %s, got %s."     )
      AssertEq ( t, f [2:], d [i] [2:], "Expected devices %#v, got %#v." )

   }

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=119: ————————————————————————————————————————————————————————