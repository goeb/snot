// +build test

// Tests for the factory.BtrfsToolFactory type.
//
package factory

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "os"
   "testing"

   "snot/lib/btrfs/backend"
   "snot/lib/btrfs/backend/btrfstool/factory"

 . "snot/t/testtools"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

var _ backend.BtrfsBackendFactory = (*factory.BtrfsToolFactory) (nil)

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsToolFactory_NewBtrfsToolFactory_NoParameters (t *testing.T) {

   f, e := factory.NewBtrfsToolFactory ()

   AssertNl ( t, e,                     "Expected no error, got %#v."  )
   AssertNn ( t, f,                     "Expected BtrfsTool, got nil." )
   AssertTp ( t, f, "BtrfsToolFactory", "Expected %s, got %s."         )

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsToolFactory_NewBtrfsToolFactory_NilParameters (t *testing.T) {

   f, e := factory.NewBtrfsToolFactory (nil, nil)

   AssertNl ( t, e,                     "Expected no error, got %#v."  )
   AssertNn ( t, f,                     "Expected BtrfsTool, got nil." )
   AssertTp ( t, f, "BtrfsToolFactory", "Expected %s, got %s."         )

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsToolFactory_NewBtrfsToolFactory_ErrorParameters (t *testing.T) {

   o := func (*factory.BtrfsToolFactory) error {
      return TestError { "Error in constructor." }
   }

   f, e := factory.NewBtrfsToolFactory (o)

   AssertNl ( t, f,              "Expected nil, got %#v."      )
   AssertNn ( t, e,              "Expected an error, got nil." )
   AssertTp ( t, e, "TestError", "Expected %s, got %s."        )

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsToolFactory_NewBtrfsToolFactory_WithBtrfsCommand (t *testing.T) {

   f, e := factory.NewBtrfsToolFactory (factory.WithBtrfsCommand (&TestBtrfsCommand {}))

   AssertNl ( t, e,                     "Expected no error, got %#v."  )
   AssertNn ( t, f,                     "Expected BtrfsTool, got nil." )
   AssertTp ( t, f, "BtrfsToolFactory", "Expected %s, got %s."         )

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsToolFactory_NewBtrfsToolFactory_DefaultCommand (t *testing.T) {

   f, e := factory.NewBtrfsToolFactory ()

   AssertNl ( t, e,                     "Expected no error, got %#v."  )
   AssertNn ( t, f,                     "Expected BtrfsTool, got nil." )
   AssertTp ( t, f, "BtrfsToolFactory", "Expected %s, got %s."         )

   b, e := f.NewBtrfsBackend ()

   AssertNl ( t, e,              "Expected no error, got %#v."  )
   AssertNn ( t, b,              "Expected BtrfsTool, got nil." )
   AssertTp ( t, b, "BtrfsTool", "Expected %s, got %s."         )

}

//—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

func Test_BtrfsToolFactory_NewBtrfsToolFactory_TestCommand (t *testing.T) {

   if os.Getenv ("MOCK_COMMAND") == "1" {
      os.Exit (0)
   }

   c := &TestBtrfsCommand { FuncName : "Test_BtrfsToolFactory_NewBtrfsToolFactory_TestCommand" }

   f, e := factory.NewBtrfsToolFactory (factory.WithBtrfsCommand (c))

   AssertNl ( t, e,                     "Expected no error, got %#v."  )
   AssertNn ( t, f,                     "Expected BtrfsTool, got nil." )
   AssertTp ( t, f, "BtrfsToolFactory", "Expected %s, got %s."         )

   b, e := f.NewBtrfsBackend ()

   AssertNl ( t, e,              "Expected no error, got %#v."  )
   AssertNn ( t, b,              "Expected BtrfsTool, got nil." )
   AssertTp ( t, b, "BtrfsTool", "Expected %s, got %s."         )

   b.GetBtrfsFilesystems ()

   AssertEq ( t, c.Counter, 1, "Expected %d command calls, got %d." )

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=119: ————————————————————————————————————————————————————————