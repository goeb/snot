// The pathutils package defines the PathUtils and PathUtilsFactory interfaces and provides a
// default implementation for both.
//
package pathutils

//—————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "io"
   "os"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// A PathUtils implementation provides some path and file related functions.
//
type PathUtils interface {

   // See filepath.Abs(…).
   //
   Abs (path string) (string, error)

   // See filepath.Dir(…).
   //
   Dir (path string) string

   // DirIsEmpty(…) must return true if the specified directory is empty.
   //
   // On success the error must be nil, and the first return value must indicate whether the
   // specified directory is empty (true) or not (false). On failure the error must be set and the
   // first return value must be false.
   //
   DirIsEmpty (path string) (bool, error)

   // See filepath.EvalSymlinks(…).
   //
   EvalSymlinks (path string) (string, error)

   // IsMountPoint(…) must return true if the specified path is a mount point.
   //
   // On success the error must be nil, and the first return value must indicate whether the
   // specified path is a mount point (true) or not (false). On failure the error must be set and
   // the first return value must be false.
   //
   IsMountPoint (path string) (bool, error)

   // IsOutside(…) must return true if the specified relative path leads outside the working dir.
   //
   // The specified path must be a relative path (i.e. not start with the path separator). If it
   // starts with a "../" or if it is equal to ".." the function must return true, otherwise false.
   //
   IsOutside (path string) bool

   // Like os.MkdirAll(…), with the following additions:
   //
   // In case of an error, this method must clean up after itself, i.e. every newly created empty
   // directory has to be removed if an error occurs (with the exception of the directory exists
   // error, of course).
   //
   // If everything went well (regardless if a new directory has actually been created), the method
   // must return a list of all directories it has created, starting with the leaf (i.e. the last
   // directory in the path). The paths returned must be resolved and absolute. The list may be
   // empty if no directory has been created and no error occurred. In case of an error it must be
   // returned as the second return value (which must be nil on success), and the first return
   // value must be the list of newly created directories that could not be deleted after the
   // failure, if any.
   //
   MkdirAll (path string, perm os.FileMode) ([] string, error)

   // See os.Open(…)
   //
   Open (name string) (io.ReadCloser, error)

   // See os.OpenFile(…)
   //
   OpenFile (name string, flag int, perm os.FileMode) (io.ReadWriteCloser, error)

   // See ioutil.ReadDir(…)
   //
   ReadDir (dirname string) ([] os.FileInfo, error)

   // See os.Remove(…).
   //
   Remove (path string) error

   // RemoveEmptyTree(…) must remove empty directories in the directory tree rooted at (and
   // including) the specified path.
   //
   // All empty directories have to be removed (depth-first traversal), non-empty directories have
   // to be kept, but cause an error to be returned at the end. On success the return value must be
   // nil.
   //
   RemoveEmptyTree (path string) error

   // RemoveList(…) must remove the files/directories/… in the order specified.
   //
   // The removal has to be aborted on the first error, and the error has to be returned. On
   // success nil must be returned. Note that a non-existing file/directory/etc. is not an error
   // and the method must continue with the next entry in the list.
   //
   RemoveList (files ... string) error

   // See os.Rename(…).
   //
   Rename (oldpath string, newpath string) error

   // ResolvePath(…) must return the absolute and cleaned path with symlinks resolved.
   //
   // This is basically chaining EvalSymlinks(…) and Abs(…). In case of an error it must return the
   // error and an empty string, on success error must be nil.
   //
   ResolvePath (path string) (string, error)

   // Slashed(…) must return the supplied parameter enclosed by the path separator.
   //
   // The return value must begin and end with exactly one path separator character (usually "/").
   // If the specified path is empty (or consists only of separators), the return value must be a
   // single separator character. Otherwise the path should not be modified/resolved/cleaned….
   //
   Slashed (path string) string

   // See ioutil.TempDir(…).
   //
   TempDir (dir string, prefix string) (string, error)

   // The Unslashed(…) function must return the supplied parameter with any leading and trailing
   // path separators removed.
   //
   // The return value may be an empty string. Except for removing leading and trailing path
   // separators the specified string must not be modifed.
   //
   Unslashed (path string) string

}

// A PathUtilsFactory instance creates new PathUtils instances.
//
type PathUtilsFactory interface {

   // The NewPathUtils() method must return a new PathUtils instance every time it is called, or an
   // error if something went wrong.
   //
   NewPathUtils () (PathUtils, error)

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————