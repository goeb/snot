// The CommonPathUtils type.

package pathutils

//—————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "os"
   "path/filepath"
   "strings"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The CommonPathUtils type implements several methods of the PathUtils interface that do not
// actually require filesystem access, i.e. the return values depend solely on the input, not on
// any files etc. on the filesystem, it's basically pure string processing.
//
type CommonPathUtils struct {}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// See filepath.Dir(…).
//
func (pu *CommonPathUtils) Dir (path string) string {

   return filepath.Dir (path)

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The IsOutside(…) function checks if the specified relative path leads outside the working
// directory.
//
// The specified path must be a relative path (i.e. not start with the path separator). If it
// starts with a "../" or if it is equal to ".." this function will return true, otherwise false.
// The specified path will not be modified before the check.
//
func (pu *CommonPathUtils) IsOutside (path string) (bool) {

   return path == ".." || strings.HasPrefix (path, ".." + string (os.PathSeparator))

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Slashed(…) function returns the supplied parameter enclosed by the path separator.
//
// The return value will begin and end with exactly one path separator character (usually "/"). If
// the specified path is empty (or consists only of separators), the return value will be a single
// separator character.
//
func (pu *CommonPathUtils) Slashed (path string) string {

   separator := string (os.PathSeparator)
   trimmed   := strings.Trim (path, separator)

   if trimmed == "" {
      return separator
   }

   return separator + trimmed + separator

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Unslashed(…) function returns the supplied parameter with any leading and trailing path
// separators removed.
//
// The return value may be an empty string. Except for removing leading and trailing path
// separators the specified string will not be modifed.
//
func (pu *CommonPathUtils) Unslashed (path string) (string) {

   return strings.Trim (path, string (os.PathSeparator))

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————