// A PathUtils implementation using the standard library functions.

package pathutils

//—————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "io"
   "io/ioutil"
   "os"
   "path/filepath"
   "strings"
   "syscall"

   "snot/lib/multierror"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The DefaultPathUtils type implements the PathUtils interface. Most methods are just calling the
// appropriate standard library functions.
//
// The CommonPathUtils are embedded in the DefaultPathUtils.
//
type DefaultPathUtils struct {
   CommonPathUtils
}

// The DefaultPathUtilsFactory type implements the PathUtilsFactory interface to provide a factory
// method that creates DefaultPathUtils instances.
//
type DefaultPathUtilsFactory struct {}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Make sure we implement the PathUtils and PathUtilsFactory interfaces (compile time check).
//
var _ PathUtils        = (*DefaultPathUtils       ) (nil)
var _ PathUtilsFactory = (*DefaultPathUtilsFactory) (nil)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The NewDefaultPathUtils() function returns a new DefaultPathUtils instance.
//
func NewDefaultPathUtils () *DefaultPathUtils {

   return &DefaultPathUtils {}

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// See filepath.Abs(…).
//
func (pu *DefaultPathUtils) Abs (path string) (string, error) {

   return filepath.Abs (path)

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The DirIsEmpty(…) method returns true if the specified directory is empty.
//
// On success the error will be nil, and the first return value will indicate whether the specified
// directory is empty (true) or not (false). On failure the error will be set and the first return
// value must be false.
//
func (pu *DefaultPathUtils) DirIsEmpty (path string) (bool, error) {

   dir, err := os.Open (path)

   if err != nil {
      return false, err
   }

   defer dir.Close ()

   if _, err = dir.Readdirnames (1) ; err == io.EOF {
      return true, nil
   }

   return false, err

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// See filepath.EvalSymlinks(…).
//
func (pu *DefaultPathUtils) EvalSymlinks (path string) (string, error) {

   return filepath.EvalSymlinks (path)

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The IsMountPoint(…) method returns true if the specified path is a mount point.
//
// On success the error will be nil, and the first return value will indicate whether the specified
// path is a mount point (true) or not (false). On failure the error will be set and the first
// return value will be false.
//
// Note: Compares device numbers returned by a stat call. Files may also be mount points. False
// positives shouldn't occur, false negatives may be possible (unverified).
//
func (pu *DefaultPathUtils) IsMountPoint (path string) (bool, error) {

   var err error

   path1 := ""
   stat1 := syscall.Stat_t {}

   path2 := ""
   stat2 := syscall.Stat_t {}

   if path1, err = pu.ResolvePath (path) ; err != nil {
      return false, err
   }

   path2 = filepath.Dir (path1)

   if err = syscall.Stat (path1, &stat1) ; err != nil { return false, err }
   if err = syscall.Stat (path2, &stat2) ; err != nil { return false, err }

   return stat1.Dev != stat2.Dev, nil

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Like os.MkdirAll(…), with the following additions:
//
// In case of an error, this method will clean up after itself, i.e. every newly created empty
// directory will be removed if an error occurs (with the exception of the directory exists error,
// of course).
//
// If everything went well (regardless if a new directory has actually been created), the method
// will return a list of all directories it has created, starting with the leaf (i.e. the last
// directory in the path). The paths returned will be resolved and absolute. The list may be empty
// if no directory has been created and no error occurred. In case of an error nil and the error
// will be returned.
//
// Note: This will only work on Unix filesystems (i.e. anything using / as root dir and separator).
// Symbolic links in the specified path are not supported!
//
func (pu *DefaultPathUtils) MkdirAll (path string, perm os.FileMode) ([] string, error) {

   created := make ([] string, 0)

   if path = filepath.Clean (path) ; path == "." {
      return created, nil
   }

   parts   := strings.Split (path, string (os.PathSeparator))
   current := ""

   for _, part := range (parts) {

      // After splitting the first element should be the only one that can be empty (if the path
      // starts with a slash - assuming Unix-like filesystems). Set the current path to "/" and
      // continue in that case.

      if part == "" {
         current = "/"
         continue
      }

      current = filepath.Join (current, part)

      if err := os.Mkdir (current, perm) ; err == nil {

         if res, err := pu.ResolvePath (current) ; err == nil {
            created = append ([] string { res }, created ...)
            continue
         } else {
            for len (created) > 0 {
               if err := os.Remove (created [0]) ; err == nil {
                  created = created [1:]
               } else {
                  return created, err
               }
            }
            return created, err
         }

      } else if os.IsExist (err) {

         if stat, err := os.Lstat (current) ; err == nil {
            if ! stat.IsDir () {
               return created, &os.PathError { Op: "mkdir", Path: current, Err: syscall.ENOTDIR }
            }
         } else {
            return created, err
         }

      } else {

         for len (created) > 0 {
            if err := os.Remove (created [0]) ; err == nil {
               created = created [1:]
            } else {
               return created, err
            }
         }
         return created, err

      }

   }

   return created, nil

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// See os.Open(…).
//
func (pu *DefaultPathUtils) Open (name string) (io.ReadCloser, error) {

   return os.Open (name)

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// See os.OpenFile(…).
//
func (pu *DefaultPathUtils) OpenFile (

   name string,
   flag int,
   perm os.FileMode,

) (io.ReadWriteCloser, error) {

   return os.OpenFile (name, flag, perm)

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// See ioutil.ReadDir(…).
//
func (pu *DefaultPathUtils) ReadDir (dirname string) ([] os.FileInfo, error) {

   return ioutil.ReadDir (dirname)

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// See os.Remove(…).
//
func (pu *DefaultPathUtils) Remove (path string) error {

   return os.Remove (path)

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The RemoveEmptyTree(…) method will remove empty directories in the directory tree rooted at (and
// including) the specified path.
//
// The path will not be resolved/cleaned/etc.
//
// All empty directories will be removed (depth-first traversal), non-empty directories will be
// kept, but cause an error to be returned at the end. On success the return value will be nil.
//
func (pu *DefaultPathUtils) RemoveEmptyTree (path string) error {

   stat, err := os.Stat (path)

   if err != nil {
      return err
   } else if ! stat.IsDir () {
      return &os.PathError { Op: "rmempty", Path: path, Err: syscall.ENOTDIR }
   }

   subs, err := ioutil.ReadDir (path)

   if err != nil {
      return err
   }

   mErr := multierror.New ()

   for _, sub := range subs {
      if err := pu.RemoveEmptyTree (filepath.Join (path, sub.Name ())) ; err != nil {
         mErr.Append (err)
      }
   }

   if mErr.Get () == nil {
      return os.Remove (path)
   } else {
      return mErr.Get ()
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The RemoveList(…) method removes the files/directories/… in the order specified.
//
// The removal will be aborted on the first error, and the error will be returned. On success nil
// will be returned. Non-existing files etc. will not result in an error.
//
func (pu *DefaultPathUtils) RemoveList (files ... string) error {

   for _, file := range files {
      if err := pu.Remove (file) ; err != nil && ! os.IsNotExist (err) {
         return err
      }
   }

   return nil

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// See os.Rename(…).
//
func (pu *DefaultPathUtils) Rename (oldpath string, newpath string) error {

   return os.Rename (oldpath, newpath)

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The ResolvePath(…) method resolves all symbolic links and returns the cleaned absolute path.
//
// The path has to exist and it must be accessible. Paths that are not absolute will be resolved
// relative to the current working directory. In case of an error it will be returned and the path
// will be empty, on success the path will not be empty and error will be nil.
//
func (pu *DefaultPathUtils) ResolvePath (path string) (string, error) {

   if resolved, err := filepath.EvalSymlinks (path) ; err == nil {
      return filepath.Abs (resolved)
   } else {
      return "", err
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// See ioutil.TempDir(…).
//
func (pu *DefaultPathUtils) TempDir (dir string, prefix string) (string, error) {

   return ioutil.TempDir (dir, prefix)

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The NewDefaultPathUtilsFactory() method returns a new DefaultPathUtilsFactory instance.
//
func NewDefaultPathUtilsFactory () *DefaultPathUtilsFactory {

   return &DefaultPathUtilsFactory {}

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The NewPathUtils() method returns a new DefaultPathUtils instance.
//
func (pf *DefaultPathUtilsFactory) NewPathUtils () (PathUtils, error) {

   return NewDefaultPathUtils (), nil

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————