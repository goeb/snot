// These error types may be used by FSFinder implementations as appropriate.

package fsfinder

//—————————————————————————————————————————————————————————————————————————————————————————————————

import "fmt"

//—————————————————————————————————————————————————————————————————————————————————————————————————

// A MultipleMatchesError occurs when a specific filesystem is requested and multiple are found.
//
type MultipleMatchesError struct {
   Filesystem string
}

// The Error() method returns the error message.
//
func (err MultipleMatchesError) Error () string {
   return fmt.Sprintf ("Multiple matches for filesystem %s.", err.Filesystem)
}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// A NoDevicesError occurs when a filesystem is found without any devices.
//
type NoDevicesError struct {
   UUID string
}

// The Error() method returns the error message.
//
func (err NoDevicesError) Error () string {
   return fmt.Sprintf ("No devices found for filesystem %s.", err.UUID)
}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// A NoUUIDOrLabelError occurs when a filesystem is found without a valid UUID or label.
//
type NoUUIDOrLabelError struct {
   Dev string
}

// The Error() method returns the error message.
//
func (err NoUUIDOrLabelError) Error () string {
   return fmt.Sprintf ("Filesystem on %s has invalid UUID or label.", err.Dev)
}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————