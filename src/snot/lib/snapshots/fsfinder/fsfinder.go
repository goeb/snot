// The fsfinder package provides access to the registry of FSFinders to get a list of Snapshottable
// filesystems.
//
package fsfinder

//—————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "sync"

   "snot/lib/snapshots"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// An FSFinder implementation provides a method to retrieve a list of Snapshottable filesystems.
//
type FSFinder interface {

   // The Get() method must return a list of Snapshottable instances. In case of an error it must
   // return the Snapshottable list already build, or nil if there is none, and the error,
   // otherwise the list (which may be empty) and nil. Implementations may return all
   // Snapshottables they see fit, additional restrictions (filters etc.) may be implemented as
   // required.
   //
   Get () ([] snapshots.Snapshottable, error)

}

// The FSFinderRegistry type provides a registry for FSFinder implementations.
//
// FSFinderRegistry methods are thread-safe. FSFinders are registered by a name and a priority. The
// name should be the filesystem name, e.g. "btrfs" for FSFinders that return a list of Btrfs
// instances. The priority allows different implementations for one filesystem type to be present,
// during registration an implementation with a higher priority takes precedence over one with a
// lower priority.
//
type FSFinderRegistry struct {

   finders     map [string] FSFinder
   priorities  map [string] int
   lock        sync.Mutex

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

var (

   instance    *FSFinderRegistry       // A global FSFinderRegistry instance for convenience.
   once        sync.Once               // To initialize the global instance on demand.

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The NewFSFinderRegistry() function returns a new empty FSFinderRegistry instance.
//
func NewFSFinderRegistry () *FSFinderRegistry {

   return &FSFinderRegistry {
      finders    : make (map [string] FSFinder),
      priorities : make (map [string] int),
      lock       : sync.Mutex {},
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Add(…) method adds an FSFinder to the registry.
//
// The finder must be identified by the specified name. If a finder with this name is already
// present in the registry, it will be overridden if the specified priority is greater than the
// priority of the existing finder, otherwise it will not be added.
//
// Note: If the finder parameter is nil, an existing finder with the specified name will be removed
// if the specified priority is higher or equal to the priority of the existing finder.
//
func (ffreg *FSFinderRegistry) Add (name string, priority int, finder FSFinder) {

   ffreg.lock.Lock ()
   defer ffreg.lock.Unlock ()

   if prio, ok := ffreg.priorities [name] ; ok {
      if priority <= prio {
         return
      }
   }

   if finder != nil {
      ffreg.finders    [name] = finder
      ffreg.priorities [name] = priority
   } else {
      delete (ffreg.finders,    name)
      delete (ffreg.priorities, name)
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The All(…) function returns the list of registered FSFinder implementations.
//
// The list will be returned in no specific order. The list may be empty.
//
func (ffreg *FSFinderRegistry) All () [] FSFinder {

   ffreg.lock.Lock ()
   defer ffreg.lock.Unlock ()

   finders := make ([] FSFinder, len (ffreg.finders))

   i := 0
   for _, finder := range ffreg.finders {
      finders [i] = finder
      i ++
   }

   return finders

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Registry() function provides access to a "global" FSFinderRegistry instance.
//
// The instance will be created on demand, i.e. when Registry() is called for the first time. Once
// created, Registry() will always return the same instance. This is thread-safe.
//
func Registry () *FSFinderRegistry {

   once.Do (func () {
      instance = NewFSFinderRegistry ()
   })

   return instance

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————