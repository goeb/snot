// The snapshots package - create and restore filesystem snapshots.
//
package snapshots

//—————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "fmt"
   "os"
   "sort"
   "sync"
   "syscall"
   "time"

   "snot/lib/multierror"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Snapshot type represents a single snapshot of one or more filesystems.
//
type Snapshot struct {

   created              time.Time                  // Creation time of the snapshot, UTC.
   description          string                     // Description, expanded.
   descriptionTemplate  string                     // Description, template.
   filesystems          [] Snapshottable           // List of Snapshottable filesystems.

}

// The SnapshotOption type defines the signature for the constructor's option functions.
//
type SnapshotOption func (*Snapshot) error

// The ErrorHandler type defines the signature of the error handler functions for the Create(…),
// Restore(…) and Delete(…) methods.
//
// The first parameter is the channel that will receive any errors. The second parameter is a
// WaitGroup, the error handler has to call its Done() method on exit.
//
// The function must run until the error channel is closed. See the PrintErrors(…) function in
// this package for an example.
//
type ErrorHandler func (chan error, *sync.WaitGroup)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The NewSnapshot(…) function creates a new Snapshot instance.
//
// The following options are available to set properties of the Snapshot instance:
//
//    * WithCreationTime        (…) - Set the snapshot's timestamp.
//    * WithDescription         (…) - Set the snapshot's description.
//    * WithDescriptionTemplate (…) - Set the snapshot's description (template).
//    * WithFilesystems         (…) - Set the list of Snapshottable filesystems.
//
// See the individual option functions for more details.
//
// Unless stated otherwise, when using the same option multiple times the last one will override
// preceding options. Note that if the description is set to a non-empty string (WithDescription(…)
// option) the template set by WithDescriptionTemplate(…) will be ignored.
//
// If no creation time is set with WithCreationTime(…) it will be set to the current time.
//
// NewSnapshot(…) returns the instance on success (error will be nil), or nil and the error on
// failure.
//
func NewSnapshot (options ... SnapshotOption) (*Snapshot, error) {

   created  := time.Now ().UTC ()
   snapshot := &Snapshot { filesystems : make ([] Snapshottable, 0) }

   for _, option := range options {
      if option == nil {
         continue
      }
      if err := option (snapshot) ; err != nil {
         return nil, err
      }
   }

   if snapshot.created.IsZero () {
      snapshot.created = created
   }

   return snapshot, nil

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Snapshots(…) function returns a list of snapshots found on the Snapshottable filesystems
// specified as parameters.
//
// This function will call the Snapshottables' Snapshots() method to get a list of snapshot data
// for each filesystem. One Snapshot instance will be created per snapshot ID found (even if the
// same ID is found on multiple filesystems). The list will be sorted by creation time, starting
// with the oldest snapshot.
//
// Note: Currently the only data not derived from the snapshot's creation time is the description
// of the snapshot. In general, if multiple Snapshottables return different results for the same
// data like the description, the first non-empty (or non-zero) value will be used (filesystems
// will be processed in the order they are specified in the parameter list). If the meta data of
// a snapshot differs across filesystems, an error will be returned (and there is no guarantee as
// to which meta data the snapshot returned will use).
//
// If anything goes wrong, the list returned may be incomplete or nil (depending on where the error
// occurs) and the error will be set. On success the list will be returned and the error will be
// nil. The list may be empty.
//
func Snapshots (fs ... Snapshottable) ([] *Snapshot, error) {

   mErr := multierror.New ()

   // Using the snapshot timestamps as keys in the maps.

   metadata    := make (map [int64] *MetaData,        0)
   filesystems := make (map [int64] [] Snapshottable, 0)

   for _, fs := range fs {

      if fs == nil {
         continue
      }

      snaps, err := fs.Snapshots ()

      if err != nil {
         mErr.Append (err)
      }

      for _, snap := range snaps {

         snap    := snap
         created := snap.Created.UnixNano ()

         if current, ok := metadata [created] ; ok {

            if err := current.MergeStored (snap) ; err != nil {
               mErr.Append (err)
            }

            filesystems [created] = append (filesystems [created], fs)

         } else {

            metadata    [created]     = &snap
            filesystems [created]     = make ([] Snapshottable, 1)
            filesystems [created] [0] = fs

         }

      }

   }

   ids := make ([] int64, len (metadata))

   i := 0
   for id := range metadata {
       ids [i] = id
       i ++
   }

   sort.Slice (ids, func (i, j int) bool { return ids [i] < ids [j] })

   snapshots := make ([] *Snapshot, 0)

   for _, id := range ids {

      snapshot, err := NewSnapshot (
         WithCreationTime (metadata    [id].Created    ),
         WithFilesystems  (filesystems [id] ...        ),
         WithDescription  (metadata    [id].Description),
      )

      if err != nil {
         mErr.Append (err)
      } else {
         snapshots = append (snapshots, snapshot)
      }

   }

   return snapshots, mErr.Get ()

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithCreationTime(…) as parameter to NewSnapshot(…) to set the snapshot's creation time.
//
// Note that using a zero time will be treated as if no time has been set by the constructor. The
// specified time will always be converted to UTC.
//
func WithCreationTime (created time.Time) SnapshotOption {

   return func (snot *Snapshot) error {
      snot.created = created.UTC ()
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithDescription(…) as parameter to NewSnapshot(…) to set the snapshot's description.
//
// This will set the snapshot's description directly without the use of a template (to use one see
// WithDescriptionTemplate(…)). If the description is set to a non-empty string, the template will
// be ignored.
//
func WithDescription (description string) SnapshotOption {

   return func (snot *Snapshot) error {
      snot.description = description
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithDescriptionTemplate(…) as parameter to NewSnapshot(…) to set the snapshot's description.
//
// The description will be treated as a template and may contain SnapshotData fields (that make
// sense). It will be expanded on demand, i.e. when Data() is called. This means that any template
// errors will not be triggered immediatley. Use WithDescription(…) to set the description directly
// without using a template.
//
func WithDescriptionTemplate (template string) SnapshotOption {

   return func (snot *Snapshot) error {
      snot.descriptionTemplate = template
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithFilesystems(…) as parameter to NewSnapshot(…) to set the list of Snapshottable
// filesystems.
//
// Using this option multiple times is allowed, new filesystems will be appended to the current
// list.
//
func WithFilesystems (filesystems ... Snapshottable) SnapshotOption {

   return func (snot *Snapshot) error {
      snot.filesystems = append (snot.filesystems, filesystems ...)
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Create(…) method creates a new snapshot.
//
// The first parameter indicates whether snapshots should be created in parallel (if true) or
// sequentially (if false). In parallel mode, all snapshot operations will be prepared as
// necessary, and started in the background in parallel at the same time. If parallel is false,
// snapshots will be created one after another.
//
// The second parameter specifies the behaviour on error: if it is true, an error will not abort
// the snapshotting process, and successfully created snapshots will be left on disk. If false, an
// error will cause all already created snapshots to be deleted and the snapshotting to be aborted.
//
// The third parameter is the ErrorHandler function dealing with any errors. The PrintErrors(…)
// function returns an error handler that will print all errors to STDERR (which is pretty much
// everything this handler should do).
//
// Create(…) returns all errors that occured during snapshotting as multierror.MultiError instance,
// or nil if there were no errors.
//
func (snot *Snapshot) Create (parallel bool, ignoreErrors bool, errHandler ErrorHandler) error {

   creators := make ([] CreateSnapshot, 0)   // All CreateSnapshot(…) functions.
   errChan  := make (chan error)             // Channel to receive errors on.
   errDone  := sync.WaitGroup {}             // To wait for the error handler to be finished.
   allErrs  := multierror.New ()             // Container for all the errors received.
   allDone  := sync.WaitGroup {}             // To wait for all CreateSnapshot(…) goroutines.
   rollback := make ([] chan bool, 0)        // Whether to rollback, one channel for each creator.
   failure  := false                         // Will be true if any rollback-worthy error occured.

   // Start the error handler. It will run until the end of this method.

   errDone.Add (1)
   go errorWrapper (errChan, &errDone, allErrs, errHandler)

   // Get all creators for all filesystems. If an error occurs (and it shouldn't be ignored), empty
   // the creators list and stop looking for creators, effectively skipping the snapshot creation
   // while cleanup at the end of this method will still be done.

   data := snot.Data ()
   for _, fs := range snot.filesystems {
      crs, err := fs.Creators (data)
      if err != nil {
         errChan <- err
         if ! ignoreErrors {
            creators = nil
            break
         }
      }
      creators = append (creators, crs ...)
   }

   // Start the creator goroutines.

   if parallel {

      // Parallel mode:

      ready   := sync.WaitGroup {}                    // To sync the snapshot start.
      results := make (chan bool, len (creators))     // Receives "return" values of goroutines.

      for _, creator := range creators {

         // Create a bool channel for every goroutine, once all are done (i.e. once all have sent
         // their return value on the results channel) the rollback channel will be used to tell
         // the goroutines whether their work should be kept (if false) or discarded (if true).

         rb      := make (chan bool)
         rollback = append (rollback, rb)

         ready.Add   (1)
         allDone.Add (1)

         go creator (snot.Data (), results, rb, ignoreErrors, errChan, &ready, &allDone)

      }

      // The ready.Wait() call will block until all goroutines have finished their preparations.
      // The goroutines themeselves will also Wait() for this WaitGroup, so once all have called
      // Done() for it, this method and all goroutines will continue.

      syscall.Sync ()
      ready.Wait   ()

      // Collect the "return" values of all goroutines. The channels are unbuffered, the goroutines
      // will block until we read the result. If any of the goroutines returns false, we set the
      // rollback variable to true.

      for i := 1 ; i <= len (creators) ; i ++ {
         result := <- results
         if ! result {
            failure = true
         }
      }
      close (results)

   } else {

      // Sequential mode:

      for _, creator := range creators {

         result  := make (chan bool)                        // Return channel can be local here.
         rb      := make (chan bool)                        // Same as for parallel mode.
         rollback = append (rollback, rb)

         allDone.Add (1)

         // Even in sequential mode the goroutines have to run in the background to be able to
         // rollback the changes if required. The difference is that we can wait for the result of
         // the operation for every goroutine after we started it, and skip remaining filesystems
         // in case of an error (unless it should be ignored).

         syscall.Sync ()
         go creator (snot.Data (), result, rb, ignoreErrors, errChan, nil, &allDone)

         ok := <- result
         close (result)

         if ! ok {
            failure = true
            if ! ignoreErrors {
               break
            }
         }

      }

   }

   // After sending the result of the snapshot operation, the goroutines have to wait for the
   // overall result on their rollback channel, which determines whether the snapshot (if any)
   // should be kept (rollback will be false) or their work should be discarded (rollback will be
   // true).

   for _, rb := range rollback {
      rb <- (failure && ! ignoreErrors)
      close (rb)
   }

   // Wait for all goroutines (and the error handler) to be finished and return the errors
   // collected (or nil if there were no errors).

   allDone.Wait ()

   close (errChan)
   errDone.Wait ()

   return allErrs.Get ()

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Restore(…) method restores an existing snapshot.
//
// The first parameter specifies the behaviour on error: if it is true, any non-fatal errors will
// not abort the process. If false, all errors will cause the operation to be cancelled and all
// changes to be undone.
//
// The second parameter is the ErrorHandler function dealing with any errors. The PrintErrors(…)
// function returns an error handler that will print all errors to STDERR (which is pretty much
// everything this handler should do).
//
// Restore(…) returns all errors that occured as a multierror.MultiError instance, or nil if there
// were no errors.
//
func (snot *Snapshot) Restore (ignoreErrors bool, errHandler ErrorHandler) error {

   // This is pretty much the same logic as in Create(…), only difference is that Restore(…) does
   // not support a parallel mode…

   restorers := make ([] RestoreSnapshot, 0)  // All RestoreSnapshot(…) functions.
   errChan   := make (chan error)             // Channel to receive errors on.
   errDone   := sync.WaitGroup {}             // To wait for the error handler to be finished.
   allErrs   := multierror.New ()             // Container for all the errors received.
   allDone   := sync.WaitGroup {}             // To wait for all CreateSnapshot(…) goroutines.
   rollback  := make ([] chan bool, 0)        // Whether to rollback, one channel for each creator.
   failure   := false                         // Will be true if any rollback-worthy error occured.

   // Start the error handler. It will run until the end of this method.

   errDone.Add (1)
   go errorWrapper (errChan, &errDone, allErrs, errHandler)

   // Get all restorers for all filesystems. If an error occurs (and it shouldn't be ignored),
   // empty the list and stop looking for restorers, effectively skipping the snapshot restore
   // while cleanup at the end of this method will still be done.

   data := snot.Data ()
   for _, fs := range snot.filesystems {
      rs, err := fs.Restorers (data)
      if err != nil {
         errChan <- err
         if ! ignoreErrors {
            restorers = nil
            break
         }
      }
      restorers = append (restorers, rs ...)
   }

   // Start the restorer goroutines.

   for _, restorer := range restorers {

      result  := make (chan bool)
      rb      := make (chan bool)
      rollback = append (rollback, rb)

      allDone.Add (1)

      syscall.Sync ()
      go restorer (snot.Data (), result, rb, ignoreErrors, errChan, &allDone)

      ok := <- result
      close (result)

      if ! ok {
         failure = true
         if ! ignoreErrors {
            break
         }
      }

   }

   // After sending the result of the snapshot operation, the goroutines have to wait for the
   // overall result on their rollback channel, which determines whether the snapshot (if any)
   // should be kept or their work should be discarded.

   for _, rb := range rollback {
      rb <- (failure && ! ignoreErrors)
      close (rb)
   }

   // Wait for all goroutines (and the error handler) to be finished and return the errors
   // collected (or nil if there were no errors).

   allDone.Wait ()

   close (errChan)
   errDone.Wait ()

   return allErrs.Get ()

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Delete(…) method deletes an existing snapshot.
//
// The first parameter specifies the behaviour on error: if it is true, an error will not abort the
// deletion, and as much of the snapshot as possible will be removed. If it is false, the first
// error encountered will abort the process.
//
// The second parameter is the ErrorHandler function dealing with any errors. The PrintErrors(…)
// function returns an error handler that will print all errors to STDERR (which is pretty much
// everything this handler should do).
//
// Delete(…) returns all errors that occured as multierror.MultiError instance, or nil if there
// were no errors.
//
func (snot *Snapshot) Delete (ignoreErrors bool, errHandler ErrorHandler) error {

   errChan := make (chan error)              // Channel to receive errors on.
   errDone := sync.WaitGroup {}              // To wait for the error handler to be finished.
   allErrs := multierror.New ()              // Container for all the errors received.

   // Start the error handler. It will run until the end of this method.

   errDone.Add (1)
   go errorWrapper (errChan, &errDone, allErrs, errHandler)

   // Simply iterate over the Snapshottables and call Delete(…). Error behaviour depends on the
   // ignoreErrors parameter.

   data := snot.Data ()

   for _, fs := range snot.filesystems {
      ok := fs.Delete (data, ignoreErrors, errChan)
      if ! ok && ! ignoreErrors {
         break
      }
   }

   // Wait for the error handler to finish and return the collected errors (or nil if there were
   // no errors).

   close (errChan)
   errDone.Wait ()

   return allErrs.Get ()

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The errorWrapper(…) function will be started as a goroutine by Create(…) etc.
//
// It will receive errors on the error channel specified as the first parameter and add these to
// the MultiError instance specified as the third parameter. The wrapper will itself start the
// ErrorHandler (last parameter) and forward any error it receives to this handler. The second
// parameter is a WaitGroup and the wrapper will call its Done() method once the error channel has
// been closed and the wrapper has finished cleaning up.
//
func errorWrapper (ch chan error, wg *sync.WaitGroup, me *multierror.MultiError, eh ErrorHandler) {

   defer wg.Done ()

   var errCh chan error
   var errWg sync.WaitGroup

   errCh = make (chan error)
   errWg.Add (1)
   go eh (errCh, &errWg)

   for err := range (ch) {
      me.Append (err)
      errCh <- err
   }

   close (errCh)
   errWg.Wait ()

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The PrintErrors(…) function returns an ErrorHandler that will print the errors to STDERR.
//
// The prefix parameter will be prepended to the error message for printing.
//
func PrintErrors (prefix string) ErrorHandler {

   return func (ch chan error, wg *sync.WaitGroup) {

      defer wg.Done ()

      for err := range ch {
         fmt.Fprintf (os.Stderr, "%s%v\n", prefix, err)
      }

   }

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————