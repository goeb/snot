// The MetaStorage and MetaStorageFactory interfaces.

package metadata

//—————————————————————————————————————————————————————————————————————————————————————————————————

import "snot/lib/snapshots"

//—————————————————————————————————————————————————————————————————————————————————————————————————

// A MetaStorage interface implementing type may be used to store/load/delete snapshot meta data.
//
type MetaStorage interface {

   // The Save(…) method must save the meta data to the storage.
   //
   // In case of an error, it must be returned, otherwise the return value must be nil. Note that
   // this method may ignore certain errors (for example when trying to create a file that already
   // exists).
   //
   // The first parameter is a SnapshotData struct for the snapshot. The method may use the
   // SnapshotData.MetaData function to convert the SnapshotData to the MetaData that should be
   // stored.
   //
   // The second parameter may be used to pass data specific to the MetaStorage implementation to
   // the method (if required). See the implementation documentation for more details.
   //
   Save (snapshots.SnapshotData, interface {}) error

   // The Load(…) method must load the meta data from the storage.
   //
   // Parameters are the same as for Save(…), see there for more details. The snapshot to load the
   // data for is specified by its SnapshotData (first parameter, fields that have to be loaded
   // from the storage will be uninitialized in this data).
   //
   // The method must return the MetaData and nil on success. In case of an error the MetaData
   // returned should be initialized as far as possible, and the error must be returned.
   //
   Load (snapshots.SnapshotData, interface {}) (snapshots.MetaData, error)

   // The Delete(…) method must delete the meta data from the storage.
   //
   // The snapshot will be specified by its SnapshotData as the first parameter.
   //
   // In case of an error, it must be returned, otherwise the return value must be nil. The
   // non-existence of meta data for the specified snapshot should not be considered an error.
   //
   Delete (snapshots.SnapshotData, interface {}) error

}

// A MetaStorageFactory interface implementing type may be used to create MetaStorage instances.
//
type MetaStorageFactory interface {

   // The NewMetaStorage() method must return a new MetaStorage instance.
   //
   // On success, the instance and nil must be returned, otherwise nil and the error.
   //
   NewMetaStorage () (MetaStorage, error)

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————