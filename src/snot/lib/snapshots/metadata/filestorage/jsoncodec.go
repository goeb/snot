// The JSONCodec type, implementing the FileCodec interface.

package filestorage

//—————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "encoding/json"
   "io"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The JSONCodec type implements the FileCodec interface, using JSON as serialization format.
//
type JSONCodec struct {}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Check that the JSONCodec type implements the FileCodec interface (compile-time check).
//
var _ FileCodec = (*JSONCodec) (nil)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The NewJSONCodec() function returns a new JSONCodec instance.
//
func NewJSONCodec () *JSONCodec {

   return &JSONCodec {}

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Encode(…) function will encode the data specified by the first parameter to JSON and write
// the result the io.Writer specified by the second parameter.
//
// In case of an error it will to be returned, otherwise the return value will be nil.
//
func (j *JSONCodec) Encode (data interface {}, writer io.Writer) error {

   enc := json.NewEncoder (writer)
   enc.SetIndent ("", "   ")

   return enc.Encode (data)

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Decode(…) function will decode the JSON data read from the io.Reader specified as the second
// parameter and store it in the value pointed to by the first parameter.
//
// In case of an error it will be returned, otherwise the return value will be nil.
//
func (j *JSONCodec) Decode (data interface {}, reader io.Reader) error {

   dec := json.NewDecoder (reader)

   return dec.Decode (data)

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————