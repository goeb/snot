// The factory package provides the FileStorageFactory type, implementing the MetaStorageFactory
// interface.
//
package factory

//—————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "snot/lib/pathutils"
   "snot/lib/snapshots/metadata"
   "snot/lib/snapshots/metadata/filestorage"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The FileStorageFactory type implements the MetaStorageFactory interface to create FileStorage
// instances.
//
type FileStorageFactory struct {

   fileCodec         filestorage.FileCodec            // The FileCodec for the new instances.
   filenameTemplate  string                           // File name template.
   pathUtilsFactory  pathutils.PathUtilsFactory       // Factory to create the PathUtils.

}

// The FileStorageFactoryOption type defines the signature for the constructor's option functions.
//
type FileStorageFactoryOption func (*FileStorageFactory) error

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Check if FileStorageFactory implements the MetaStorageFactory interface (compile-time check).
//
var _ metadata.MetaStorageFactory = (*FileStorageFactory) (nil)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The NewFileStorageFactory(…) function creates a new FileStorageFactory instance.
//
// Available options are:
//
//    * WithFileCodec        (…) - Set the FileCodec for the new FileStorage instances.
//    * WithFilenameTemplate (…) - Set the file name template.
//    * WithPathUtilsFactory (…) - Set the PathUtilsFactory for the instance.
//
// See the individual option functions for more details.
//
// Returns the new FileStorageFactory instance and nil for the error on success, or nil and the
// error in case of an error.
//
func NewFileStorageFactory (options ... FileStorageFactoryOption) (*FileStorageFactory, error) {

   ff := &FileStorageFactory {}

   for _, option := range options {
      if option == nil {
         continue
      }
      if err := option (ff) ; err != nil {
         return nil, err
      }
   }

   return ff, nil

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithFileCodec(…) as parameter to NewFileStorageFactory(…) to set the FileCodec for new
// FileStorage instances created by the factory.
//
func WithFileCodec (codec filestorage.FileCodec) FileStorageFactoryOption {

   return func (ff *FileStorageFactory) error {
      ff.fileCodec = codec
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithFileTemplate(…) as parameter to NewFileStorageFactory(…) to set the file name template
// for new FileStorage instances created by the factory.
//
// See the filestorage.WithFilenameTemplate(…) function for more information.
//
func WithFileTemplate (template string) FileStorageFactoryOption {

   return func (ff *FileStorageFactory) error {
      ff.filenameTemplate = template
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithPathUtilsFactory(…) to set the PathUtilsfactory that will be used to create the
// PathUtils for the FileStorage instance.
//
func WithPathUtilsFactory (factory pathutils.PathUtilsFactory) FileStorageFactoryOption {

   return func (ff *FileStorageFactory) error {
      ff.pathUtilsFactory = factory
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The NewMetaStorage() method returns a new FileStorage instance.
//
func (ff *FileStorageFactory) NewMetaStorage () (metadata.MetaStorage, error) {

   opts := [] filestorage.FileStorageOption {
      filestorage.WithFileCodec        (ff.fileCodec       ),
      filestorage.WithFilenameTemplate (ff.filenameTemplate),
   }

   if ff.pathUtilsFactory != nil {
      if pu, err := ff.pathUtilsFactory.NewPathUtils () ; err == nil {
         opts = append (opts, filestorage.WithPathUtils (pu))
      } else {
         return nil, err
      }
   }

   return filestorage.NewFileStorage (opts ...)

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————