// The filestorage package contains the FileStorage type implementing the MetaStorage interface.
//
package filestorage

//—————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "io"
   "os"

   "snot/lib/pathutils"
   "snot/lib/snapshots"
   "snot/lib/snapshots/metadata"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The FileStorage type implements the MetaStorage interface, storing snapshot meta data to files.
//
type FileStorage struct {

   directories       [] string               // Directories created to store the meta data.
   fileCodec         FileCodec               // Codec to use. Defaults to JSONCodec.
   filename          string                  // Last file created with Save(…).
   filenameTemplate  string                  // Template for file name.
   pathUtils         pathutils.PathUtils     // PathUtils implementing instance.

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The FileStorageOption type defines the signature for the constructor's option functions.
//
type FileStorageOption func (*FileStorage) error

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Check if FileStorage implements the MetaStorage interface (compile-time check).
//
var _ metadata.MetaStorage = (*FileStorage) (nil)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The NewFileStorage(…) function creates a new FileStorage instance.
//
// The following options are available to set properties of the new instance:
//
//    * WithFileCodec        (…) - Set FileCodec implementation.
//    * WithFilenameTemplate (…) - Set the file name template.
//    * WithPathUtils        (…) - Set the PathUtils implementation.
//
// See the individual option functions for more details.
//
// Unless stated otherwise, when using the same option multiple times the last one will override
// preceding options.
//
// If no FileCodec is set, it will be set to a new JSONCodec instance.
//
// If no PathUtils is set, it will be set to a new DefaultPathUtils instance.
//
// NewFileStorage(…) returns the instance on success (error will be nil), or nil and the error on
// failure.
//
func NewFileStorage (options ... FileStorageOption) (*FileStorage, error) {

   fs := &FileStorage {}

   for _, option := range options {
      if option == nil {
         continue
      }
      if err := option (fs) ; err != nil {
         return nil, err
      }
   }

   if fs.fileCodec == nil {
      fs.fileCodec = NewJSONCodec ()
   }

   if fs.pathUtils == nil {
      fs.pathUtils = pathutils.NewDefaultPathUtils ()
   }

   return fs, nil

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithFileCodec(…) as parameter to NewFileStorage(…) to set the FileCodec for the instance.
//
// If no codec is set, it will be set to a new JSONCodec by the constructor.
//
func WithFileCodec (codec FileCodec) FileStorageOption {

   return func (fs *FileStorage) error {
      fs.fileCodec = codec
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithFileTemplate(…) as parameter to NewFileStorage(…) to set the file name template for the
// instance.
//
// The template may contain any fields present in the data that will later be passed to the
// Save(…)/Load(…)/Delete(…) methods. The template will be expanded in Save(…) etc. using the
// snapshots.ExpandTemplate(…) function, see there for details on the syntax.
//
func WithFilenameTemplate (template string) FileStorageOption {

   return func (fs *FileStorage) error {
      fs.filenameTemplate = template
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithPathUtils(…) as parameter to NewFileStorage(…) to set the PathUtils for the instance.
//
func WithPathUtils (pu pathutils.PathUtils) FileStorageOption {

   return func (fs *FileStorage) error {
      fs.pathUtils = pu
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Save(…) method will save the snapshot meta data to a file.
//
// To get the filename the template set via the constructor option (see WithFilenameTemplate(…))
// will be expanded using the data supplied as the second parameter. Missing directories will be
// created if necessary.
//
// Note: The path will not be verified or sanitized in any way after template expansion!
//
// This method will return nil on success, the error otherwise. In case of an error all newly
// created directories will be removed (if possible). Already existing files will not be modified
// and will not cause an error to be returned.
//
func (fs *FileStorage) Save (data snapshots.SnapshotData, config interface {}) error {

   var path string
   var err  error
   var dirs [] string

   if path, err = snapshots.ExpandTemplate (fs.filenameTemplate, config) ; err != nil {
      return err
   }

   if dirs, err = fs.pathUtils.MkdirAll (fs.pathUtils.Dir (path), 0700) ; err != nil {
      return err
   }

   file, err := fs.pathUtils.OpenFile (path, os.O_CREATE | os.O_EXCL | os.O_WRONLY, 0600)

   if err != nil {
      if os.IsExist (err) {
         return nil
      }
      if err := fs.pathUtils.RemoveList (dirs ...) ; err != nil {
         return err
      }
      return err
   }

   defer file.Close ()

   meta := data.MetaData ()

   if err = fs.fileCodec.Encode (meta, file) ; err != nil {
      if err := file.Close              (        ) ; err != nil { return err }
      if err := fs.pathUtils.Remove     (path    ) ; err != nil { return err }
      if err := fs.pathUtils.RemoveList (dirs ...) ; err != nil { return err }
      return err
   }

   fs.filename    = path
   fs.directories = dirs

   return file.Close ()

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Load(…) method will load the snapshot meta data from a file.
//
// To get the filename the template set via the constructor option (see WithFilenameTemplate(…))
// will be expanded using the data supplied as the second parameter.
//
// Note: The path will not be verified or sanitized in any way after template expansion!
//
// This method will return the MetaData and nil as error on success. In case of an error, the
// MetaData (which may not be fully initialized) and the error. Unlike Save(…), that does not
// consider already existing files an error, non-existing files wil cause an error with Load(…).
// The caller has to handle these errors appropriately if required (use os.IsNotExist(…)). Note
// that it is also an error if the meta data read from storage does not match the SnapshotData
// where it should (e.g. if the timestamps differ).
//
//
func (fs *FileStorage) Load (

   data     snapshots.SnapshotData,
   config   interface {},

) (snapshots.MetaData, error) {

   var load snapshots.MetaData
   var path string
   var err  error
   var file io.ReadCloser

   meta := data.MetaData ()

   if path, err = snapshots.ExpandTemplate (fs.filenameTemplate, config) ; err != nil {
      return meta, err
   }

   if file, err = fs.pathUtils.Open (path) ; err != nil {
      return meta, err
   }

   defer file.Close ()

   if err = fs.fileCodec.Decode (&load, file) ; err != nil {
      return meta, err
   }

   if err = meta.MergeStored (load) ; err != nil {
      return meta, err
   }

   return meta, file.Close ()

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Delete(…) method will delete the meta data file for the snapshot.
//
// To get the filename the template set via the constructor option (see WithFilenameTemplate(…))
// will be expanded using the data supplied as the second parameter.
//
// Note: The path will not be verified or sanitized in any way after template expansion!
//
// If Delete(…) is called after Save(…) and the meta data file is the same for both (after template
// processing), Delete(…) will also try to remove all directories that have been created by Save(…)
// earlier. An error deleting one of these directories will also be returned!
//
// This method will return nil on success, the error otherwise. Unlike for Load(…), it is not an
// error if the meta data file does not exist.
//
func (fs *FileStorage) Delete (data snapshots.SnapshotData, config interface {}) error {

   var path string
   var err  error

   if path, err = snapshots.ExpandTemplate (fs.filenameTemplate, config) ; err != nil {
      return err
   }

   if err = fs.pathUtils.Remove (path) ; err != nil && ! os.IsNotExist (err) {
      return err
   }

   if path == fs.filename {
      if err = fs.pathUtils.RemoveList (fs.directories ...) ; err != nil {
         return err
      }
   }

   return nil

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————