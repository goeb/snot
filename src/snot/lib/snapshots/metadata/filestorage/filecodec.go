// The FileCodec interface.

package filestorage

//—————————————————————————————————————————————————————————————————————————————————————————————————

import "io"

//—————————————————————————————————————————————————————————————————————————————————————————————————

// A FileCodec implementation must serialize the snapshot meta data to a format suitable to be
// saved to a file, and deserialize the data read from a file.
//
// Note: The interface uses io.Reader and io.Writer, so it is not strictly bound to using files.
// The data is also not bound to a specific type to allow different implementations use whatever
// they see fit.
//
// The implementing type should be stateless as one instance may be used by multiple FileStorage
// instances!
//
type FileCodec interface {

   // The Encode(…) function must encode the data specified by the first parameter and write it to
   // the io.Writer specified by the second parameter.
   //
   // In case of an error it has to be returned, otherwise the return value has to be nil.
   //
   Encode (interface {}, io.Writer) error

   // The Decode(…) function must decode the data read from the io.Reader specified as the first
   // parameter, and store it in the value pointed to by the second parameter.
   //
   // In case of an error it has to be returned, otherwise the return value has to be nil.
   //
   Decode (interface {}, io.Reader) error

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————