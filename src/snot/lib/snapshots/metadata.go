// The MetaData type and related methods.

package snapshots

//—————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "reflect"
   "time"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The MetaData type defines the data to be saved to disk (by a MetaStorage implementation). It
// contains all the data necessary to restore a snapshot's information. Creation time is UTC.
//
// For convenience, it also includes the timestamp (Unix timestamp, nanoseconds), and the FNV hash
// of this timestamp, both of these can be restored from the creation time.
//
type MetaData struct {

   Created              time.Time   `type:"check"`    // Creation time. UTC.
   ID                   string      `type:"check"`    // FNV-1a 32 bit hash of the UnixNano time.
   Timestamp            string      `type:"check"`    // UnixNano time (base 10), primary ID.

   Description          string      `type:"restore"`  // Expanded snapshot description.

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The MetaData() method returns the MetaData derived from this SnapshotData.
//
func (data *SnapshotData) MetaData () MetaData {

   return MetaData {
      Created     : data.Created,
      ID          : data.ID,
      Timestamp   : data.TS,
      Description : data.Description,
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The MergeStored(…) method merges this MetaData with the specified MetaData.
//
// This MetaData's fields that are tagged "restore" in the struct will be set to the value of the
// stored MetaData's fields (the only parameter of this method) if they contain their type's zero
// value. If they do not contain the zero value of their type, they will keep their current value.
// If a "restore" field is not zero in both structs, and they do not match, it will cause a
// FieldMismatchError to be returned at the end.
//
// Fields tagged "check" will be compared, if one of those do not match between the two structs the
// return value of this method will be a FieldMismatchError. If all match (or none are tagged
// "check"), nil will be returned.
//
func (data *MetaData) MergeStored (stored MetaData) error {

   // Could copy them manually, just an exercise in reflection. Doesn't increase the executable
   // size, so it's fine. Should be slower, though, but that doesn't really matter here.

   tp := reflect.TypeOf (stored)
   mv := reflect.Indirect (reflect.ValueOf (data))
   sv := reflect.ValueOf (&stored).Elem ()
   eq := true

   for i := 0 ; i < tp.NumField () ; i ++ {

      tg := tp.Field (i).Tag.Get ("type")
      mf := mv.Field (i)
      sf := sv.Field (i)

      switch tg {

         case "restore":

            if mf.Interface () == reflect.Zero (mf.Type ()).Interface () {
               mf.Set (sf)
            } else if sf.Interface () != reflect.Zero (sf.Type ()).Interface () {
               eq = eq && (mf.Interface () == sf.Interface ())
            }

         case "check":

            eq = eq && (mf.Interface () == sf.Interface ())

      }

   }

   if ! eq {
      return FieldMismatchError { data.ID, stored.ID }
   }

   return nil

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————