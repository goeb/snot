// The Snapshottable interface.

package snapshots

//—————————————————————————————————————————————————————————————————————————————————————————————————

import "sync"

//—————————————————————————————————————————————————————————————————————————————————————————————————

// A CreateSnapshot(…) function must create the snapshot (or part of the snapshot) of a filesystem.
//
// A Snapshottable filesystem may make use of more than one of these functions, for example a Btrfs
// filesystem may use one function for each subvolume to create a snapshot of.
//
// The function will always be run as a goroutine, all CreateSnapshot(…) functions (of all
// Snapshottable filesystems) will eventually run in parallel, even in sequential mode! Sequential
// mode is indicated by the fifth parameter (ready) being nil. In parallel mode, all functions will
// will be started in parallel, in sequential mode the next function will only be started when the
// previous function has returned a result on the result channel (second parameter)!
//
// The function(s) must create a snapshot for the filesystem based on the SnapshotData passed as
// first parameter, and write the meta data to disk (it is up to the Snapshottable what and how or
// where this data is stored, but the Snapshottable has to be able to restore the snapshot instance
// later). If the fifth parameter (ready) is not nil, the function must prepare the snapshot as far
// as possible without actually creating it, call ready.Done() and then ready.Wait() to sync with
// other CreateSnapshot(…) functions. Once ready.Wait() returns the snapshot must be created. If
// the fifth parameter is nil, the snapshot must be taken immediately. After that, success or
// failure creating the snapshot must be sent back to the caller on the result channel (second
// parameter), either true for success or false for failure. The function must send exactly one
// value on this channel! After sending its result back to the caller, the function must read one
// value from the rollback channel (third parameter, will be closed after this one value). If this
// is true, the snapshot just taken (if any) has to be deleted, everything has to be undone. If it
// is false, all went well and the results must be kept. Note that if the function itself
// encounters a fatal error, it is free to immediately clean up as far as necessary (e.g. deleting
// a directory that has been created for the snapshot if it does not serve any other purpose). For
// non-fatal errors the function should continue if ignoreErrors (fourth parameter) is true,
// otherwise it should act as for fatal errors. The result sent back on the results channel should
// always be false in case of an error, even if ignoreErrors is true and the error is not fatal. At
// the very end the function must signal that it is done by calling the Done() method of the
// WaitGroup passed as the sixth parameter. Any errors that occur must be sent to the caller on the
// errors channel passed as the fourth parameter.
//
// Example function that doesn't do anything (parameters omitted for brevity):
//
//    func CreateSnapshot (…) {
//
//       defer done.Done ()
//
//       // Prepare for the snapshot.
//
//       if ready != nil {
//          ready.Done ()
//          ready.Wait ()
//       }
//
//       // Create the snapshot.
//
//       // In case everything is ok:
//       result <- true
//
//       // If there were an error:
//       //
//       //    errors <- someError
//       //    result <- false
//
//       rb := <- rollback
//
//       if rb {
//          // Undo everything.
//       }
//
//    }
//
type CreateSnapshot func (

   data           SnapshotData,
   result         chan bool,
   rollback       chan bool,
   ignoreErrors   bool,
   errors         chan error,
   ready          *sync.WaitGroup,
   done           *sync.WaitGroup,

)

// A RestoreSnapshot(…) function must restore the snapshot (or part of the snapshot) of a
// filesystem.
//
// A Snapshottable filesystem may make use of more than one of these functions, for example a Btrfs
// filesystem may use one function for each subvolume to restore.
//
// This function is similar to the CreateSnapshot(…) function. It does not require a ready channel,
// since restoring is always done one function after the other. They functions will still be run as
// goroutines, pretty much like CreateSnapshot(…) function in sequential mode.
//
// See the documentation of the CreateSnapshot(…) fucntion for details on the parameters.
//
// Note that a RestoreSnapshot(…) function must be able to undo its changes if required, so it must
// not delete the pre-restore filesystem data until the value it read from the rollback channel
// indicated success from all RestoreSnapshot(…) goroutines.
//
// Example function that doesn't do anything (parameters omitted for brevity):
//
//    func RestoreSnapshot (…) {
//
//       defer done.Done ()
//
//       // Restore the snapshot.
//
//       // In case everything is ok:
//       result <- true
//
//       // If there were an error:
//       //
//       //    errors <- someError
//       //    result <- false
//
//       rb := <- rollback
//
//       if rb {
//          // Undo everything.
//       }
//
//    }
//
type RestoreSnapshot func (

   data           SnapshotData,
   result         chan bool,
   rollback       chan bool,
   ignoreErrors   bool,
   errors         chan error,
   done           *sync.WaitGroup,

)

// The Snapshottable interface.
//
type Snapshottable interface {

   // The MountRequired() method must return true if the filesystem has to be mounted before a
   // snapshot can be created/restored etc., otherwise false. This is part of the interface to
   // allow the caller to automatically mount filesystems that do require it, no Snapshot method
   // will mount any filesystem itself.
   //
   MountRequired () bool

   // The Snapshots() method must return a list of snapshot meta data for all snapshots found on
   // the Snapshottable filesystem.
   //
   // This method will be called by the snapshots.Snapshots() function, see there for more
   // information.
   //
   // In case of an error the list must contain data for all snapshots found up to that point, it
   // may be incomplete or nil depending on where the error occurs, and the error set accordingly,
   // otherwise the error must be nil.
   //
   Snapshots () ([] MetaData, error)

   // The Creators() method must return a list of CreateSnapshot(…) functions that create the
   // snapshot(s) for the Snapshottable filesystem.
   //
   // See the CreateSnapshot(…) documentation for more information.
   //
   // In case of an error the list must contain all functions gathered up to that point, it may be
   // incomplete or nil depending on where the error occurs, and the error set accordingly,
   // otherwise the error must be nil.
   //
   Creators (data SnapshotData) ([] CreateSnapshot, error)

   // The Restorers() method must return a list of RestoreSnapshot(…) functions that restore the
   // snapshot(s) for the Snapshottable filesystem.
   //
   // See the RestoreSnapshot(…) documentation for more information.
   //
   // In case of an error the list must contain all functions gathered up to that point, it may be
   // incomplete or nil depending on where the error occurs, and the error set accordingly,
   // otherwise the error must be nil.
   //
   Restorers (data SnapshotData) ([] RestoreSnapshot, error)

   // The Delete(…) method must delete the snapshot from the filesystem.
   //
   // Contrary to the other methods, this is a straight forward method, called directly by the
   // Snapshot if it should be deleted. The snapshot to delete is specified by its SnapshotData
   // (the first parameter). If ignoreErrors is true, non-fatal errors should not cause the method
   // to return. Any error that occurs must be sent to the errChan (third parameter).
   //
   // There is no rollback option, once started deletion may be final up to point when an error is
   // encountered that can't be ignored (implementations are free to implement their own rollback
   // if desired). The return value of this method must be true on success, false if any error
   // (even if it has been ignored) occured.
   //
   Delete (data SnapshotData, ignoreErrors bool, errChan chan error) bool

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————