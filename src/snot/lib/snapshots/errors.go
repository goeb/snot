// Error types for the snapshots package.

package snapshots

//—————————————————————————————————————————————————————————————————————————————————————————————————

import "fmt"

//—————————————————————————————————————————————————————————————————————————————————————————————————

// A FieldMismatchError may be returned by MetaData.MergeStored(…) when two fields that should be
// equal differ.
//
type FieldMismatchError struct {
   ID1 string
   ID2 string
}

// The Error() method returns the error message.
//
func (err FieldMismatchError) Error () string {
   return fmt.Sprintf ("Meta data mismatch for snapshot %s/%s.", err.ID1, err.ID2)
}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// An InvalidTimestampError will be returned if a timestamp didn't match the required format.
//
type InvalidTimestampError struct {
   timestamp string
}

// The Error() method returns the error message.
//
func (err InvalidTimestampError) Error () string {
   return fmt.Sprintf ("Invalid timestamp: %s.", err.timestamp)
}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————