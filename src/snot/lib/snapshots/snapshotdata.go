// The SnapshotData type and the Data() method and Data(…) function.

package snapshots

//—————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "strconv"
   "time"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The SnapshotData type.
//
// Most fields are derived from the snapshot's creation time. The main purpose of this struct is to
// allow the use of those data in templates (though numeric values are integer types and have to be
// padded with zeroes manually if required). Since this struct contains pretty much all of the data
// required by other packages, the Snapshot type does not define any getters for its data, and this
// type is used to access it in most places.
//
// Note that the Created field is a time.Time value with the location set to UTC, while the other
// time fields (e.g. Year, Hour etc.) are derived from the local time! Timestamps (UnixTime etc.)
// and the ID (derived from the timestamp) are independent from the location.
//
type SnapshotData struct {

   Created           time.Time         // Creation time, UTC.

   Year              int               // Year.
   Month             int               // Month, January = 1 … December = 12.
   Day               int               // Day, 1 … 31.

   Hour              int               // Hour, 0 … 23.
   Minute            int               // Minute.
   Second            int               // Second.
   Nanosecond        int               // Nanosecond.

   Week              int               // ISO 8601 week number, 1 … 53.

   DayOfWeek         int               // Day of the week, Sunday = 0 … Saturday = 6.
   DayOfYear         int               // Day of the year, 1 … 365, or 1 … 366 for leap years.

   UnixTime          int64             // Seconds since January 1, 1970 UTC.
   UnixNano          int64             // Nanoseconds since January 1, 1970 UTC.

   NameOfDay         string            // English name of the day.
   NameOfMonth       string            // English name of the month.

   Timezone          string            // Abbreviated timezone name.
   Offset            int               // Timezone offset in seconds.
   OffsetStr         string            // Timezone offset as string (e.g. "+0200").
   Location          string            // Timezone information.

   TS                string            // UnixNano timestamp as string.
   ID                string            // FNV hash of the timestamp.

   Description       string            // Expanded description.
   Template          string            // Description template.

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Data() method returns the SnapshotData struct for the snapshot.
//
// If the snapshot's description is an empty string, ExpandTemplate(…) will be used to get the
// description (unless the template is empty, too). Note: If an error occurs during template
// processing, the description will be left an empty string and the error will be ignored.
//
func (snot *Snapshot) Data () SnapshotData {

   data := Data (snot.created)

   data.Description = snot.description
   data.Template    = snot.descriptionTemplate

   if data.Description == "" && data.Template != "" {
      data.Description, _ = ExpandTemplate (data.Template, data)
   }

   return data

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Data(…) function returns a SnapshotData struct based on the supplied time.
//
// Most of the fields of a SnapshotData struct are derived from the snapshot's creation time. This
// function will use the specified time and create a SnapshotData struct based on it. All fields
// that cannot be derived from the time will be left uninitialized with their zero value.
//
func Data (created time.Time) SnapshotData {

   loc := created.Local ()
   utc := created.UTC   ()

   year, month, day := loc.Date       ()
   hour, min,   sec := loc.Clock      ()
   nsec             := loc.Nanosecond ()
   year, week       := loc.ISOWeek    ()
   wday             := loc.Weekday    ()
   yday             := loc.YearDay    ()
   unix             := loc.Unix       ()              // Same as utc.Unix().
   nano             := loc.UnixNano   ()              // Same as utc.UnixNano().
   zone, offset     := loc.Zone       ()
   lct              := loc.Location   ()
   offstr           := loc.Format     ("-07:00")

   un               := strconv.FormatInt (nano, 10)
   id               := FNV32aHash (un)

   data := SnapshotData {

      Created     : utc,

      Year        : year,
      Month       : int (month),
      Day         : day,

      Hour        : hour,
      Minute      : min,
      Second      : sec,
      Nanosecond  : nsec,

      Week        : week,

      DayOfWeek   : int (wday),
      DayOfYear   : yday,

      UnixTime    : unix,
      UnixNano    : nano,

      NameOfDay   : wday.String (),
      NameOfMonth : month.String (),

      Timezone    : zone,
      Offset      : offset,
      OffsetStr   : offstr,
      Location    : lct.String (),

      TS          : un,
      ID          : id,

   }

   return data

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————