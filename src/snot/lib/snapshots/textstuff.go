// Some functions dealing with text and stuff.

package snapshots

//—————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "bytes"
   "fmt"
   "hash/fnv"
   "strconv"
   "text/template"
   "time"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The ExpandTemplate(…) function applies the data to the template and returns the result.
//
// If there is any error, the returned string will be empty and the error will be set, on success
// the string will contain the result and error will be nil. See text/template for details on the
// template format, the delimeters are set to "<" and ">" instead of the default "{{" and "}}".
//
// Note: The template name will always be set to an empty string.
//
func ExpandTemplate (tmpl string, data interface {}) (string, error) {

   t, err := template.New ("").Delims ("<", ">").Parse (tmpl)

   if err != nil {
      return "", err
   }

   res := new (bytes.Buffer)

   if err := t.Execute (res, data) ; err != nil {
      return "", err
   }

   return res.String (), nil

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The FNV32aHash(…) function returns the FNV-1a 32 bit hash of the specified text.
//
// The hash will be returned as hexadecimal string.
//
func FNV32aHash (text string) string {

   hash := fnv.New32a ()
   hash.Write ([] byte (text))

   return fmt.Sprintf ("%08x", hash.Sum32 ())

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The TimestampToTime(…) function converts the specified timestamp to a Time instance.
//
// The timestamp must be the string representation (base 10) of the int64 value that would be
// returned by the Time.UnixNano() method for the converted Time.
//
// On success the function will return the Time and nil, otherwise the return value will be a zero
// Time and the error.
//
func TimestampToTime (timestamp string) (time.Time, error) {

   var err  error
   var secs int64
   var nano int64
   var t    time.Time

   timestamp = fmt.Sprintf ("%010s", timestamp)

   l := len (timestamp)

   if secs, err = strconv.ParseInt (timestamp [:l-9], 10, 64) ; err != nil { return t, err }
   if nano, err = strconv.ParseInt (timestamp [l-9:], 10, 64) ; err != nil { return t, err }

   return time.Unix (secs, nano), nil

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————