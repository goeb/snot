// The subcmd package manages a command line application's subcommands.
//
// This is meant for applications called as: <command> <subcommand> [options]
//
// Put subcommand code in its own package, and register it (e.g. in init()):
//
//    package subcommand
//
//    import "snot/lib/subcmd"
//
//    func subcommandMain () (int, error) {
//       // subcommand code goes in here…
//       return 0, nil
//    }
//
//    func init() {
//       // Simple example without any command line options:
//       subcmd.Instance ().Add ("subcommand", subcommandMain, nil)
//    }
//
// In the main package, call the Run(…) method:
//
//    func main () {
//       exitCode, err := subcmd.Instance ().Run (nil, os.Args [1:]...)
//       // Error handling…
//    }
//
// The subcmd.Instance() function will return the same instance every time it is called (it is
// thread-safe), but instances can also be created manually if required, see New().
//
package subcmd

//—————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "flag"
   "fmt"
   "os"
   "sort"
   "sync"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Subcommmands type stores the subcommands (a set of subcommand instances) of the application.
//
type Subcommands struct {

   args     [] string                        // Remaining command line options after parsing.
   commands map [string] *Subcommand         // Individual subcommands by name.
   lock     sync.Mutex                       // For locking.

}

// The subcommand type stores a single subcommand: its name, callback and command line options.
//
type Subcommand struct {

   flags    *flag.FlagSet                    // Command line options of the subcommand.
   main     func () (int, error)             // Callback function of the subcommand.
   name     string                           // Name of the subcommand.

}

// A ConfigCallback function may be used with Run(…) to dynamically add command line options to the
// option list before parsing. The function will receive the subcommand name as its parameter (this
// is already verified, i.e. a subcommand by that name does exist). It must return a string list
// containing the options to add and nil on success. In case of an error it has to be returned as
// second return value, the first return value will be ignored in this case. An error in a callback
// function will cause Run(…) to fail. The options returned by this function will be added before
// the options specified for Run(…) directly.
//
type ConfigCallback func (string) ([] string, error)

//—————————————————————————————————————————————————————————————————————————————————————————————————

var (

   instance *Subcommands                     // Global subcommands instance.
   once     sync.Once                        // To initialize the global instance on demand.

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The New() function initializes a new Subcommands instance and returns it.
//
func New () *Subcommands {

   return &Subcommands {
      args     : nil,
      commands : make (map [string] *Subcommand),
      lock     : sync.Mutex {},
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Instance() function provides access to a "global" subcommands instance.
//
// The instance will be created on demand, i.e. when Instance() is called for the first time. Once
// created, Instance() will always return the same instance. This is thread-safe. Using this is the
// recommended way to use the subcmd package.
//
func Instance () *Subcommands {

   once.Do (func () {
      instance = New ()
   })

   return instance

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Lock() method locks the subcommands instance.
//
// It may be used together with the UnlockedAdd(…) function if the caller wants to take care of
// locking itself. Add(…) will perform the locking itself and Lock() must not be used with it!
//
func (subcmds *Subcommands) Lock () {
   subcmds.lock.Lock ()
}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Unlock() method unlocks the subcommands instance. See Lock() for more details.
//
func (subcmds *Subcommands) Unlock () {
   subcmds.lock.Unlock ()
}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Add(…) method registers a new subcommand.
//
// Parameters are (in this order): the name of the subcommand, the subcommand's main function, the
// subcommand's command line options. The subcommand's name must be unique, if a subcommand by the
// specified name has been registered already Add(…) does nothing (and returns an error). It is
// this name that identifies the subcommand on the command line. The options parameter may be nil
// if the subcommand does not support any additional options.
//
// Add(…) will lock the instance during its execution, see UnlockedAdd(…) if this is not desired.
//
// Returns nil if the command could be registered, or an error if something went wrong.
//
func (subcmds *Subcommands) Add (

   command     string,
   callback    func () (int, error),
   options     *flag.FlagSet,

) error {

   subcmds.Lock ()
   defer subcmds.Unlock ()

   return subcmds.UnlockedAdd (command, callback, options)

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The UnlockedAdd(…) method registers a new subcommand without locking the instance, otherwise
// this is the same as Add(…), see above for details.
//
func (subcmds *Subcommands) UnlockedAdd (

   command     string,
   callback    func () (int, error),
   options     *flag.FlagSet,

) error {

   if _, exists := subcmds.commands [command] ; exists {
      return SubcommandAlreadyRegisteredError { command }
   }

   subcmds.commands [command] = &Subcommand {
      flags : options,
      main  : callback,
      name  : command,
   }

   return nil

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Registered() method returns a list of registered subcommand names, sorted alphabetically.
//
func (subcmds *Subcommands) Registered () [] string {

   names := make ([] string, len (subcmds.commands))

   i := 0
   for name := range subcmds.commands {
      names [i] = name
      i ++
   }

   sort.Strings (names)

   return names

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Parse the options and run the requested subcommand.
//
// The first parameter must be a ConfigCallback function, or nil if none is required. See the
// ConfigCallback documentation for more details. The callback will not be executed if there are
// no flags set for the subcommand.
//
// The remaining parameters must be the options to parse, with the name of the subcommand as first
// option, followed by the subcommand's options (e.g. the command line options: os.Args[1:]).
// Options returned by the callback will be prepended to these options (i.e. inserted after the
// subcommand name).
//
// If the subcommand exists, its main function (as specified via Add(…)) will be executed and the
// return values of this function will be returned. Note that in this case the first return value
// of run will always be a positive number or zero.
//
// In case of an error in Run(…), the first return value will be a negative number, the second
// return value will specify the error. The error codes are:
//
//    * -1 - No options specified, i.e. no subcommand specified.
//    * -2 - The specified subcommand does not exist.
//    * -3 - The config callback function returned an error.
//    * -4 - There was an error parsing the remaining options (except ErrHelp).
//
// In each of these cases the subcommand's callback function will not be executed! If -h or --help
// are not defined for a subcommand but specified in the options, the help message will be printed
// to os.Stderr and the method returns 0, nil. Note that in this help message os.Args[0] will be
// used as the command name, followed by the first method parameter as subcommand name.
//
// Note: The subcommands instance will be locked when this method is executed, and only unlocked
// once it is finished, i.e. it will be locked during the execution of the subcommand handler!
//
func (subcmds *Subcommands) Run (cfg ConfigCallback, opts ... string) (int, error) {

   var err     error
   var cfgOpts [] string

   if len (opts) < 1 {
      return -1, NoSubcommandSpecifiedError {}
   }

   subcmds.Lock ()
   defer subcmds.Unlock ()

   command, exists := subcmds.commands [opts [0]]

   if ! exists {
      return -2, UnknownSubcommandError { opts [0] }
   }

   if command.flags != nil {

      if cfg != nil {
         if cfgOpts, err = cfg (opts [0]) ; err != nil {
            return -3, err
         }
      }

      command.flags.Usage = func () {}

      if err := command.flags.Parse (append (cfgOpts, opts [1:] ...)) ; err != nil {
         if err == flag.ErrHelp {
            fmt.Fprintf (os.Stderr, "Usage of %s %s:\n\n", os.Args [0], opts [0])
            command.flags.PrintDefaults ()
            return 0, nil
         }
         return -4, err
      }

      subcmds.args = command.flags.Args ()

   } else {

      subcmds.args = opts [1:]

   }

   exitCode, error := command.main ()

   if (exitCode < 0) {
      exitCode = - exitCode
   }

   return exitCode, error

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Args() method returns the remaining command line parameters after parsing.
//
// Run(…) has to be called before the parameters are available. They will be initialized before
// calling the subcommand handler, so calling Args() from within the handler will work. If the
// subcommand does not have any command line options, Args() will contain all the strings passed
// to the Run(…) method (except the first, i.e. except the subcommand name). If Run(…) has never
// been called the return value of Args() will be nil.
//
func (subcmds *Subcommands) Args () [] string {
   return subcmds.args
}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————