// Error types and variables for the subcmd package.

package subcmd

//—————————————————————————————————————————————————————————————————————————————————————————————————

import "fmt"

//—————————————————————————————————————————————————————————————————————————————————————————————————

// A NoSubcommandSpecifiedError will be returned by Run(…) if the option list is empty.
//
type NoSubcommandSpecifiedError struct {}

// The Error() method returns the error message.
//
func (err NoSubcommandSpecifiedError) Error () (string) {
   return "No subcommand specified."
}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// A SubcommandAlreadyRegisteredError will be returned when trying to register a command twice.
//
type SubcommandAlreadyRegisteredError struct {
   command string
}

// The Error() method returns the error message.
//
func (err SubcommandAlreadyRegisteredError) Error () (string) {
   return fmt.Sprintf ("Subcommand %s already registered.", err.command)
}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// An UnknownSubcommandError will be returned by Run(…) if the command has not been registered.
//
type UnknownSubcommandError struct {
   command string
}

// The Error() method returns the error message.
//
func (err UnknownSubcommandError) Error () (string) {
   return fmt.Sprintf ("Unknown subcommand: %s.", err.command)
}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————