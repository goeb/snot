// The intervals package provides methods to deal with time intervals, i.e. time periods with a
// fixed start and end time.
//
package interval

//—————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "regexp"
   "time"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Interval type represents a single time interval, with a fixed start and end time.
//
type Interval  struct {

   start       time.Time      // Start time, may be the zero value for -∞.
   stop        time.Time      // End time, may be the zero value for +∞.

   inverted    bool           // Whether Match(…) returns false for values inside the interval.

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The canonical format we use for specifying date/time as string. See ParseDateTime(…) for more
// details.
//
const dateTimeFormat        = "2006-01-02-15-04-05"

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Matches all separators that may be used in a date/time string.
//
var dateSeps *regexp.Regexp = regexp.MustCompile ("[ .:/_-]")

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The ParseDateTime(…) function parses the specified string as a date. The specified dateTime must
// be in one of the following formats (with "-" being any of the separators listed below):
//
//    * YYYY-MM-DD-hh-mm-ss - e.g. "2006-01-02 15:04:05"
//    * YYYY-MM-DD-hh-mm
//    * YYYY-MM-DD
//    * YYYY-MM
//    * YYYY
//
//    * an empty string will result in a zero time.Time to be returned (i.e. this is not an error)
//
// Separator characters are " " (space), ".", ":", "/", "-" and "_". Everywhere a separator is
// found in the formats above, exactly one of those must be used, it does not matter which one.
//
// Missing information will either be filled in with the minimum value for the field (e.g. zero for
// minutes, one for month etc.) if the fullPeriod parameter is false, or with the maximum possible
// value if fullPeriod is true (nanosecond granularity; dates will always be valid). For example:
//
//    * with fullPeriod = false, "2020-02" will be "2020-02-01 00:00:00.000000000"
//    * with fullPeriod = true,  "2020-02" will be "2020-02-29 23:59:59.999999999"
//
// The specified time will always be interpreted as local time (and any switch from or to daylight
// saving will also be considered).
//
// In case of an error (e.g. when the specified string can not be parsed), this function returns a
// zero time.Time and the error, on success the time.Time resulting from the parsing (which may be
// the zero value if dateTime is an empty string) and nil.
//
func ParseDateTime (dateTime string, fullPeriod bool) (time.Time, error) {

   if dateTime == "" {
      return time.Time {}, nil
   }

   format := dateTimeFormat
   switch len (dateTime) {
      case 19, 16, 10, 7, 4:
         format = format [:len (dateTime)]
      default:
         return time.Time {}, ParseError { dateTime }
   }

   t, err := time.ParseInLocation (format, dateSeps.ReplaceAllString (dateTime, "-"), time.Local)

   if err != nil {
      return time.Time {}, err
   }

   if fullPeriod {
      switch len (dateTime) {
         case 19: t = t.Add (time.Second)
         case 16: t = t.Add (time.Minute)
         case 10: t = t.AddDate (0, 0, 1)
         case  7: t = t.AddDate (0, 1, 0)
         case  4: t = t.AddDate (1, 0, 0)
      }
      t = t.Add (- time.Nanosecond)
   }

   return t, nil

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The New(…) function initializes a new Interval based on the specified limits. It returns the
// interval and nil on success, on failure the zero Interval and the error.
//
// Both start and stop parameters must be in the format specified for ParseDateTime(…), or empty
// strings (to set the start or stop time to ±∞). For the start time, ParseDateTime(…) will be
// called with the fullPeriod parameter set to false, for the stop time it will be set to true.
//
// If after parsing the start time is after the stop time, a call to Match(…) will return true if
// the time to check is either before the specified stop time, or after the specified start time
// (i.e. it will return true if the value is outside of [stop, start]).
//
func New (start string, stop string) (Interval, error) {

   tStart, err := ParseDateTime (start, false)
   if err != nil {
      return Interval {}, err
   }

   tStop, err := ParseDateTime (stop, true)
   if err != nil {
      return Interval {}, err
   }

   inverted := false
   if start != "" && stop != "" && tStart.After (tStop) {
      tStart, tStop, inverted = tStop.Add (time.Nanosecond), tStart.Add (- time.Nanosecond), true
   }

   return Interval { start : tStart, stop : tStop, inverted : inverted }, nil

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Match(…) method returns true if the specified time is inside the Interval. Both the start
// and end times (if set) are included, if a time is not set there is no upper or lower limit. See
// New(…) for some more information.
//
func (interval Interval) Match (t time.Time) bool {

   result := false

   if (interval.start.IsZero () || (interval.start.Before (t) || interval.start.Equal (t))) &&
      (interval.stop.IsZero  () || (interval.stop.After   (t) || interval.stop.Equal  (t))) {
      result = true
   }

   return result != interval.inverted

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————