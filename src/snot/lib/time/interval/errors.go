// Error types for the interval package.

package interval

//—————————————————————————————————————————————————————————————————————————————————————————————————

import "fmt"

//—————————————————————————————————————————————————————————————————————————————————————————————————

// A ParseError will be returned if the specified time string could not be parsed.
//
type ParseError struct {
   time string
}

// The Error() method returns the error message.
//
func (err ParseError) Error () string {
   return fmt.Sprintf ("Could not parse date/time string: %s.", err.time)
}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————