// Error types for the Btrfs package.

package btrfs

//—————————————————————————————————————————————————————————————————————————————————————————————————

import "fmt"

//—————————————————————————————————————————————————————————————————————————————————————————————————

// An InvalidPathError will be returned when trying to access a path outside the Btrfs filesystem,
// or if the path is not a well-formed path.
//
type InvalidPathError struct {
   path string
}

// The Error() function returns the error message.
//
func (err InvalidPathError) Error () string {
   return fmt.Sprintf ("Invalid path: %s.", err.path)
}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// A NotMountedError occurs when the a filesystem is not mounted when it should be.
//
type NotMountedError struct {
   UUID string
}

// The Error() function returns the error message.
//
func (err NotMountedError) Error () string {
   return fmt.Sprintf ("Filesystem not mounted: %s.", err.UUID)
}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————