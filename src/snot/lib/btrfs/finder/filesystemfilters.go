// Predefined filesystem filters.

package finder

//—————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "bufio"
   "os"
   "strconv"
   "strings"

   "snot/lib/btrfs"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The MountedOnlyFilter(…) function returns a FilesystemFilter that only matches filesystem's with
// a device listed in the specified mounts file.
//
// Format of the mounts file must be like /proc/mounts, i.e. the first field has to be the device,
// followed by a space, remaining fields are ignored.
//
// If there is an error processing the mounts file, the filter function returned will exclude every
// filesystem!
//
func MountedOnlyFilter (mounts string) FilesystemFilter {

   file, err := os.Open (mounts)

   if err != nil {
      return func (*btrfs.Btrfs) (bool, error) { return false, err }
   }

   defer file.Close ()

   scanner := bufio.NewScanner (file)
   devices := make (map [string] bool)

   for scanner.Scan () {

      line := scanner.Text ()

      if ! strings.HasPrefix (line, "/") {
         continue
      }

      dev := strings.SplitN (line, " ", 2) [0]

      // Quoting getmntent(3), assuming /proc/mounts is the same:
      //
      // Since fields in the mtab and fstab files are separated by whitespace, octal escapes are
      // used to represent the characters space (\040), tab (\011), newline (\012), and backslash
      // (\\) in those files when they occur in one of the four strings in a mntent structure. The
      // routines addmntent() and getmntent() will convert from string representation to escaped
      // representation and back. When converting from escaped representation, the sequence \134 is
      // also converted to a backslash.
      //
      // The strconv.Unquote(…) function should convert these escape sequences correctly in
      // double-quoted strings. If Unquote(…) fails the device will be skipped.

      if dev, err = strconv.Unquote (`"` + dev + `"`) ; err != nil {
         continue
      }

      devices [dev] = true

   }

   if err := scanner.Err () ; err != nil {
      return func (*btrfs.Btrfs) (bool, error) { return false, err }
   }

   if err := file.Close () ; err != nil {
      return func (*btrfs.Btrfs) (bool, error) { return false, err }
   }

   // The actual FilesystemFilter function:

   return func (fs *btrfs.Btrfs) (bool, error) {

      for _, dev := range fs.Devices () {
         if devices [dev] {
            return true, nil
         }
      }

      return false, nil

   }

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————