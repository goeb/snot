// The BtrfsFinder type, an FSFinder implementation.
//
package finder

//—————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "snot/lib/btrfs"
   "snot/lib/btrfs/backend"
   "snot/lib/btrfs/backend/btrfstool/factory"
   "snot/lib/fs/mounter"
   "snot/lib/multierror"
   "snot/lib/pathutils"
   "snot/lib/snapshots"
   "snot/lib/snapshots/fsfinder"
   "snot/lib/snapshots/metadata"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The BtrfsFinder type implements the FSFinder interface to search for Btrfs filesystems.
//
type BtrfsFinder struct {

   backend     backend.BtrfsBackend          // The BtrfsBackend to use for the finder.
   factory     backend.BtrfsBackendFactory   // The factory to create new Btrfs backend(s).
   mounter     mounter.MounterFactory        // To create Mounters for the new Btrfs instances.
   putils      pathutils.PathUtilsFactory    // To create PathUtils for the new Btrfs instances.
   mstorage    metadata.MetaStorageFactory   // To create MetaStorages for the new Btrfs instances.

   filter      string                        // Look for this specific filesystem if not empty.

   liveDir     string                        // Source subvolume directory.
   snapDir     string                        // Target snapshot base directory.
   rw          bool                          // Whether to create writable snapshots.
   keepLive    bool                          // Whether to keep live subvolumes after restore.

   fsFilters   [] FilesystemFilter           // Filesystem filters for the finder.
   ssFilters   [] btrfs.SnapshotFilter       // Snapshot filters for the new Btrfs instances.
   svFilters   [] btrfs.SubvolumeFilter      // Subvolume filters for the new Btrfs instances.

   metaConfig  btrfs.MetaStorageConfig       // MetaStorage configuration function.

}

// The BtrfsFinderOption type defines the signature for the constructor's option functions.
//
type BtrfsFinderOption func (*BtrfsFinder) error

// A FilesystemFilter function is used to filter (exclude) filesystems.
//
// The function will receive the Btrfs instance as parameter.
//
// It must return a false value if the filesystem should be excluded from the list returned by the
// Get() method as the first return value, or true if it should be included. If multiple filters
// are set for the finder, the first false return value is final and the filesystem will be
// excluded. The second return value must be nil if no error occurred, otherwise the error. Note
// that an error will also cause the filesystem to be excluded.
//
type FilesystemFilter func (*btrfs.Btrfs) (bool, error)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Check if BtrfsFinder implements the FSFinder interface (compile-time check).
//
var _ fsfinder.FSFinder = (*BtrfsFinder) (nil)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The NewBtrfsFinder(…) function creates a new BtrfsFinder instance.
//
// Available options are:
//
//    * WithBtrfsBackend         (…) - Set the BtrfsBackend used by the BtrfsFinder.
//    * WithBtrfsBackendFactory  (…) - Set the BtrfsBackendFactory for the new Btrfs instances.
//    * WithFilesystem           (…) - Search for a specific filesystem by label/UUID/device.
//    * WithFilesystemFilters    (…) - Set the filesystem filters for the finder.
//    * WithLiveDir              (…) - Set the subvolume source directory.
//    * WithMetaStorageFactory   (…) - Set the factory to create MetaStorages for the Btrfs.
//    * WithMounterFactory       (…) - Set the factory instance to create Mounters for the Btrfs.
//    * WithPathUtilsFactory     (…) - Set the factory instance to create PathUtils for the Btrfs.
//    * WithSnapshotDir          (…) - Set the snapshot base directory.
//    * WithSnapshotFilters      (…) - Set the snapshot filters for the new Btrfs instances.
//    * WithSubvolumeFilters     (…) - Set the subvolume filters for the new Btrfs instances.
//    * WithWritable             (…) - Whether to create writable snapshots.
//
// See the individual option functions for more details.
//
// If no BtrfsBackendFactory is set, it will be set to a new BtrfsToolFactory (with no options,
// i.e. using the BtrfsToolFactory defaults). If no BtrfsBackend is set, the factory will be used
// to create one.
//
// Unless stated otherwise, when using the same option multiple times the last one will override
// preceding options.
//
// NewBtrfsFinder(…) returns the instance and nil on success, or nil and an error on failure.
//
func NewBtrfsFinder (options ... BtrfsFinderOption) (*BtrfsFinder, error) {

   bf := &BtrfsFinder {
      fsFilters : make ([] FilesystemFilter,      0),
      ssFilters : make ([] btrfs.SnapshotFilter,  0),
      svFilters : make ([] btrfs.SubvolumeFilter, 0),
   }

   for _, option := range options {
      if option == nil {
         continue
      }
      if err := option (bf) ; err != nil {
         return nil, err
      }
   }

   if bf.factory == nil {
      if fact, err := factory.NewBtrfsToolFactory () ; err == nil {
         bf.factory = fact
      } else {
         return nil, err
      }
   }

   if bf.backend == nil {
      if bend, err := bf.factory.NewBtrfsBackend () ; err == nil {
         bf.backend = bend
      } else {
         return nil, err
      }
   }

   return bf, nil

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithBtrfsBackend(…) as parameter to NewBtrfsFinder(…) to set the BtrfsBackend for the
// instance.
//
// If no backend is set the constructor will use the backend factory to create one, see
// WithBtrfsBackendFactory(…).
//
func WithBtrfsBackend (backend backend.BtrfsBackend) BtrfsFinderOption {

   return func (bf *BtrfsFinder) error {
      bf.backend = backend
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithBtrfsBackendFactory(…) as parameter to NewBtrfsFinder(…) to set the BtrfsBackendFactory
// for the instance.
//
// If no backend is set the constructor will set it to a new BtrfsToolFactory instance (with its
// default options).
//
// The BtrfsBackendFactory set with this option for the BtrfsFinder will also be used by all Btrfs
// instances created by this finder. The BtrfsBackend of a Btrfs instance create by this finder
// will be created by this factory, too (which matches the default behaviour of NewBtrfs(…) if no
// backend is set).
//
func WithBtrfsBackendFactory (factory backend.BtrfsBackendFactory) BtrfsFinderOption {

   return func (bf *BtrfsFinder) error {
      bf.factory = factory
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithPathUtilsFactory(…) as parameter to NewBtrfsFinder(…) to set the PathUtilsFactory.
//
// If no PathUtilsFactory is set the Btrfs instances created by Get() will use the default
// PathUtils implementation, see fs.NewFilesystem(…).
//
func WithPathUtilsFactory (factory pathutils.PathUtilsFactory) BtrfsFinderOption {

   return func (bf *BtrfsFinder) error {
      bf.putils = factory
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithMounterFactory(…) as parameter to NewBtrfsFinder(…) to set the MounterFactory.
//
// If no MounterFactory is set the Btrfs instances created by Get() will use the default Mounter
// implementation, see fs.NewFilesystem(…).
//
func WithMounterFactory (factory mounter.MounterFactory) BtrfsFinderOption {

   return func (bf *BtrfsFinder) error {
      bf.mounter = factory
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithMetaStorageFactory(…) as parameter to NewBtrfsFinder(…) to set the MetaStorageFactory.
//
// If no MetaStorageFactory is set the Btrfs instances created by Get() will use the default
// MetaStorage implementation, see btrfs.NewBtrfs(…).
//
func WithMetaStorageFactory (factory metadata.MetaStorageFactory) BtrfsFinderOption {

   return func (bf *BtrfsFinder) error {
      bf.mstorage = factory
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithMetaStorageConfig(…) as parameter to NewBtrfsFinder(…) to set the MetaStorage
// configuration function.
//
// If this is not set the Btrfs instances will use btrfs.MetaStorageConfigSnapshotData. See
// btrfs.NewBtrfs(…).
//
func WithMetaStorageConfig (config btrfs.MetaStorageConfig) BtrfsFinderOption {

   return func (bf *BtrfsFinder) error {
      bf.metaConfig = config
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithFilesystem(…) as parameter to NewBtrfsFinder(…) to search for a specific filesystem.
//
// The parameter must specify the label or UUID of the filesystem, or one of the devices it is
// located on. Setting the parameter to an empty string will be treated as if this options has not
// been specified.
//
func WithFilesystem (filter string) BtrfsFinderOption {

   return func (bf *BtrfsFinder) error {
      bf.filter = filter
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithFilesystemFilters(…) as parameter to NewBtrfsFinder(…) to add filesystem filters.
//
// Filters will be added in the order they are specified. Using this option multiple times is
// allowed, filters will be appended to the existing list.
//
func WithFilesystemFilters (filters ... FilesystemFilter) BtrfsFinderOption {

   return func (bf *BtrfsFinder) error {
      bf.fsFilters = append (bf.fsFilters, filters ...)
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithKeepLiveSubvolumes(…) as parameter to NewBtrfsFinder(…) to set whether the old live
// subvolumes will be deleted on restore or not.
//
// See btrfs.WithKeepLiveSubvolumes(…) for more information.
//
func WithKeepLiveSubvolumes (keep bool) BtrfsFinderOption {

   return func (bf *BtrfsFinder) error {
      bf.keepLive = keep
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithLiveDir(…) as parameter to NewBtrfsFinder(…) to set the source subvolume directory.
//
// See btrfs.WithLiveDir(…) for more information.
//
func WithLiveDir (dir string) BtrfsFinderOption {

   return func (bf *BtrfsFinder) error {
      bf.liveDir = dir
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithSnapshotDir(…) as parameter to NewBtrfsFinder(…) to set the snapshot base directory.
//
// See btrfs.WithSnapshotDir(…) for more information.
//
func WithSnapshotDir (dir string) BtrfsFinderOption {

   return func (bf *BtrfsFinder) error {
      bf.snapDir = dir
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithWritable(…) as parameter to NewBtrfsFinder(…) to set snapshot writability.
//
// See btrfs.WithWritable(…) for more information.
//
func WithWritable (rw bool) BtrfsFinderOption {

   return func (bf *BtrfsFinder) error {
      bf.rw = rw
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithSnapshotFilters(…) as parameter to NewBtrfsFinder(…) to add snapshot filters.
//
// See btrfs.WithSnapshotFilters(…) for more information.
//
func WithSnapshotFilters (filters ... btrfs.SnapshotFilter) BtrfsFinderOption {

   return func (bf *BtrfsFinder) error {
      bf.ssFilters = append (bf.ssFilters, filters ...)
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithSubvolumeFilters(…) as parameter to NewBtrfsFinder(…) to add subvolume filters.
//
// See btrfs.WithSubvolumeFilters(…) for more information.
//
func WithSubvolumeFilters (filters ... btrfs.SubvolumeFilter) BtrfsFinderOption {

   return func (bf *BtrfsFinder) error {
      bf.svFilters = append (bf.svFilters, filters ...)
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Get() method returns a list of Snapshottable Btrfs instances.
//
// A suitable backend has to be set using the constructor. The Btrfs instances returned will use
// the default Mounter and PathUtils implementations unless appropriate factories have been set via
// the constructor. The BtrfsBackendFactory will be set to that of the finder and a backend will be
// created using it.
//
// The list returned may be empty. In case of an error the list may be incomplete or nil (depending
// on where the error occurs) and the error will be set, on success the error will be nil.
//
func (bf *BtrfsFinder) Get () ([] snapshots.Snapshottable, error) {

   mErr := multierror.New ()

   filter := [] string {}
   if bf.filter != "" {
      filter = [] string { bf.filter }
   }

   // GetBtrfsFilesystems(…) may return a list despite an error. Just add any to the MultiError and
   // continue with the (possibly incomplete) filesystem list (the list may be nil, but that does
   // not matter either).

   results, err := bf.backend.GetBtrfsFilesystems (filter ...)

   if err != nil {
      mErr.Append (err)
   }

   filesystems := [] snapshots.Snapshottable {}

   // Any error in the following loop will also just be appended to the MultiError instance, and
   // the loop will continue with the next filesystem. At the end, the list of successfully created
   // Snapshottables will be returned, together with the MultiError (if there was an error, nil
   // otherwise).

   for _, fs := range results {

      backend, err := bf.factory.NewBtrfsBackend ()

      if err != nil {
         mErr.Append (err)
         continue
      }

      opts := [] btrfs.BtrfsOption {
         btrfs.WithBtrfsBackend        (backend         ),
         btrfs.WithBtrfsBackendFactory (bf.factory      ),
         btrfs.WithUUID                (fs [0]          ),
         btrfs.WithLabel               (fs [1]          ),
         btrfs.WithDevices             (fs [2:] ...     ),
         btrfs.WithKeepLiveSubvolumes  (bf.keepLive     ),
         btrfs.WithWritable            (bf.rw           ),
         btrfs.WithSnapshotFilters     (bf.ssFilters ...),
         btrfs.WithSubvolumeFilters    (bf.svFilters ...),
         btrfs.WithMetaStorageConfig   (bf.metaConfig   ),
      }

      if bf.mounter != nil {
         if mounter, err := bf.mounter.NewMounter () ; err != nil {
            mErr.Append (err)
            continue
         } else {
            opts = append (opts, btrfs.WithMounter (mounter))
         }
      }

      if bf.putils != nil {
         if putils, err := bf.putils.NewPathUtils () ; err != nil {
            mErr.Append (err)
            continue
         } else {
            opts = append (opts, btrfs.WithPathUtils (putils))
         }
      }

      if bf.mstorage != nil {
         if mstorage, err := bf.mstorage.NewMetaStorage () ; err != nil {
            mErr.Append (err)
            continue
         } else {
            opts = append (opts, btrfs.WithMetaStorage (mstorage))
         }
      }

      if bf.liveDir != "" { opts = append (opts, btrfs.WithLiveDir     (bf.liveDir)) }
      if bf.snapDir != "" { opts = append (opts, btrfs.WithSnapshotDir (bf.snapDir)) }

      fs, err := btrfs.NewBtrfs (opts ...)

      if err != nil {
         mErr.Append (err)
         continue
      }

      include := true

      for _, filter := range bf.fsFilters {
         if inc, err := filter (fs) ; err == nil && ! inc {
            include = false
            break
         } else if err != nil {
            mErr.Append (err)
            include = false
            break
         }
      }

      if include {
         filesystems = append (filesystems, fs)
      }

   }

   return filesystems, mErr.Get ()

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————