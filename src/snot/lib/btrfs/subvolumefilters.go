// Predefined subvolume filters.

package btrfs

//—————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "os"

   "snot/lib/snapshots"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The ExcludeFileFilter(…) function returns a SubvolumeFilter that excludes a subvolume based on
// the existence of a file (or directory, link…).
//
// The parameter may contain template placeholders for SubvolumeData fields. If the parameter is
// empty, the filter returned will not exclude anything!
//
// Note: When trying to check for the existence of the file any other error than the file not
// existing will cause false and the error to be returned (excluding the subvolume)!
//
func ExcludeFileFilter (template string) SubvolumeFilter {

   if template == "" {
      return func (SubvolumeData) (bool, error) { return true, nil }
   }

   return func (data SubvolumeData) (bool, error) {

      filename, err := snapshots.ExpandTemplate (template, data)

      if err != nil {
         return false, err
      }

      if _, err = os.Stat (filename) ; os.IsNotExist (err) {
         return true, nil
      }

      return false, err

   }

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————