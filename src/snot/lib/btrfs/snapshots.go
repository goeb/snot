// Methods to create/restore/delete Btrfs snapshots (or subvolumes).

package btrfs

//—————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "os"
   "regexp"
   "sync"

   "snot/lib/multierror"
   "snot/lib/snapshots"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// A SnapshotFilter function is used to filter (exclude) snapshots.
//
// The function will receive the btrfs.SnapshotData and snapshots.MetaData as parameters, in that
// order.
//
// It must return a false value if the snapshot should be excluded from the snapshot operation
// (list, delete…) as the first return value, or true if it should be included. If multiple filters
// are set for the filesystem, the first false return value is final and the snapshot will be
// excluded. The second return value must be nil if no error occurred, otherwise the error (an
// error will also exclude the snapshot).
//
// Note: Snapshot discovery is done per filesystem. If one filesystem's filters cause the snapshot
// to be excluded, but another filesystem's filters do not exclude the same snapshot, it will be
// added to the snapshot list. In that case, the snapshot's Snapshottable filesystem list will only
// include those filesystems that did not exclude the snapshot.
//
type SnapshotFilter func (SnapshotData, snapshots.MetaData) (bool, error)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The timestamps are used as snapshot directories, 19 digits. We could narrow it down more to
// exclude some implausible values, but why should we…
//
var timestampRE = regexp.MustCompile (`^[0-9]{19}$`)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Snapshots() method returns a list of snapshot MetaData for snapshots found on this
// filesystem.
//
// The filesystem has to be mounted before calling this method, an error will be returned if it is
// not mounted.
//
// Snapshots are discovered solely by looking at the subdirectories of the configured snapshot
// directory. Every directory with a name in the right format is considered to contain a snapshot,
// and the snapshot's creation time is directly derived from the snapshot's directory name.
//
// On success the meta data list (may be empty) and nil will be returned. On error, the list may be
// nil or incomplete (depending on where the error occured), and the error will be set accordingly.
// For example, if a snapshot is found that has a meta data file, but this file can't be loaded for
// some reason, the snapshot will not be included in the list.
//
func (btrfs *Btrfs) Snapshots () ([] snapshots.MetaData, error) {

   mErr := multierror.New ()

   if btrfs.MountPoint () == "" {
      return nil, NotMountedError { btrfs.UUID () }
   }

   dirs, err := btrfs.PathUtils ().ReadDir (btrfs.Data ().SnapRoot)

   if err != nil {
      mErr.Append (err)
   }

   snaps := make ([] snapshots.MetaData, 0)

   for _, dir := range dirs {

      if ! (dir.IsDir () && timestampRE.MatchString (dir.Name ())) {
         continue
      }

      created, err := snapshots.TimestampToTime (dir.Name ())

      if err != nil {
         mErr.Append (err)
         continue
      }

      snapData   := snapshots.Data (created)
      metaConfig := btrfs.metaConfig (btrfs.SnapshotData (snapData))
      meta, err  := btrfs.metaStorage.Load (snapData, metaConfig)

      if err != nil && ! os.IsNotExist (err) {
         mErr.Append (err)
         continue
      }

      include := true
      for _, filter := range btrfs.ssFilters {
         if inc, err := filter (btrfs.SnapshotData (snapData), meta) ; err == nil && ! inc {
            include = false
            break
         } else if err != nil {
            include = false
            mErr.Append (err)
            break
         }
      }

      if include {
         snaps = append (snaps, meta)
      }

   }

   return snaps, mErr.Get ()

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Creators(…) method returns a CreateSnapshot(…) function for every subvolume of this
// filesystem to create a snapshot of and one more creator function for general filesystem-wide
// preparations.
//
// See PrepareSnapshot(…) and subvolume.CreateSnapshot(…).
//
// On success the method will return the creator list (may be empty) and nil. On error, it will
// return the creator list for a possibly incomplete subvolume list and the error, unless the
// subvolume list was empty, in which case no creators will be returned.
//
func (btrfs *Btrfs) Creators (data snapshots.SnapshotData) ([] snapshots.CreateSnapshot, error) {

   subvolumes, err := btrfs.subvolumes (btrfs.liveDir, false, &data)
   creators        := make ([] snapshots.CreateSnapshot, 0)

   if len (subvolumes) > 0 {

      // The prepared channel will be used to sync the main creator function (see the
      // PrepareSnapshot(…) method) with the subvolumes' creator functions.

      prepared := make (chan bool, len (subvolumes))
      creators = append (creators, btrfs.PrepareSnapshot (prepared, len (subvolumes)))

      for _, sv := range subvolumes {
         creators = append (creators, sv.CreateSnapshot (prepared))
      }

   }

   return creators, err

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The PrepareSnapshot(…) method returns a CreateSnapshot function that prepares the Btrfs
// filesystem for the snapshots.
//
// Actual snapshots will be created by the functions returned by the subvolumes' CreateSnapshot(…)
// method. This method just creates the necessary snapshot directory and stores the meta data.
//
// This method requires the prepared channel, which must have a buffer size equal to the number of
// subvolumes to snapshot, as the first parameter, and the number of subvolumes as the second
// parameter.
//
func (btrfs *Btrfs) PrepareSnapshot (prepared chan bool, svols int) snapshots.CreateSnapshot {

   return func (

      data           snapshots.SnapshotData,    // SnapshotData of the snapshot to create.

      result         chan bool,                 // Whether everything went well.
      rollback       chan bool,                 // Whether to undo everything.

      ignoreErrors   bool,                      // Whether to ignore non-fatal errors.
      errCh          chan error,                // Error channel.

      ready          *sync.WaitGroup,           // Call Done() and Wait() when ready to go.
      done           *sync.WaitGroup,           // Call Done() on exit.

   ) {

      defer done.Done ()

      sData        := btrfs.SnapshotData (data)
      dir          := sData.SnapDir
      dirsCreated  := true
      metaCreated  := true
      metaConfig   := btrfs.metaConfig (sData)
      newDirs, err := btrfs.PathUtils ().MkdirAll (dir, 0700)

      if err == nil {

         // Failure to create the meta data will cause the overall result to be false, but the
         // subvolume snapshots should still be created in case errors should be ignored.

         if err := btrfs.metaStorage.Save (data, metaConfig) ; err != nil {
            errCh <- err
            metaCreated = false
         }

      } else {

         errCh <- err
         dirsCreated = false
         metaCreated = false

      }

      // Signal the subvolume goroutines that we're done, either successful or unsuccessful, so
      // they can start their preparations (or not, on failure). If ignoreErrors is false, meta
      // data creation means everything is ok. If ignoreErrors is true, we only require the
      // directories to be successfully created.

      for i := 1 ; i <= svols ; i ++ {
         prepared <- metaCreated || (ignoreErrors && dirsCreated)
      }

      // Wait for all creator goroutines to be ready (across all filesystems).

      if ready != nil {
         ready.Done ()
         ready.Wait ()
      }

      // Nothing to be done here, actual snapshot creation is left to the subvolume creators. Just
      // send the result from the preparations back to the main method and wait for the other
      // creators' results (on the rollback channel). Note that result expects the overall result,
      // regardless of the ignoreErrors setting. Successful directory and meta data creation is
      // required for this to be true.

      result <- dirsCreated && metaCreated

      rb := <- rollback

      if rb {

         // In case of a rollback, wait for the subvolumes' goroutines to finish their rollback,
         // then delete the meta data (if it has been created before) and remove all newly created
         // directories.

         for i := 1 ; i <= svols ; i ++ {
            _ = <- prepared
         }

         if metaCreated {
            if err := btrfs.metaStorage.Delete (data, metaConfig) ; err != nil {
               errCh <- err
            }
         }

         for _, dir := range newDirs {
            if err := btrfs.PathUtils ().Remove (dir) ; err != nil {
               errCh <- err
               break
            }
         }

      }

      close (prepared)

   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Restorers(…) method returns a RestoreSnapshot(…) function for every subvolume of this
// filesystem to restore.
//
// See subvolume.RestoreSnapshot(…).
//
// On success the method will return the restorer list (may be empty) and nil. On error, it will
// return the restorer list for a possibly incomplete subvolume list and the error.
//
func (btrfs *Btrfs) Restorers (data snapshots.SnapshotData) ([] snapshots.RestoreSnapshot, error) {

   // The subvolumes(…) method may return some subvolumes and an error. Still return all the
   // restorers and the error at the end so snapshots.Restore(…) can ignore the error if requested.

   sData           := btrfs.SnapshotData (data)
   subvolumes, err := btrfs.subvolumes (sData.SnapRel, true, &data)
   restorers       := make ([] snapshots.RestoreSnapshot, 0)

   for _, sv := range subvolumes {
      restorers = append (restorers, sv.RestoreSnapshot ())
   }

   return restorers, err

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Delete(…) method will delete the snapshot's data on this filesystem.
//
// The snapshot to delete is specified by its SnapshotData. The second parameter specifies whether
// errors should be ignored (as far as possible, if ignoreErrors is true), or if deletion should be
// aborted as soon as an error is encountered (if ignoreErrors is false). The third parameter
// specifies a channel to which each error that occurs should be sent to.
//
// The method will return false if there was an error, true otherwise.
//
func (btrfs *Btrfs) Delete (

   data           snapshots.SnapshotData,
   ignoreErrors   bool,
   errChan        chan error,

) bool {

   sData := btrfs.SnapshotData (data)
   ok    := true
   svDel := true

   // Delete all snapshot subvolumes.

   svs, err := btrfs.subvolumes (sData.SnapRel, true, &data)

   if err != nil {
      errChan <- err
      if ! ignoreErrors {
         return false
      }
      ok = false
   }

   for _, sv := range svs {
      err = sv.Delete ()
      if err != nil {
         errChan <- err
         if ! ignoreErrors {
            return false
         }
         ok    = false
         svDel = false
      }
   }

   // Delete the meta data.

   err = btrfs.metaStorage.Delete (data, btrfs.metaConfig (sData))

   if err != nil {
      errChan <- err
      if ! ignoreErrors {
         return false
      }
      ok = false
   }

   // Delete the snapshot's directory tree if we could remove all subvolumes. Don't even try if a
   // subvolume couldn't be removed to avoid deleting any empty directories on any subvolumes.

   if svDel {

      err = btrfs.PathUtils ().RemoveEmptyTree (sData.SnapDir)

      if err != nil {
         errChan <- err
         if ! ignoreErrors {
            return false
         }
         ok = false
      }

   }

   return ok

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————