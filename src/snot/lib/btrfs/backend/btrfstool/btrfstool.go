// The BtrfsTool type, a BtrfsBackend implementation.
//
package btrfstool

//—————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "bufio"
   "regexp"
   "strings"

   "snot/lib/btrfs/backend"
   "snot/lib/btrfs/backend/btrfstool/command"
   "snot/lib/btrfs/backend/btrfstool/command/defaultcmd"
   "snot/lib/snapshots/fsfinder"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The BtrfsTool type implements the BtrfsBackend interface. It uses the `btrfs` command to
// interact with Btrfs filesystems (to be precise, the actual command to run depends on the
// BtrfsCommand interface implementation set for the BtrfsTool instance).
//
type BtrfsTool struct {
   cmd command.BtrfsCommand
}

// The BtrfsToolOption type defines the signature for the constructor's option functions.
//
type BtrfsToolOption func (*BtrfsTool) error

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Make sure we implement the BtrfsBackend interface (compile time check).
//
var _ backend.BtrfsBackend = (*BtrfsTool) (nil)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Matches the lines in the output of the `btrfs subvolume list -os` command.
//
var btrfsSnapshotRE  = regexp.MustCompile (
   `(?i)^ID \d+ gen \d+ cgen \d+ top level \d+ otime [0-9 :-]{19} path (.+)$`,
)

// Matches the first line for a filesystem in the `btrfs filesystem show` output.
//
var btrfsLblRE       = regexp.MustCompile (
   `(?i)^Label:\s+(.*?)\s+uuid:\s+([0-9a-f]{4}(?:[0-9a-f]{4}-){4}[0-9a-f]{12})$`,
)

// Matches the device lines for a filesystem in the `btrfs filesystem show` output.
//
var btrfsDevRE       = regexp.MustCompile (`(?i)^\s+devid\s+.+\s+path\s+(.+)$`)

// Matches all strings starting with whitespace.
//
var startsWithSpace  = regexp.MustCompile (`(?i)^\s+`)

// Matches the lines in the output of the `btrfs subvolume list -o` command.
//
var btrfsSubvolumeRE = regexp.MustCompile (`(?i)^ID \d+ gen \d+ top level \d+ path (.+)$`)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The NewBtrfsTool(…) function creates a new BtrfsTool instance.
//
// Available options are:
//
//    * WithBtrfsCommand (…) - Set the BtrfsCommand used by the BtrfsTool.
//
// See the individual option functions for more details. If no command implementation is set the
// BtrfsDefaultCommand will be used.
//
// Returns the new BtrfsTool instance and nil for the error on success, or nil and the error in
// case of an error.
//
func NewBtrfsTool (options ... BtrfsToolOption) (*BtrfsTool, error) {

   bt := &BtrfsTool {}

   for _, option := range options {
      if option == nil {
         continue
      }
      if err := option (bt) ; err != nil {
         return nil, err
      }
   }

   if bt.cmd == nil {
      if bc, err := defaultcmd.NewBtrfsDefaultCommand () ; err != nil {
         return nil, err
      } else {
         bt.cmd = bc
      }
   }

   return bt, nil

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithBtrfsCommand(…) as parameter to NewBtrfsTool(…) to set the BtrfsCommand for the
// instance.
//
// If no command is set the constructor will set it to a new BtrfsDefaultCommand instance.
//
func WithBtrfsCommand (cmd command.BtrfsCommand) BtrfsToolOption {

   return func (bt *BtrfsTool) error {
      bt.cmd = cmd
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The GetBtrfsFilesystems(…) method searches for Btrfs filesystems and returns information about
// them.
//
// See the BtrfsBackend.GetBtrfsFilesystems(…) interface documentation for details. Specifying more
// than one parameter will cause an error!
//
// Note that if a filesystem is specified it will be used as unmodifed as a parameter for the
// `btrfs filesystem show` command, and a "--" parameter will be added before it to avoid it being
// interpreted as an option. There should be no issues using an unvalidated value, if validation is
// desired it has to be done by the caller.
//
// Every error encountered will abort the search and return nil or the list of filesystems found up
// to that point (depending on where the error occurs) and the error.
//
func (bt *BtrfsTool) GetBtrfsFilesystems (filesystem ... string) ([][] string, error) {

   // We're just calling `btrfs filesystem show` with all parameters we got, more than one will
   // cause `btrfs` to exit with an error, which will cause this method to return an error to the
   // caller. So there's no need to do an extra check for more than one parameter. Additionally, if
   // a parameter is specified and no match can be found, `btrfs` will also exit with an error.

   btrfsOptions := [] string { "filesystem", "show" }

   if len (filesystem) > 0 {
      btrfsOptions = append (btrfsOptions, "--"          )
      btrfsOptions = append (btrfsOptions, filesystem ...)
   }

   command     := bt.cmd.Cmd (btrfsOptions ...)
   stdout, err := command.StdoutPipe ()

   if                           err != nil { return nil, err }
   if err := command.Start () ; err != nil { return nil, err }

   scanner     := bufio.NewScanner (stdout)
   filesystems := make ([][] string, 0)
   fs          := make (  [] string, 0)

   // Example output for a filesystem. Tabs at the beginning of the second and third line. There
   // will be one device line per device of the filesystem. If no label is set the output will be
   // none (unquoted, if a label is set it will be enclosed in single quotes, the example also
   // shows that single quotes in the label will not be escaped).
   //
   //    Label: 'test'1'  uuid: b7d9e6a4-8c57-4ba4-85f5-4e2c5474a938
   //      Total devices 1 FS bytes used 112.00KiB
   //      devid    1 size 128.00MiB used 88.00MiB path /dev/loop0

   for scanner.Scan () {

      line := scanner.Text ()

      if strings.TrimSpace (line) == "" {
         continue
      }

      if ! startsWithSpace.MatchString (line) {

         // Assuming every line that doesn't start with a space starts a new filesystem entry. If
         // it doesn't match the "Label: …" format we return an error. This means any malformed
         // UUIDs (can this happen? probably not…) will be filtered out at this point, too.

         // The current fs slice must either be empty (on first iteration) or contain the [uuid,
         // label, device ...] elements. If its length is 2 it only contains the label and UUID,
         // and we return a NoDevicesError.

         if len (fs) > 2 {
            filesystems = append (filesystems, fs)
            fs          = make ([] string, 0)
         } else if len (fs) != 0 {
            return filesystems, fsfinder.NoDevicesError { UUID: fs [0] }
         }

         if btrfsLblRE.MatchString (line) {

            matches := btrfsLblRE.FindStringSubmatch (line)

            fs = append (fs, matches [2])                                  // UUID.

            if matches [1] != "none" {
               fs = append (fs, matches [1] [1:len (matches [1]) - 1])     // Label without quotes.
            } else {
               fs = append (fs, "")                                        // No label.
            }

         } else {

            // If the line does not start with a space and doesn't match "Label: …", we don't know
            // what to do. Return the current filesystem list and an error.

            return filesystems, fsfinder.NoUUIDOrLabelError { Dev: line }

         }

      } else {

         // Lines starting with spaces belong to the currently processed filesystem. If there is
         // none, i.e. if the fs slice does not contain the UUID and label elements, we return an
         // error. Note that these lines are not necessarily lines with device information (see the
         // example above).

         if len (fs) < 2 {
            return filesystems, fsfinder.NoUUIDOrLabelError { Dev: line }
         }

         if btrfsDevRE.MatchString (line) {
            matches := btrfsDevRE.FindStringSubmatch (line)
            fs       = append (fs, matches [1])
         }

      }

   }

   // Once more to add the last filesystem (if any) to the list.
   //
   if len (fs) > 2 {
      filesystems = append (filesystems, fs)
   } else if len (fs) != 0 {
      return filesystems, fsfinder.NoDevicesError { UUID: fs [0] }
   }

   if err := scanner.Err  () ; err != nil { return filesystems, err }
   if err := command.Wait () ; err != nil { return filesystems, err }

   if len (filesystem) > 0 && len (filesystems) > 1 {
      return filesystems, fsfinder.MultipleMatchesError { Filesystem: filesystem [0] }
   }

   return filesystems, nil

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The GetBtrfsSubvolumes(…) method returns a list of subvolumes located in the specified path.
//
// See the BtrfsBackend.GetBtrfsSubvolumes(…) interface documentation for details.
//
// Note that the path will be used as specified as a parameter for the `btrfs subvolume list`
// command (separated by preceding options by a "--"). If validation of the path is required, it
// has to be done by the caller.
//
// Returns a list of subvolume paths, all relative to the filesystem root, and nil for the error on
// success. In case of an error, it will return either nil or a (possibly incomplete) subvolume
// list, depending on where the error occurs, and the error.
//
func (bt *BtrfsTool) GetBtrfsSubvolumes (path string, snapshotsOnly bool) ([] string, error) {

   var subvolRE      *regexp.Regexp
   var btrfsOptions  [] string

   if snapshotsOnly {
      btrfsOptions = [] string { "subvolume", "list", "-o", "-s", "--", path }
      subvolRE     = btrfsSnapshotRE
   } else {
      btrfsOptions = [] string { "subvolume", "list", "-o",       "--", path }
      subvolRE     = btrfsSubvolumeRE
   }

   command     := bt.cmd.Cmd (btrfsOptions ...)
   stdout, err := command.StdoutPipe ()

   if                           err != nil { return nil, err }
   if err := command.Start () ; err != nil { return nil, err }

   scanner    := bufio.NewScanner (stdout)
   subvolumes := make ([] string, 0)

   for scanner.Scan () {

      line := scanner.Text ()

      // Empty lines and every line not matching the regular expression will be ignored, and not
      // cause an error.

      if strings.TrimSpace (line) == "" {
         continue
      }

      if subvolRE.MatchString (line) {
         matches := subvolRE.FindStringSubmatch (line)
         subvolumes = append (subvolumes, matches [1])
      }

   }

   if err := scanner.Err  () ; err != nil { return subvolumes, err }
   if err := command.Wait () ; err != nil { return subvolumes, err }

   return subvolumes, nil

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The DeleteBtrfsSubvolume(…) deletes the subvolume specified by its full path.
//
// The specified path will not be validated or modified in any way.
//
// On successful deletion this method returns nil, otherwise the error.
//
func (bt *BtrfsTool) DeleteBtrfsSubvolume (path string) error {

   options := [] string { "subvolume", "delete", "-C", "--", path }
   command := bt.cmd.Cmd (options ...)

   if err := command.Start () ; err != nil { return err }
   if err := command.Wait  () ; err != nil { return err }

   return  nil

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————