// The command package contains the BtrfsCommand interface.
//
package command

//—————————————————————————————————————————————————————————————————————————————————————————————————

import "os/exec"

//—————————————————————————————————————————————————————————————————————————————————————————————————

// A BtrfsCommand implementation is used by the BtrfsTool backend to get the actual command to run.
//
// Note that a single BtrfsCommand may be used by multiple BtrfsTool instances!
//
type BtrfsCommand interface {

   // The Cmd(…) method must return the exec.Cmd instance for the command to run.
   //
   // It is recommended that implementations set the LC_ALL environment variable to "C".
   //
   Cmd (args ... string) *exec.Cmd

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————