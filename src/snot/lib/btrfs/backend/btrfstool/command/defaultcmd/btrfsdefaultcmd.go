// The defaultcmd package contains the BtrfsDefaultCommand type implementing the BtrfsCommand
// interface.
//
package defaultcmd

//—————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "os"
   "os/exec"

   "snot/lib/btrfs/backend/btrfstool/command"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The BtrfsDefaultCommand implements the BtrfsCommand interface.
//
// The command to run may be set with the constructor (see NewBtrfsDefaultCommand(…)), it defaults
// to `btrfs` (no full path, it will be searched in the system's $PATH).
//
type BtrfsDefaultCommand struct {
   path string
}

// The BtrfsDefaultCommandOption type defines the signature for the constructor's option functions.
//
type BtrfsDefaultCommandOption func (*BtrfsDefaultCommand) error

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Make sure we implement the BtrfsCommand interface (compile time check).
//
var _ command.BtrfsCommand = (*BtrfsDefaultCommand) (nil)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The NewBtrfsDefaultCommand(…) function creates a new BtrfsDefaultCommand instance.
//
// Available options are:
//
//    * WithBtrfsPath (…) - Set the path of the `btrfs` command.
//
// See the individual option functions for more details. The default path will be "btrfs" (i.e. no
// full path, the command must be somewhere in the $PATH).
//
// Returns the new BtrfsDefaultCommand instance and nil for the error on success, or nil and the
// error in case of an error.
//
func NewBtrfsDefaultCommand (options ... BtrfsDefaultCommandOption) (*BtrfsDefaultCommand, error) {

   bc := &BtrfsDefaultCommand {}

   for _, option := range options {
      if option == nil {
         continue
      }
      if err := option (bc) ; err != nil {
         return nil, err
      }
   }

   if bc.path == "" {
      bc.path = "btrfs"
   }

   return bc, nil

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithBtrfsPath(…) as parameter to NewBtrfsDefaultCommand(…) to set the executable path.
//
// If no path is set the constructor will set it to "btrfs" which will be looked up in the $PATH.
//
// Note: No attempt will be made to verify the path (existence, access rights etc.), if the
// specified executable can't be executed, calling Run(…) (or Start(…) etc.) for the os/exec.Cmd
// returned by the Cmd(…) method will fail with an error.
//
func WithBtrfsPath (path string) BtrfsDefaultCommandOption {

   return func (bc *BtrfsDefaultCommand) error {
      bc.path = path
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Cmd(…) method returns an os.exec.Cmd instance to run the `btrfs` command with the specified
// command line parameters.
//
// Note: LC_ALL=C will be added to the command's environment. The command name and/or full path
// must be set during instantiation, see NewBtrfsDefaultCommand(…).
//
func (bc *BtrfsDefaultCommand) Cmd (args ... string) *exec.Cmd {

   cmd    := exec.Command (bc.path, args ...)
   cmd.Env = append (os.Environ (), "LC_ALL=C")

   return cmd

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————