// The factory package contains the BtrfsToolFactory type implementing the BtrfsBackendFactory
// interface.
//
package factory

//—————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "snot/lib/btrfs/backend"
   "snot/lib/btrfs/backend/btrfstool"
   "snot/lib/btrfs/backend/btrfstool/command"
   "snot/lib/btrfs/backend/btrfstool/command/defaultcmd"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// A BtrfsToolFactory instance creates new BtrfsTool instances.
//
type BtrfsToolFactory struct {
   cmd command.BtrfsCommand
}

// The BtrfsToolOption type defines the signature for the constructor's option functions.
//
type BtrfsToolFactoryOption func (*BtrfsToolFactory) error

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Make sure we implement the BtrfsBackendFactory interface (compile time check).
//
var _ backend.BtrfsBackendFactory = (*BtrfsToolFactory) (nil)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The NewBtrfsToolFactory(…) function creates a new BtrfsToolFactory instance.
//
// Available options are:
//
//    * WithBtrfsCommand (…) - Set the BtrfsCommand for the factory.
//
// See the individual option functions for more details.
//
// If no BtrfsCommand is set the constructor will set it to a new BtrfsDefaultCommand instance
// (with its default options). The BtrfsCommand set will be used for all BtrfsTool instances
// created by the factory.
//
// Returns the new BtrfsToolFactory instance and nil for the error on success, or nil and the
// error in case of an error.
//
func NewBtrfsToolFactory (options ... BtrfsToolFactoryOption) (*BtrfsToolFactory, error) {

   btf := &BtrfsToolFactory {}

   for _, option := range options {
      if option == nil {
         continue
      }
      if err := option (btf) ; err != nil {
         return nil, err
      }
   }

   if btf.cmd == nil {
      if cmd, err := defaultcmd.NewBtrfsDefaultCommand () ; err != nil {
         return nil, err
      } else {
         btf.cmd = cmd
      }
   }

   return btf, nil

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithBtrfsCommand(…) as parameter to NewBtrfsToolFactory(…) to set the BtrfsCommand for the
// instance.
//
// The BtrfsCommand set will be used for all BtrfsTool instances created by this factory. If no
// command is set, the constructor will set it to a new BtrfsDefaultCommand instance (with its
// default options).
//
func WithBtrfsCommand (cmd command.BtrfsCommand) BtrfsToolFactoryOption {

   return func (btf *BtrfsToolFactory) error {
      btf.cmd = cmd
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The NewBtrfsBackend() method returns a new BtrfsTool instance.
//
// The BtrfsTool will use the factory's BtrfsCommand. If an error occurs, it will be returned and
// the BtrfsBackend will be nil, otherwise the BtrfsBackend will be returned and the error will be
// nil.
//
func (btf *BtrfsToolFactory) NewBtrfsBackend () (backend.BtrfsBackend, error) {

   return btrfstool.NewBtrfsTool (btrfstool.WithBtrfsCommand (btf.cmd))

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————