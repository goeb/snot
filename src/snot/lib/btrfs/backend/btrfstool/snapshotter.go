// The BtrfsToolSnapshotter type, implementing the BtrfsSnapshotter interface.

package btrfstool

//—————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "os/exec"

   "snot/lib/btrfs/backend"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The BtrfsToolSnapshotter implements the BtrfsSnapshotter interface.
//
type BtrfsToolSnapshotter struct {

   tool     *BtrfsTool              // The "parent" BtrfsTool.
   cmd      *exec.Cmd               // The exec.Cmd instance to use.
   source   string                  // Source subvolume, full path.
   target   string                  // Snapshot to create, full path.
   rw       bool                    // Create snapshot rw (true) or ro (false).

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Check if BtrfsToolSnapshotter implements the BtrfsSnapshotter interface (compile-time check).
//
var _ backend.BtrfsSnapshotter = (*BtrfsToolSnapshotter) (nil)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The GetSnapshotter(…) method returns a new BtrfsToolSnapshotter instance.
//
// The first parameter must be the source subvolume (full path). The second parameter the target
// path (the full path to the snapshot). The third parameter must be true to create a writable
// snapshot, false to create a read-only snapshot.
//
// No parameter will be verified/validated/modified in any way. This method will return the new
// BtrfsToolSnapshotter, the error will always be nil.
//
func (bt *BtrfsTool) GetSnapshotter (

   source   string,
   target   string,
   rw       bool,

) (backend.BtrfsSnapshotter, error) {

   return &BtrfsToolSnapshotter {
      tool   : bt,
      source : source,
      target : target,
      rw     : rw,
   }, nil

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Prepare() method will prepare the command to create the snapshot.
//
// This method will always return nil.
//
func (bs *BtrfsToolSnapshotter) Prepare () error {

   var opts [] string

   if bs.rw {
      opts = [] string { "subvolume", "snapshot",       "--", bs.source, bs.target }
   } else {
      opts = [] string { "subvolume", "snapshot", "-r", "--", bs.source, bs.target }
   }

   bs.cmd = bs.tool.cmd.Cmd (opts ...)

   return nil

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Snapshot() method will run the prepared command (see Prepare()).
//
// Prepare() has to be called before Snapshot()! In case of an error it will be returned, otherwise
// the return value will be nil.
//
func (bs *BtrfsToolSnapshotter) Snapshot () error {

   if err := bs.cmd.Start () ; err != nil { return err }
   if err := bs.cmd.Wait  () ; err != nil { return err }

   return nil

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————