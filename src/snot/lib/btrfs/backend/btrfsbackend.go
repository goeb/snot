// The BtrfsBackend, BtrfsBackendFactory and BtrfsSnapshotter interfaces.
//
package backend

//—————————————————————————————————————————————————————————————————————————————————————————————————

// A BtrfsBackend is used to interact with and/or get information about Btrfs filesystems.
//
// This layer may be used to switch between different sources for the same information, for example
// one implementation use the `btrfs` command line tool, another one may use the btrfs libraries.
//
type BtrfsBackend interface {

   // The GetBtrfsFilesystems(…) method must return information about Btrfs filesystems found on
   // the system.
   //
   // Without any parameter, information about all Btrfs filesystems found on the system must be
   // returned. If none are found, the list must be empty, and the error must be nil. If one
   // parameter is specified, it must be used to search for a specific filesystem, which has a UUID
   // or label or is located on the device specified by the parameter (this is consistent with the
   // `btrfs filesystem show <…>` command). Note that multiple filesystems may match the parameter,
   // the returned list may contain more than one element. In this case, the list must be returned
   // and the error must be set to a MultipleMatchesError (package snapshots/fsfinder). If a
   // parameter is specified and there is no matching filesystem, an error and an empty list must
   // be returned. If more than one parameter is specified the method must return an error.
   //
   // The return value must be a slice of string slices. Each sub-slice must contain the UUID and
   // label of the filesystem as its first two elements (in that order), followed by any devices
   // the filesystem is located on. In case of an error, the list of filesystems found before the
   // error (if any) and the error has to be returned. If the error occurs before any filesystems
   // can be found the first return value should be nil.
   //
   GetBtrfsFilesystems (filesystem ... string) ([][] string, error)

   // The GetBtrfsSubvolumes(…) method must return a list of subvolumes below the specified path.
   //
   // Basically, this must return the last column of the `btrfs subvolume list -o <path>` output
   // (with an additional `-s` parameter if snapshotsOnly is true), i.e. the paths of subvolumes
   // located below the subvolume specified by the path, relative to the filesystem's root, nested
   // subvolumes have to be ommitted.
   //
   // Note that, to conform to the `btrfs` output, it is not required that the returned subvolumes
   // are located beneath the specified path, they only have to reside on the parent subvolume this
   // path belongs to. Filtering out unwanted subvolumes will be done by the caller.
   //
   // On success the method must return the list of subvolumes (may be empty) and nil. On error it
   // must return either nil or, if some subvolumes have been found, the (possibly incomplete)
   // subvolume list and the error.
   //
   GetBtrfsSubvolumes (path string, snapshotsOnly bool) ([] string, error)

   // The DeleteBtrfsSubvolume(…) method must delete the subvolume specified by its full path.
   //
   // On success, this method must return nil, otherwise the error.
   //
   DeleteBtrfsSubvolume (path string) error

   // The GetSnapshotter(…) method must return a new BtrfsSnapshotter instance that will be used to
   // take the snapshot from the source subvolume (first parameter) and store it at the target
   // (second parameter, this may be a target directory or the full path of the target subvolume),
   // either as a readonly snapshot if the third parameter is false, writable otherwise.
   //
   // On success the returned error must be nil, otherwise the BtrfsSnapshotter must be nil and the
   // error set accordingly.
   //
   GetSnapshotter (source string, target string, rw bool) (BtrfsSnapshotter, error)

}

// A BtrfsBackendFactory instance creates new BtrfsBackend instances.
//
// Note that a single factory instance may be shared by multiple Btrfs etc. instances, depending on
// how these are created.
//
type BtrfsBackendFactory interface {

   // The NewBtrfsBackend() method must return a new BtrfsBackend instance every time it is called,
   // or an error if something went wrong.
   //
   NewBtrfsBackend () (BtrfsBackend, error)

}

// A BtrfsSnapshotter will create a snapshot of a subvolume, splitting the operation into a
// preparation phase and the actual snapshot creation.
//
type BtrfsSnapshotter interface {

   // The Prepare() method must prepare the snapshot as far as possible without actually creating
   // it. Note that this does not include creation of any directories, it just means to prepare the
   // command (or whatever) to take the snapshot.
   //
   // It must return nil on success, otherwise the error.
   //
   Prepare () error

   // The Snapshot() method must create the (previously prepared) snapshot.
   //
   // An implementation may assume that this method will only be called after Prepare() has been
   // called.
   //
   // It must return nil on success, otherwise the error.
   //
   Snapshot () error

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————