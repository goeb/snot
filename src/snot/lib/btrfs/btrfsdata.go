// The BtrfsData and btrfs.SnapshotData types.

package btrfs

//—————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "path/filepath"

   "snot/lib/snapshots"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The BtrfsData type contains data about the filesystem and configured paths that does not depend
// on the snapshot and subvolume information.
//
type BtrfsData struct {

   UUID        string
   Label       string
   Type        string

   MountPoint  string      // /mnt/point
   MP          string

   LiveBase    string      // volumes/@live
   LB          string

   SnapBase    string      // volumes/@snap
   SB          string

   LiveRoot    string      // /mnt/point/volumes/@live
   LR          string

   SnapRoot    string      // /mnt/point/volumes/@snap
   SR          string

}

// The btrfs.SnapshotData type contains data about the filesystem, including both the BtrfsData and
// the snapshots.SnapshotData (both embedded). Other than the BtrfsData type, this one is obviously
// snapshot-specific.
//
type SnapshotData struct {

   BtrfsData
   snapshots.SnapshotData

   LiveDir     string      // /mnt/point/volumes/@live
   LD          string

   SnapDir     string      // /mnt/point/volumes/@snap/1234567890
   SD          string

   LiveRel     string      // volumes/@live
   LL          string

   SnapRel     string      // volumes/@snap/1234567890
   SL          string

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Data() method returns the BtrfsData for the Btrfs instance.
//
func (btrfs *Btrfs) Data () BtrfsData {

   mp := btrfs.MountPoint ()
   lb := btrfs.liveDir
   sb := btrfs.snapDir
   lr := filepath.Join (mp, btrfs.liveDir)
   sr := filepath.Join (mp, btrfs.snapDir)

   return BtrfsData {

      UUID           : btrfs.UUID   (),
      Label          : btrfs.Label  (),
      Type           : btrfs.FSType (),

      MountPoint     : mp,
      MP             : mp,

      LiveBase       : lb,
      LB             : lb,

      SnapBase       : sb,
      SB             : sb,

      LiveRoot       : lr,
      LR             : lr,

      SnapRoot       : sr,
      SR             : sr,

   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The SnapshotData(…) method returns the btrfs.SnapshotData for the instance and the specified
// SnapshotData.
//
func (btrfs *Btrfs) SnapshotData (data snapshots.SnapshotData) SnapshotData {

   bd := btrfs.Data ()
   mp := btrfs.MountPoint ()
   ld := filepath.Join (mp, btrfs.liveDir)
   sd := filepath.Join (mp, btrfs.snapDir, data.TS)
   ll := bd.LiveBase
   sl := filepath.Join (bd.SnapBase, data.TS)

   return SnapshotData {

      BtrfsData      : bd,
      SnapshotData   : data,

      LiveDir        : ld,
      LD             : ld,

      SnapDir        : sd,
      SD             : sd,

      LiveRel        : ll,
      LL             : ll,

      SnapRel        : sl,
      SL             : sl,

   }

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————