// The (private) subvolume type and associated methods.

package btrfs

//—————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "os"
   "path/filepath"
   "sync"

   "snot/lib/btrfs/backend"
   "snot/lib/multierror"
   "snot/lib/snapshots"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The subvolume type represents a Btrfs subvolume.
//
// Private type, no constructor.
//
type subvolume struct {

   backend  backend.BtrfsBackend          // BtrfsBackend will be created by the Btrfs' factory.
   btrfs    *Btrfs                        // The parent filesystem.

   // The following fields specify the subvolume path relative to its filesystem's root directory.
   //
   // full = <root>/<path>/<name>
   // svol =        <path>/<name>
   //
   // Some of these may be empty, e.g. path if the subvolume is located right beneath the root. The
   // root will be set to the value set when the subvolumes() method is called, and may contain the
   // snapshot subdirectory.

   full     string                        // Full subvolume path relative to filesystem root.
   name     string                        // Subvolume name.
   path     string                        // Subvolume path, relative to root, without the name.
   root     string                        // Subvolume root, relative to filesystem root.
   svol     string                        // Subvolume path relative to subvolume root.

}

// A SubvolumeFilter function is used to filter (exclude) subvolumes.
//
// The function will receive the SubvolumeData as parameter.
//
// It must return a false value if the subvolume should be excluded from the snapshot operation
// (create, restore…) as the first return value, or true if it should be included. If multiple
// filters are set for the filesystem, the first false return value is final and the subvolume will
// be excluded. The second return value must be nil if no error occurred, otherwise the error (an
// error will cause exclusion of the subvolume).
//
type SubvolumeFilter func (SubvolumeData) (bool, error)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The subvolumes(…) method returns a list of subvolumes located in the specified root path.
//
// The root path has to be specified relative to the filesystem root, i.e. relative to the mount
// point of the filesystem (and it will be interpreted as a relative path even if it starts with a
// slash). The resulting list will contain only subvolumes located beneath the specified root path.
// The path will be resolved, and must itself be located beneath the mount point if it exists. If
// it does not exist, an empty subvolume list will be returned (i.e. a non-existing root path is
// not considered an error). Note that all path-related properties of the subvolume instances are
// set based on the root path specified here!
//
// If snapshotsOnly is true, only snapshot subvolumes will be returned.
//
// The *SnapshotData, if specified, will be used to filter the list of subvolumes based on the
// SubvolumeFilters set for the Btrfs instance. This parameter may be nil to just include all
// subvolumes found. If no filter functions are set all subvolumes will be included, too.
//
// Note: By default, subvolumes will be included in the results. Filter functions will be run
// until one returns false, which will skip the remaining filters and cause the subvolume to not be
// included. All filters must return true for a subvolume to be included.
//
// On success, the method will return the subvolume list (which may be empty) and the error will be
// nil, otherwise the list (which may be incomplete or nil, depending on where the error occurs)
// and the error.
//
// Note that the filesystem has to be mounted before calling this method!
//
func (btrfs *Btrfs) subvolumes (

   root           string,
   snapshotsOnly  bool,
   data           *snapshots.SnapshotData,

) ([] *subvolume, error) {

   var err error
   var mnt string       // Mount point, absolute, resolved.
   var svp string       // Full path to the subvolume root, absolute, resolved.
   var rel string       // Resolved subvolume root, relative to filesystem root.

   if btrfs.MountPoint () == "" {
      return nil, NotMountedError { btrfs.UUID () }
   }

   // The backend will return real paths, we need to resolve all symbolic links in the specified
   // path, as well as in the mount point (just to be safe). If the root path doesn't exist return
   // an empty list, but no error.

   if mnt, err = btrfs.PathUtils ().ResolvePath (btrfs.MountPoint ()) ; err != nil {
      return nil, err
   }

   if svp, err = btrfs.PathUtils ().ResolvePath (filepath.Join (mnt, root)) ; err != nil {
      if os.IsNotExist (err) {
         return make ([] *subvolume, 0), nil
      }
      return nil, err
   }

   // The subvolume root directory, resolved, relative to the filesystem root. If this is not
   // located beneath the mount point we return an error (this should filter out symbolic links
   // crossing filesystem boundaries). If the subvolume root is the same as the filesystem root,
   // rel will be set to an empty string.

   if rel, err = filepath.Rel (mnt, svp) ; err != nil {
      return nil, err
   } else if btrfs.PathUtils ().IsOutside (rel) {
      return nil, InvalidPathError { root }
   } else if rel == "." {
      rel = ""
   }

   // Get all subvolumes, but keep only those that are located in the subvolume root directory, and
   // are not excluded by a subvolume filter.
   //
   // GetBtrfsSubvolumes(…) may return an incomplete list on error. We do the same here.

   mErr       := multierror.New ()
   svols, err := btrfs.backend.GetBtrfsSubvolumes (svp, snapshotsOnly)

   if err != nil {
      mErr.Append (err)
   }

   subvolumes := make ([] *subvolume, 0)

   for _, svol := range svols {

      vol, err := filepath.Rel (
         btrfs.PathUtils ().Slashed (rel),
         btrfs.PathUtils ().Slashed (svol),
      )

      if err != nil {
         mErr.Append (err)
         continue
      }

      if btrfs.PathUtils ().IsOutside (vol) {
         continue
      }

      backend, err := btrfs.factory.NewBtrfsBackend ()

      if err != nil {
         mErr.Append (err)
         continue
      }

      dir := filepath.Dir (vol)
      if dir == "." || dir == string (os.PathSeparator) {
         dir = ""
      }

      sv := &subvolume {
         backend : backend,
         btrfs   : btrfs,
         full    : filepath.Join (rel, vol),
         name    : filepath.Base (vol),
         path    : dir,
         root    : rel,
         svol    : vol,
      }

      if data == nil || len (btrfs.svFilters) == 0 {
         subvolumes = append (subvolumes, sv)
         continue
      }

      include := true
      for _, filter := range btrfs.svFilters {
         if inc, err := filter (sv.Data (*data)) ; err == nil && ! inc {
            include = false
            break
         } else if err != nil {
            mErr.Append (err)
            include = false
            break
         }
      }

      if include {
         subvolumes = append (subvolumes, sv)
      }

   }

   return subvolumes, mErr.Get ()

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Delete() method will delete the subvolume.
//
// The filesystem has to be mounted. In case of an error it will be returned, otherwise the return
// value will be nil.
//
func (sv *subvolume) Delete () error {

   if sv.btrfs.MountPoint () == "" {
      return NotMountedError { sv.btrfs.UUID () }
   }

   return sv.backend.DeleteBtrfsSubvolume (filepath.Join (sv.btrfs.MountPoint (), sv.full))

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The CreateSnapshot(…) method returns a snapshots.CreateSnapshot function that takes a snapshot
// of the subvolume.
//
// The CreateSnapshot function will wait until a value is available on the prepared channel. If it
// is true, it will start the actual CreateSnapshot logic, otherwise nothing will be done. In case
// of a rollback the function will send a value back on the prepared channel once it's done.
//
// See the Creators(…) and PrepareSnapshot(…) methods for more details.
//
func (sv *subvolume) CreateSnapshot (prepared chan bool) snapshots.CreateSnapshot {

   return func (

      data           snapshots.SnapshotData,

      result         chan bool,                 // Whether everything went well.
      rollback       chan bool,                 // Whether to undo everything.
      ignoreErrors   bool,
      errCh          chan error,                // Error channel.

      ready          *sync.WaitGroup,           // Call Done() and Wait() when ready to go.
      done           *sync.WaitGroup,           // Call Done() on exit.

   ) {

      defer done.Done ()

      var snap    backend.BtrfsSnapshotter
      var err     error
      var dirs    [] string
      var sdata   SubvolumeData

      // Wait for the preparation of the filesystem to be finished. If these preparations fail,
      // we assume that we cannot continue and skip the snapshotting.

      ok := <- prepared

      if ok {
         sdata = sv.Data (data)
         if dirs, err = sv.btrfs.PathUtils ().MkdirAll (sdata.SnapPath, 0700) ; err != nil {
            errCh <- err
            ok = false
         }
      }

      if ok {
         if snap, err = sv.backend.GetSnapshotter (sdata.LF, sdata.SP, sv.btrfs.rw) ; err != nil {
            errCh <- err
            ok = false
         }
      }

      if ok {
         if err = snap.Prepare () ; err != nil {
            errCh <- err
            ok = false
         }
      }

      // Everything's prepared now. Wait for others to be ready.

      if ready != nil {
         ready.Done ()
         ready.Wait ()
      }

      // Create the snapshot.

      del := false
      if ok {
         if err = snap.Snapshot () ; err != nil {
            errCh <- err
            ok = false
         } else {
            del = true
         }
      }

      // Send our result back to the caller and wait for the result of the others.

      result <- ok

      rb := <- rollback

      if rb {

         if del {
            if err = sv.backend.DeleteBtrfsSubvolume (sdata.SnapFull) ; err != nil {
               errCh <- err
               dirs = nil
            }
         }

         for _, dir := range dirs {
            if err := sv.btrfs.PathUtils ().Remove (dir) ; err != nil {
               errCh <- err
               break
            }
         }

         prepared <- true

      }

   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The RestoreSnapshot(…) method returns a snapshots.RestoreSnapshot function that restores a
// snapshot of the subvolume.
//
func (sv *subvolume) RestoreSnapshot () snapshots.RestoreSnapshot {

   return func (

      data           snapshots.SnapshotData,
      result         chan bool,
      rollback       chan bool,
      ignoreErrors   bool,
      errChan        chan error,
      done           *sync.WaitGroup,

   ) {

      defer done.Done ()

      sData    := sv.Data (data)
      pUtils   := sv.btrfs.PathUtils ()
      ok       := true
      tempDir  := ""

      // Create the target (live) directory tree, in case it doesn't exist.

      dirs, err := pUtils.MkdirAll (sData.LivePath, 0700)

      if err != nil {
         errChan <- err
         ok = false
      }

      // Create a temporary directory to move the current subvolume into. It will be located at the
      // same location as the subvolume, its name will be random, starting with a dot.

      if ok {
         tempDir, err = pUtils.TempDir (sData.LivePath, ".")
         if err != nil {
            errChan <- err
            ok = false
         }
      }

      // Move the current subvolume into the temporary directory. It is not an error if it doesn't
      // exist.

      origSV := filepath.Join (tempDir, sData.Name)

      if ok {
         err = pUtils.Rename (sData.LiveFull, origSV)
         if err != nil && ! os.IsNotExist (err) {
            errChan <- err
            ok = false
         }
      }

      // Create a writable snapshot of the snapshot at the target location.

      var ss backend.BtrfsSnapshotter

      if ok {
         ss, err = sv.backend.GetSnapshotter (sData.SnapFull, sData.LiveFull, true)
         if err != nil {
            errChan <- err
            ok = false
         }
      }

      if ok {
         if err = ss.Prepare () ; err != nil {
            errChan <- err
            ok = false
         }
      }

      if ok {
         if err = ss.Snapshot () ; err != nil {
            errChan <- err
            ok = false
         }
      }

      // Done. Send the result to the caller and wait for the other goroutines to decide whether to
      // rollback or not. Note that if an error occurred in this goroutine we will always undo all
      // changes since this subvolume could not be restored.

      result <- ok

      rb := <- rollback

      if rb || ! ok {

         // If ok is false, the restored snapshot hasn't been created. Move the backup back to its
         // original folder, remove the temporary directory and any other directories we may have
         // created. Note that there may have been an error before the original subvolume could be
         // moved etc., se we ignore any not exist errors here.

         err = pUtils.Rename (origSV, sData.LiveFull)

         if err != nil && ! os.IsNotExist (err) {
            errChan <- err
         }

         err = pUtils.Remove (tempDir)

         if err != nil && ! os.IsNotExist (err) {
            errChan <- err
         }

         err = pUtils.RemoveList (dirs ...)

         if err != nil {
            errChan <- err
         }

      } else {

         // No rollback and everything went well. Remove the old volume and temporary directories
         // unless disabled.

         if ! sv.btrfs.keepLive {
            if err = sv.backend.DeleteBtrfsSubvolume (origSV) ; err != nil {
               errChan <- err
            } else {
               if err = pUtils.Remove (tempDir) ; err != nil {
                  errChan <- err
               }
            }
         }

      }

   }

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————