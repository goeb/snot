// Predefined snapshot filters.

package btrfs

//—————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "strings"

   "snot/lib/snapshots"
   "snot/lib/time/interval"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The IncludePrefixFilter(…) function returns a SnapshotFilter that only matches snapshots with an
// ID starting with (or matching) one of the parameters. This function will never return an error.
//
// Matching is case-sensitive! Empty prefixes will be skipped.
//
func IncludePrefixFilter (prefixes ... string) (SnapshotFilter, error) {

   return func (data SnapshotData, meta snapshots.MetaData) (bool, error) {

      inc := false

      for _, prefix := range prefixes {
         if prefix == "" {
            continue
         }
         if strings.HasPrefix (data.ID, prefix) {
            inc = true
            break
         }
      }

      return inc, nil

   }, nil

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// TimeIntervalFilter(…) returns a SnapshotFilter that matches snapshots based on the specified
// interval and the snapshot creation time. See snot/lib/time/interval for more details, the
// Interval will be created from the specified strings (may be empty to not set an upper or lower
// limit), and the actual SnapshotFilter function will call Match(…). If the snapshot creation time
// matches the interval the snapshot will be included.
//
// This function will return an error (second return value) if no interval can be created from the
// specified strings.
//
func TimeIntervalFilter (from string, until string) (SnapshotFilter, error) {

   interval, err := interval.New (from, until)

   if err != nil {
      return nil, err
   }

   return func (data SnapshotData, meta snapshots.MetaData) (bool, error) {

      return interval.Match (data.Created), nil

   }, nil

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————