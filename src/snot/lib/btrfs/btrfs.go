// The btrfs package contains the Snapshottable Btrfs type.
//
package btrfs

//—————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "path/filepath"
   "strings"

   "snot/lib/btrfs/backend"
   "snot/lib/btrfs/backend/btrfstool/factory"
   "snot/lib/fs"
   "snot/lib/fs/mounter"
   "snot/lib/pathutils"
   "snot/lib/snapshots"
   "snot/lib/snapshots/metadata"
   "snot/lib/snapshots/metadata/filestorage"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Btrfs type represents a single Btrfs filesystem.
//
type Btrfs struct {

               *  fs.Filesystem                 // Embedded Filesystem type (yes, it's a pointer).
   fsOptions   [] fs.FilesystemOption           // NewFilesystem(…) options.

   backend        backend.BtrfsBackend          // The BtrfsBackend used by this Btrfs instance.
   factory        backend.BtrfsBackendFactory   // The factory to create backends for the svs.

   liveDir        string                        // Source subvolume directory.
   snapDir        string                        // Target snapshot base directory.
   rw             bool                          // Whether to create writable snapshots.
   keepLive       bool                          // Whether to keep live subvolumes on restore.

   ssFilters   [] SnapshotFilter                // Filters to decide which snapshot to include.
   svFilters   [] SubvolumeFilter               // Filters to decide which subvolume to include.

   metaStorage    metadata.MetaStorage          // Snapshot meta data storage implementation.
   metaConfig     MetaStorageConfig             // Which function to use for meta storage config.

}

// The BtrfsOption type defines the signature for the constructor's option functions.
//
type BtrfsOption func (*Btrfs) error

// The MetaStorageConfig type defines the signature of a MetaStorage configuration function.
//
type MetaStorageConfig func (SnapshotData) interface {}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Check if Btrfs implements the MountableFS and Snapshottable interfaces (compile-time check).
//
var _ fs.MountableFS          = (*Btrfs) (nil)
var _ snapshots.Snapshottable = (*Btrfs) (nil)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The NewBtrfs(…) function creates a new Btrfs instance.
//
// The following options are available to set properties of the Btrfs instance:
//
//    * WithBtrfsBackend         (…) - Set the BtrfsBackend.
//    * WithBtrfsBackendFactory  (…) - Set the BtrfsBackendFactory.
//    * WithKeepLiveSubvolumes   (…) - Whether to not delete live subvolumes on restore.
//    * WithLiveDir              (…) - Set the live subvolume directory.
//    * WithMetaStorage          (…) - Set the MetaStorage implementation.
//    * WithMetaStorageConfig    (…) - Set the MetaStorage configuration function.
//    * WithSnapshotDir          (…) - Set the snapshot directory.
//    * WithSnapshotFilters      (…) - Set the snapshot filters.
//    * WithSubvolumeFilters     (…) - Set the subvolume filters.
//    * WithWritable             (…) - Whether to create writable snapshots.
//
// The following options are used to initialize the embedded fs.Filesystem instance:
//
//    * WithDevices              (…) - Set the device(s) of the filesystem.
//    * WithLabel                (…) - Set the filesystem label.
//    * WithMounter              (…) - Set the Mounter instance.
//    * WithPathUtils            (…) - Set the PathUtils instance.
//    * WithUUID                 (…) - Set the filesystem UUID.
//
// See the individual option functions in the fs package as well as fs.NewFilesystem for more
// details.
//
// Unless stated otherwise, when using the same option multiple times the last one will override
// preceding options.
//
// If no BtrfsBackendFactory is set, it will be set to a new BtrfsToolFactory (with no options,
// i.e. using the BtrfsToolFactory defaults). If no BtrfsBackend is set, the factory will be used
// to create one. The live and snapshot directories will be set to "@live" and "@snap" if they're
// left empty. If no MetaStorage is set, it will be set to a new FileStorage, using its default
// options, the MetaStorage configuration function will be set to MetaStorageConfigSnapshotData if
// it's not set.
//
// NewBtrfs(…) returns the instance on success (error will be nil), or nil and the error on
// failure.
//
func NewBtrfs (options ... BtrfsOption) (*Btrfs, error) {

   btrfs := &Btrfs {
      fsOptions : make ([] fs.FilesystemOption, 0),
      ssFilters : make ([] SnapshotFilter,      0),
      svFilters : make ([] SubvolumeFilter,     0),
   }

   for _, option := range options {
      if option == nil {
         continue
      }
      if err := option (btrfs) ; err != nil {
         return nil, err
      }
   }

   btrfs.fsOptions = append (btrfs.fsOptions, fs.WithFSType ("btrfs"))
   btrfs.fsOptions = append (btrfs.fsOptions, fs.WithMountOptionsCallback (mountOptionsCallback))

   var err error

   if btrfs.Filesystem, err = fs.NewFilesystem (btrfs.fsOptions ...) ; err != nil {
      return nil, err
   }

   btrfs.fsOptions = nil

   if btrfs.factory == nil {
      if btrfs.factory, err = factory.NewBtrfsToolFactory () ; err != nil {
         return nil, err
      }
   }

   if btrfs.backend == nil {
      if btrfs.backend, err = btrfs.factory.NewBtrfsBackend () ; err != nil {
         return nil, err
      }
   }

   if btrfs.metaStorage == nil {
      if btrfs.metaStorage, err = filestorage.NewFileStorage () ; err != nil {
         return nil, err
      }
   }

   if btrfs.metaConfig == nil {
      btrfs.metaConfig = MetaStorageConfigSnapshotData
   }

   // If these dirs are empty, set them to the default values:

   if btrfs.liveDir == "" { btrfs.liveDir = "@live" }
   if btrfs.snapDir == "" { btrfs.snapDir = "@snap" }

   // If the dirs are ".", set them to an empty string, otherwise clean the paths and check again.
   // If the cleaned path starts with ".." or "../", or is ".", return an InvalidPathError. After
   // this both paths will be relative paths (or empty).
   //
   // That's probably only gonna work on Unixy filesystems, which is really all we care about.

   if btrfs.liveDir == "." {
      btrfs.liveDir = ""
   } else {
      btrfs.liveDir = filepath.Clean (btrfs.PathUtils ().Unslashed (btrfs.liveDir))
      if btrfs.liveDir == "." || btrfs.PathUtils ().IsOutside (btrfs.liveDir) {
         return nil, InvalidPathError { btrfs.liveDir }
      }
   }

   if btrfs.snapDir == "." {
      btrfs.snapDir = ""
   } else {
      btrfs.snapDir = filepath.Clean (btrfs.PathUtils ().Unslashed (btrfs.snapDir))
      if btrfs.snapDir == "." || btrfs.PathUtils ().IsOutside (btrfs.snapDir) {
         return nil, InvalidPathError { btrfs.snapDir }
      }
   }

   return btrfs, nil

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The mountOptionsCallback(…) function is used by the Filesystem's Mount(…) method to modify the
// specified mount options.
//
// This function for a Btrfs filesystem will add the "subvol=/" option if the original options do
// not contain a subvol=… or subvolid=… option. The original options will not be altered. The error
// returned will always be nil.
//
func mountOptionsCallback (options ... string) ([] string, error) {

   found := false
   opts  := [] string {}

   for _, option := range options {

      if strings.HasPrefix (option, "subvolid=") || strings.HasPrefix (option, "subvol=") {
         found = true
      }

      opts = append (opts, option)

   }

   if (! found) {
      opts = append (opts, "subvol=/")
   }

   return opts, nil

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithBtrfsBackend(…) as parameter to NewBtrfs(…) to set the BtrfsBackend for the filesystem.
//
// If no backend is set, it will be created by the instance's BtrfsFactory in the constructor.
//
func WithBtrfsBackend (backend backend.BtrfsBackend) BtrfsOption {

   return func (btrfs *Btrfs) error {
      btrfs.backend = backend
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithBtrfsBackendFactory(…) as parameter to NewBtrfs(…) to set the BtrfsBackendFactory for
// the filesystem.
//
// If no backend factory is set, it will be set to a new BtrfsToolFactory in the constructor.
//
func WithBtrfsBackendFactory (factory backend.BtrfsBackendFactory) BtrfsOption {

   return func (btrfs *Btrfs) error {
      btrfs.factory = factory
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithMetaStorage(…) as parameter to NewBtrfs(…) to set the MetaStorage for the filesystem.
//
// If no backend is set, the constructor will create a new FileStorage instance.
//
func WithMetaStorage (storage metadata.MetaStorage) BtrfsOption {

   return func (btrfs *Btrfs) error {
      btrfs.metaStorage = storage
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithMetaStorageConfig(…) as parameter to NewBtrfs(…) to set the MetaStorage configuration
// function for the filesystem.
//
// See the type definition for the MetaStorageConfig function's signature. The function will
// receive the btrfs.SnapshotData as parameter, other required values have to be passed in other
// suitable ways. The function must return the configuration data required for the MetaStorage's
// Save(…)/Load(…)/Delete(…) methods.
//
// This package provides one predefined function, MetaStorageConfigSnapshotData(…), which just
// returns the btrfs.SnapshotData struct passed as its parameter.
//
func WithMetaStorageConfig (config MetaStorageConfig) BtrfsOption {

   return func (btrfs *Btrfs) error {
      btrfs.metaConfig = config
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The MetaStorageConfigSnapshotData(…) function may be used as a MetaStorageConfig function.
//
// This function simply returns the SnapshotData it receives as parameter. It may be set as the
// MetaStorage configuration function with the WithMetaStorageConfig(…) constructor parameter.
//
func MetaStorageConfigSnapshotData (data SnapshotData) interface {} {

   return data

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithKeepLiveSubvolumes(…) as parameter to NewBtrfs(…) to set whether the old live subvolumes
// will be deleted on restore or not.
//
// Default is false, i.e. the live subvolumes will be deleted. Setting it to true will cause them
// to be left in a temporary directory after the restore.
//
func WithKeepLiveSubvolumes (keep bool) BtrfsOption {

   return func (btrfs *Btrfs) error {
      btrfs.keepLive = keep
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithLiveDir(…) as parameter to NewBtrfs(…) to set the source subvolume directory.
//
// The directory has to be specified as a relative path to the filesystem's root directory. Its
// existence will not be checked.
//
// If no directory is set (or it is set to an empty string), the constructor will set it to
// "@live". To actually set it to the filesystem's root directory, it must be set to ".".
//
func WithLiveDir (dir string) BtrfsOption {

   return func (btrfs *Btrfs) error {
      btrfs.liveDir = dir
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithSnapshotDir(…) as parameter to NewBtrfs(…) to set the snapshot base directory.
//
// The directory has to be specified as a relative path to the filesystem's root directory. It will
// be created if necessary. This must be the snapshot base directory, without any snapshot specific
// part.
//
// If no directory is set (or it is set to an empty string), the constructor will set it to
// "@snap". To actually set it to the filesystem's root directory, it must be set to ".".
//
func WithSnapshotDir (dir string) BtrfsOption {

   return func (btrfs *Btrfs) error {
      btrfs.snapDir = dir
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithWritable(…) as parameter to NewBtrfs(…) to set snapshot writability.
//
// If the parameter is true, snapshots will be writable, otherwise they will be created read-only
// (the default).
//
func WithWritable (rw bool) BtrfsOption {

   return func (btrfs *Btrfs) error {
      btrfs.rw = rw
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithSnapshotFilters(…) as parameter to NewBtrfs(…) to set the snapshot filter functions.
//
// Snapshot filters are used to filter the snapshot list, e.g. when deciding which ones to delete.
//
// Filters will be added in the order they are specified. Using this option multiple times is
// allowed, filters will be appended to the existing list.
//
func WithSnapshotFilters (filters ... SnapshotFilter) BtrfsOption {

   return func (btrfs *Btrfs) error {
      btrfs.ssFilters = append (btrfs.ssFilters, filters ...)
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithSubvolumeFilters(…) as parameter to NewBtrfs(…) to set the subvolume filter functions.
//
// Subvolume filters are used to decide which subvolumes to snapshot or restore.
//
// Filters will be added in the order they are specified. Using this option multiple times is
// allowed, filters will be appended to the existing list.
//
func WithSubvolumeFilters (filters ... SubvolumeFilter) BtrfsOption {

   return func (btrfs *Btrfs) error {
      btrfs.svFilters = append (btrfs.svFilters, filters ...)
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The MountRequired() method is required to implement the Snapshottable interface. This one will
// always return true, indicating that a Btrfs filesystem requires mounting before anything can be
// done.
//
func (btrfs *Btrfs) MountRequired () bool {

   return true

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// See fs.WithDevices(…).
//
func WithDevices (devices ... string) BtrfsOption {

   return func (btrfs *Btrfs) error {
      btrfs.fsOptions = append (btrfs.fsOptions, fs.WithDevices (devices ...))
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// See fs.WithLabel(…).
//
func WithLabel (label string) BtrfsOption {

   return func (btrfs *Btrfs) error {
      btrfs.fsOptions = append (btrfs.fsOptions, fs.WithLabel (label))
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// See fs.WithMounter(…).
//
func WithMounter (mounter mounter.Mounter) BtrfsOption {

   return func (btrfs *Btrfs) error {
      btrfs.fsOptions = append (btrfs.fsOptions, fs.WithMounter (mounter))
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// See fs.WithPathUtils(…).
//
func WithPathUtils (putils pathutils.PathUtils) BtrfsOption {

   return func (btrfs *Btrfs) error {
      btrfs.fsOptions = append (btrfs.fsOptions, fs.WithPathUtils (putils))
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// See fs.WithUUID(…).
//
func WithUUID (uuid string) BtrfsOption {

   return func (btrfs *Btrfs) error {
      btrfs.fsOptions = append (btrfs.fsOptions, fs.WithUUID (uuid))
      return nil
   }

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————