// The SubvolumeData type.

package btrfs

//—————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "path/filepath"

   "snot/lib/snapshots"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// A SubvolumeData struct contains all data from the btrfs.SnapshotData (embedded), plus additional
// information about the Btrfs subvolume.
//
type SubvolumeData struct {

   SnapshotData

   Path           string      // systems/main
   VP             string

   Name           string      // root
   VN             string

   FullName       string      // systems/main/root
   VF             string

   LivePath       string      // /mnt/point/volumes/@live/systems/main
   LP             string

   SnapPath       string      // /mnt/point/volumes/@snap/1234567890/systems/main
   SP             string

   LiveFull       string      // /mnt/point/volumes/@live/systems/main/root
   LF             string

   SnapFull       string      // /mnt/point/volumes/@snap/1234567890/systems/main/root
   SF             string

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Data(…) method returns the SubvolumeData for the instance and the specified snapshot.
//
func (sv *subvolume) Data (data snapshots.SnapshotData) SubvolumeData {

   sd := sv.btrfs.SnapshotData (data)
   lp := filepath.Join (sd.LiveDir, sv.path)
   sp := filepath.Join (sd.SnapDir, sv.path)
   lf := filepath.Join (sd.LiveDir, sv.svol)
   sf := filepath.Join (sd.SnapDir, sv.svol)

   return SubvolumeData {

      SnapshotData   : sd,

      Path           : sv.path,
      VP             : sv.path,

      Name           : sv.name,
      VN             : sv.name,

      FullName       : sv.svol,
      VF             : sv.svol,

      LivePath       : lp,
      LP             : lp,

      SnapPath       : sp,
      SP             : sp,

      LiveFull       : lf,
      LF             : lf,

      SnapFull       : sf,
      SF             : sf,

   }

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————