// The MountableFS interface.

package fs

//—————————————————————————————————————————————————————————————————————————————————————————————————

import "snot/lib/pathutils"

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The MountableFS interface matches the methods of the Filesystem type.
//
// See the Filesystem documentation for details on the interface's methods.
//
type MountableFS interface {

   Devices    () [] string
   FSType     ()    string
   Label      ()    string
   MountPoint ()    string
   PathUtils  ()    pathutils.PathUtils
   UUID       ()    string

   Mount  (path string, options ... string) error
   Umount ()                                error

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————