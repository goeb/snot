// The fs package provides a general Filesystem type to be embedded in specific filesystem types.
//
package fs

//—————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "regexp"
   "strings"
   "sync"

   "snot/lib/fs/mounter"
   "snot/lib/pathutils"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Filesystem type represents a single filesystem and its properties. This type also supports
// mounting and unmounting the filesystem it represents, a Mounter and PathUtils implementation is
// required for this.
//
// NewFilesystem(…) must be used to create new instances of this type. Note that all properties
// have to be set manually, there is no detection mechanism whatsoever (e.g. to get the filesystem
// properties based on the UUID/label/device).
//
type Filesystem struct {

   devices        [] string
   fstype         string
   label          string
   lock           sync.Mutex
   mounter        mounter.Mounter
   mountOptions   MountOptionsCallback
   mountPoint     string
   pathUtils      pathutils.PathUtils
   uuid           string

}

// The FilesystemOption type is used for the constructor parameters, see NewFilesystem(…).
//
type FilesystemOption func (*Filesystem) error

// The MountOptionsCallback type is used to allow a filesystem implementation to modify the mount
// options.
//
type MountOptionsCallback func (options ... string) ([] string, error)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Check that the Filesystem type implements the MountableFS interface (compile-time check).
//
var _ MountableFS = (*Filesystem) (nil)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Regular expression matching the UUID format (the UUID may be technically invalid, though, any
// random UUID-like grouped hexadecimal string will match).
//
var validUUID = regexp.MustCompile (`^[0-9a-f]{4}(?:[0-9a-f]{4}-){4}[0-9a-f]{12}$`)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The NewFilesystem(…) function creates a new Filesystem instance.
//
// Available options are:
//
//    * WithDevices              (…) - Set the device(s) of the filesystem.
//    * WithFSType               (…) - Set the filesystem type.
//    * WithLabel                (…) - Set the filesystem label.
//    * WithMounter              (…) - Set the Mounter instance.
//    * WithMountOptionsCallback (…) - Set the mount options callback function.
//    * WithPathUtils            (…) - Set the PathUtils instance.
//    * WithUUID                 (…) - Set the filesystem UUID.
//
// See the individual option functions for more details.
//
// If no Mounter is set by an option, it will be set to a new DefaultMounter instance. If no
// PathUtils implementation is set by an options a new DefaultPathUtils instance will be used.
//
// Unless stated otherwise, when using the same option multiple times the last one will override
// preceding options.
//
// NewFilesystem(…) returns the instance on success (error will be nil), or nil and the error on
// failure.
//
func NewFilesystem (options ... FilesystemOption) (*Filesystem, error) {

   fs := &Filesystem {
      devices : make ([] string, 0),
      lock    : sync.Mutex {},
   }

   var err error

   for _, option := range options {
      if option == nil {
         continue
      }
      if err = option (fs) ; err != nil {
         return nil, err
      }
   }

   if fs.mounter == nil {
      fs.mounter = mounter.NewDefaultMounter ()
   }

   if fs.pathUtils == nil {
      fs.pathUtils = pathutils.NewDefaultPathUtils ()
   }

   return fs, nil

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithDevices(…) as parameter to NewFilesystem(…) to set the list of devices of the
// filesystem.
//
// Parameters are strings, each one specifying one of the devices the filesystem is located on.
// Calling WithDevices(…) without parameters is valid. All specified devices will be added to the
// device list (in the order they are listed), using the WithDevices(…) option multiple times for
// the same filesystem is allowed, devices will be appended to the list. The devices will be added
// as specified without any further checks or modifications.
//
func WithDevices (devices ... string) FilesystemOption {

   return func (fs *Filesystem) error {
      fs.devices = append (fs.devices, devices ...)
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithFSType(…) as parameter to NewFilesystem(…) to set the filesystem type.
//
// The parameter is the type of the filesystem (as used with the mount command). If not set, it
// will default to an empty string. The specified type will be used without any validation or
// modification.
//
func WithFSType (fstype string) FilesystemOption {

   return func (fs *Filesystem) error {
      fs.fstype = fstype
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithLabel(…) as parameter to NewFilesystem(…) to set the filesystem label.
//
// The parameter is the label of the filesystem. If not set, it will default to an empty string.
// The specified label will be used without any validation or modification.
//
func WithLabel (label string) FilesystemOption {

   return func (fs *Filesystem) error {
      fs.label = label
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithMounter(…) as parameter to NewFilesystem(…) to set the Mounter instance for the
// filesystem.
//
func WithMounter (mounter mounter.Mounter) FilesystemOption {

   return func (fs *Filesystem) error {
      fs.mounter = mounter
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithMountOptionsCallback(…) as parameter to NewFilesystem(…) to set the callback used to
// modify the mount options for Mount(…).
//
// The function will receive the mount options as passed to the Mount(…) method as parameters, and
// must return the list of parameters to actually use, with options modified, added and/or removed
// as required by the specific filesystem implementation. The callback function may also return an
// error, in which case Mount(…) will fail.
//
// If this is option is not used the mount options will be left unaltered when Mount(…) is called.
//
func WithMountOptionsCallback (callback MountOptionsCallback) FilesystemOption {

   return func (fs *Filesystem) error {
      fs.mountOptions = callback
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithPathUtils(…) as parameter to NewFilesystem(…) to set the PathUtils backend for the
// instance.
//
func WithPathUtils (pu pathutils.PathUtils) FilesystemOption {

   return func (fs *Filesystem) error {
      fs.pathUtils = pu
      return nil
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Use WithUUID(…) as parameter to NewFilesystem(…) to set the UUID of the filesystem.
//
// The parameter is the UUID of the filesystem. If not set, it will default to an empty string. The
// specified UUID must be in the usual UUID format (i.e. hex string, 8-4-4-4-12), but it will not
// be checked for validity (i.e. correct UUID version/variant bits). If the UUID is not in the
// correct format, the option function (and thus the NewFilesystem(…) constructor) will return an
// InvalidUUIDError.
//
// Note: Letters in the UUID will be converted to lowercase.
//
func WithUUID (uuid string) FilesystemOption {

   return func (fs *Filesystem) error {

      uuidLower := strings.ToLower (uuid)

      if ! validUUID.MatchString (uuidLower) {
         return InvalidUUIDError { uuid }
      }

      fs.uuid = uuidLower

      return nil

   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Devices() method returns the devices of this filesystem (set by WithDevices(…)).
//
func (fs *Filesystem) Devices () [] string {

   fs.lock.Lock ()
   defer fs.lock.Unlock ()

   return fs.devices

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The FSType() method returns the filesystem type (set by WithFSType(…)).
//
func (fs *Filesystem) FSType () string {

   fs.lock.Lock ()
   defer fs.lock.Unlock ()

   return fs.fstype

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Label() method returns the label of this filesystem (set by WithLabel(…)).
//
func (fs *Filesystem) Label () string {

   fs.lock.Lock ()
   defer fs.lock.Unlock ()

   return fs.label

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The MountPoint() method returns the mount point of this filesystem if Mount(…) has been called
// (and Umount(…) hasn't), otherwise an empty string. The path returned will be and absolute and
// resolved path.
//
func (fs *Filesystem) MountPoint () string {

   fs.lock.Lock ()
   defer fs.lock.Unlock ()

   return fs.mountPoint

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The PathUtils() method returns the PathUtils instance of this filesystem (set by
// WithPathUtils(…) or automatically by the constructor, see NewFilesystem(…)).
//
func (fs *Filesystem) PathUtils () pathutils.PathUtils {

   fs.lock.Lock ()
   defer fs.lock.Unlock ()

   return fs.pathUtils

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The UUID() method returns the filesystem's UUID (set by WithUUID(…)).
//
func (fs *Filesystem) UUID () string {

   fs.lock.Lock ()
   defer fs.lock.Unlock ()

   return fs.uuid

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————