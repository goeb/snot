// The mounter package defines the Mounter and MounterFactory interfaces and provides a default
// implementation for both.
//
package mounter

// A Mounter implementation provides methods to mount and unmount a filesystem.
//
type Mounter interface {

   // The Mount(…) method mounts the filesystem of the specified type located on the specified
   // device at the path with the specified options and flags. In case of an error the error must
   // be returned, otherwise nil.
   //
   Mount (device, path, fstype string, flags uint, options ... string) error

   // The Umount(…) method unmounts the filesystem currently mounted at the specified path with the
   // specified flags. It must return the error in case of failure, or nil on success.
   //
   Umount (path string, flags int) error

}

// A MounterFactory instance creates new Mounter instances.
//
type MounterFactory interface {

   // The NewMounter() method must return a new Mounter instance every time it is called (with the
   // error being nil), or nil and the error if something went wrong.
   //
   NewMounter () (Mounter, error)

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————