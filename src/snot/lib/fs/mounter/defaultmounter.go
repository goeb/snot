// A default Mounter and MounterFactory implementation.

package mounter

//—————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "strings"
   "syscall"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The DefaultMounter type implements the Mounter interface to (un)mount a filesystem using the
// mount and umount2 syscalls.
//
type DefaultMounter struct {}

// The DefaultMounterFactory type implements the MounterFactory interface to provide a factory
// method that creates DefaultMounter instances.
//
type DefaultMounterFactory struct {}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Make sure we implement the Mounter and MounterFactory interfaces (compile time check).
//
var _ Mounter        = (*DefaultMounter       ) (nil)
var _ MounterFactory = (*DefaultMounterFactory) (nil)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The NewDefaultMounter() function returns a new DefaultMounter instance.
//
func NewDefaultMounter () *DefaultMounter {

   return &DefaultMounter {}

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Mount(…) method mounts the filesystem of the specified type located on the specified device
// at the specified path with the specified flags and options.
//
// Parameters will not be checked and used as specified. All required options have to be specified,
// there are no default options. Options will be joined with comma as separator, the filesystem has
// to support this (this is usually the case).
//
// Returns an error on failure, or nil on success.
//
func (*DefaultMounter) Mount (device, path, fstype string, flags uint, options ... string) error {

   return syscall.Mount (device, path, fstype, uintptr (flags), strings.Join (options, ","))

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Umount(…) method unmounts the mount at the specified path using the specified flags.
//
// The path and flags will not be checked or modified in any way.
//
// Returns an error on failure, or nil on success.
//
func (*DefaultMounter) Umount (path string, flags int) error {

   return syscall.Unmount (path, flags)

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The NewDefaultMounterFactory() method returns a new DefaultMounterFactory instance.
//
func NewDefaultMounterFactory () *DefaultMounterFactory {

   return &DefaultMounterFactory {}

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The NewMounter() method returns a new DefaultMounter instance.
//
func (mf *DefaultMounterFactory) NewMounter () (Mounter, error) {

   return NewDefaultMounter (), nil

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————