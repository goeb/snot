// Mount(…) and Umount(…) methods for the Filesystem type.

package fs

//—————————————————————————————————————————————————————————————————————————————————————————————————

import "snot/lib/fs/mounter"

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Mount(…) method mounts the filesystem at the specified path with the specified options.
//
// The path must exist, it will be resolved and cleaned before being used. It must not be the root
// path ("/"), the directory must be empty and it must not yet be a mount point.
//
// Options may be specified as required. If the instance has a mount options callback set (see
// WithMountOptionsCallback(…)), it will be called to modify the supplied list of mount options if
// required. After that, the following options (strings) will be automatically translated to the
// equivalent mount flags:
//
//    * noatime
//    * nodev
//    * nodiratime
//    * noexec
//    * nosuid
//    * ro
//
// The meaning is the same as for the mount command or fstab. Other mount flags are not supported
// (keep in mind that mount flags are not the same thing as mount options).
//
// Note that if the filesystem is already mounted (i.e. Mount(…) has been called already without a
// subsequent Umount(), existing mounts and mounts of external processes will not be considered),
// or if no devices are set for the filesystem, Mount(…) will do nothing and return nil. If more
// than one device is set, the first one will be used for mounting. If no filesystem type is set,
// "auto" will be used.
//
// On success, Mount(…) will return nil and the MountPoint() method will return the fully resolved
// and cleaned path to the mount point afterwards. On failure, the error will be returned, and the
// MountPoint() method will return an empty string.
//
func (fs *Filesystem) Mount (path string, options ... string) error {

   fs.lock.Lock ()
   defer fs.lock.Unlock ()

   if fs.mountPoint != "" || len (fs.devices) == 0 {
      return nil
   }

   absPath, err := fs.pathUtils.ResolvePath (path)

   if err != nil {
      return err
   } else if absPath == "/" {
      return RootMountError { fs.uuid }
   }

   mountPoint := true
   empty      := false

   if mountPoint, err = fs.pathUtils.IsMountPoint (absPath) ; err != nil { return err }
   if empty,      err = fs.pathUtils.DirIsEmpty   (absPath) ; err != nil { return err }

   if mountPoint || ! empty {
      return MountPointInUseError { UUID : fs.uuid, Dir : absPath }
   }

   opts  := [] string {}
   flags := uint (0)

   if fs.mountOptions != nil {
      if options, err = fs.mountOptions (options ...) ; err != nil {
         return err
      }
   }

   for _, option := range options {

      switch option {

         case "noatime"    : flags |= mounter.MS_NOATIME
         case "nodev"      : flags |= mounter.MS_NODEV
         case "nodiratime" : flags |= mounter.MS_NODIRATIME
         case "noexec"     : flags |= mounter.MS_NOEXEC
         case "nosuid"     : flags |= mounter.MS_NOSUID
         case "ro"         : flags |= mounter.MS_RDONLY

         default           : opts = append (opts, option)

      }

   }

   fstype := fs.fstype
   if fstype == "" {
      fstype = "auto"
   }

   if err := fs.mounter.Mount (fs.devices [0], absPath, fstype, flags, opts ...) ; err != nil {
      return err
   }

   fs.mountPoint = absPath

   return nil

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Umount() method unmounts the filesystem.
//
// If the filesystem is not mounted, i.e. if Mount(…) hasn't been called yet or it has been
// unmounted already, this method will do nothing and return nil. Otherwise it will return nil on
// success or the error on failure. After successfully unmounting the filesystem the MountPoint()
// method will return an empty string.
//
// Umount() will always set the UMOUNT_NOFOLLOW flag, custom flags are not supported!
//
// Note that a Filesystem instance will only keep track of its own mounting/unmounting, external
// processes or other instances for the same filesystem will not be considered.
//
func (fs *Filesystem) Umount () error {

   fs.lock.Lock ()
   defer fs.lock.Unlock ()

   if fs.mountPoint == "" {
      return nil
   }

   if err := fs.mounter.Umount (fs.mountPoint, mounter.UMOUNT_NOFOLLOW) ; err != nil {
      return err
   }

   fs.mountPoint = ""

   return nil

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————