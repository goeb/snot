// Error types for the fs package.

package fs

//—————————————————————————————————————————————————————————————————————————————————————————————————

import "fmt"

//—————————————————————————————————————————————————————————————————————————————————————————————————

// An InvalidUUIDError is the result of a malformed UUID.
//
type InvalidUUIDError struct {
   UUID string
}

// The Error() function returns the error message.
//
func (err InvalidUUIDError) Error () string {
   return fmt.Sprintf ("Invalid UUID: %s.", err.UUID)
}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// A MountPointInUseError occurs when trying to mount a filesystem at a directory that is not empty
// or already a mount point.
//
type MountPointInUseError struct {
   UUID string
   Dir  string
}

// The Error() function returns the error message.
//
func (err MountPointInUseError) Error () string {
   return fmt.Sprintf ("Mount point %s for filesystem %s already in use.", err.Dir, err.UUID)
}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// A RootMountError occurs when trying to mount a filesystem at the root directory (i.e. "/").
//
type RootMountError struct {
   UUID string
}

// The Error() function returns the error message.
//
func (err RootMountError) Error () string {
   return fmt.Sprintf ("Attempt to mount filesystem %s at / rejected.", err.UUID)
}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————