// The multierror package provides an error type that is a container for multiple errors.
//
// This is a minimal implementation using only a slice! Thread-safety is not guaranteed! Note that
// it can be used everywhere the error interface is expected, but then you have to type cast it to
// access the list (for example if you want to get the length).
//
// Usage:
//
//    func someFunction () error {
//       err := multiError.New ()
//       err.Append (errors.New ("I'm an error!"))
//       return err.Get ()
//    }
//
//    if err := someFunction () ; err != nil {
//       num := len (err.(multierror.MultiError))
//       fmt.Printf ("There have been %d errors.", num)
//    }
//
package multierror

//—————————————————————————————————————————————————————————————————————————————————————————————————

import "strings"

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The MultiError type provides a container for errors.
//
// This is probably the most basic implementation. The type itself is just a list, so list
// operations like append(…) or len(…), as well as indexing etc. work as with any list.
//
type MultiError [] error

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The New(…) function returns a pointer to a new MultiError slice.
//
// Parameters are optional. Every error in the parameter list will be added to the list of errors.
//
func New (err ... error) (*MultiError) {

   multiError := make (MultiError, len (err))
   multiError.Append (err ...)

   return &multiError

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Error() method returns the error messages as one string.
//
// Messages of multiple errors will be joined by newline ("\n"). The string returned will be empty
// if the error list is empty.
//
// Note: Required to satisfy the error interface, Error() can not have a pointer receiver like the
// other methods!
//
func (multiError MultiError) Error () (string) {

   errors := make ([] string, len (multiError))

   for i, err := range multiError {
      errors [i] = err.Error ()
   }

   return strings.Join (errors, "\n")

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Append(…) adds one or more errors to the end of the error list.
//
// Nil values will be skipped.
//
func (multiError *MultiError) Append (err ... error) () {

   for _, e := range err {

      if err == nil {
         continue
      }

      if m, ok := e.(MultiError) ; ok {
         *multiError = append (*multiError, m ...)
      } else {
         *multiError = append (*multiError, e    )
      }

   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Get() method returns either the (dereferenced) MultiError list, or nil if the list is empty.
//
// Any (optional) parameters will be appended to the list before the error is returned. Nil values
// will be skipped.
//
func (multiError *MultiError) Get (err ... error) (error) {

   if len (err) > 0 {
      multiError.Append (err ...)
   }

   if len (*multiError) > 0 {
      return *multiError
   }

   return nil

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————