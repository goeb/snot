// The cleaner package allows to register cleanup handlers and run them on exit or SIGINT/SIGTERM.
//
// Example (constructor parameters are optional, parameters below use the default values):
//
//    c := cleaner.New (cleaner.SIGINT (130), cleaner.SIGTERM (143))
//
//    c.Register ("handler 1", func () { fmt.Printf ("Running exit handler 1.\n") })
//    c.Register ("handler 2", func () { fmt.Printf ("Running exit handler 2.\n") })
//
//    c.Exit (0)
//
// Registered handlers will be run in reversed order (LIFO). The example above will print the
// following:
//
//    Running exit handler 2.
//    Running exit handler 1.
//
// Note that creating a cleaner instance will also automatically register SIGINT and SIGTERM signal
// handlers by default, unless disabled via the constructor, see New(…). These signal handlers will
// execute all cleanup handlers in reversed order (same as Exit(…)), and exit the program with exit
// code 130 (SIGINT) or 143 (SIGTERM). The exit codes may be configured if required, see below.
//
// To run the cleanup handlers on normal program exit, call the cleaner's Exit(…) function (instead
// of os.Exit(…), or at the end of the program).
//
// For convenience, this package also provides a package/global cleaner instance on request. See
// Instance() for details.
//
package cleaner

//—————————————————————————————————————————————————————————————————————————————————————————————————

import (

   "fmt"
   "os"
   "os/signal"
   "sync"
   "syscall"

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The cleaner type.
//
type Cleaner         struct {

   handlers          [] *Handler             // List of cleanup handlers, elements may be nil.
   lock              sync.Mutex              // For locking.
   register          map [string] int        // Mapping handler names to their index in the list.
   sigInt            int                     // Desired exit code for SIGINT, or special value < 0.
   sigTerm           int                     // As above, but for SIGTERM.

}

// The handler type. Stores a cleanup handler and the name by which it has been registered (to make
// removing the handler a bit easier).
//
type Handler         struct {

   handler           func ()                 // Handler callback function.
   name              string                  // Name of the cleanup handler.

}

// The function type used for the constructor options.
//
type CleanerOption   func (*Cleaner)

//—————————————————————————————————————————————————————————————————————————————————————————————————

var (

   instance          *Cleaner                // A global Cleaner instance for convenience.
   once              sync.Once               // To initialize the global instance on demand.

)

//—————————————————————————————————————————————————————————————————————————————————————————————————

// Constant DontExit, for configuring the signal handlers to not call os.Exit(…).
//
const DontExit       = -1

// Constant IgnoreSignal, for configuring the signal handlers to ignore a signal.
//
const IgnoreSignal   = -2

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The New() function creates a new cleaner instance.
//
// Parameters are optional, if specified they must be functions modifying the new cleaner instance
// (passed by reference as the first and only parameter), with no return value (the CleanerOption
// type). The following two functions are provided by this package:
//
//    * SIGINT  (…) - Configure the signal handler for SIGINT.
//    * SIGTERM (…) - Configure the signal handler for SIGTERM.
//
// See the functions' documentation for more details. The functions will be called in the order
// they are specified after the default initialization of the instance.
//
// Note: The constructor parameters may be nil and will be ignored in that case.
//
// By default, a signal handler for the cleaner will be installed for SIGINT and SIGTERM, if the
// application receives either of these signals the registered cleanup handlers will be run and it
// will exit with the status code 130 (SIGINT) or 143 (SIGTERM). Signal handlers can be disabled,
// and their exit code may be changed using the SIGINT or SIGTERM functions, see below.
//
// Note: Multiple independent cleaner instances may be created. When multiple cleaners are created,
// calling the Exit(…) function of one of them only executes the cleanup handlers registered with
// its instance. However, on SIGINT (or SIGTERM) all installed signal handlers will be called for
// all instances (in no specific order), and if calling os.Exit(…) is enabled for some of them the
// first one to finish executing its cleanup handlers will call it! It is recommended to only use
// one cleaner instance in the program. See the Instance() function below for a convenient way to
// get a "global" cleaner instance.
//
func New (options ... CleanerOption) (*Cleaner) {

   cleaner := &Cleaner {
      lock     : sync.Mutex {},
      handlers : make ([] *Handler, 0),
      register : make (map [string] int),
      sigInt   : 130,
      sigTerm  : 143,
   }

   for _, option := range options {
      if (option != nil) {
         option (cleaner)
      }
   }

   signalChannel := make (chan os.Signal, 2)
   signal.Notify (signalChannel, syscall.SIGTERM, syscall.SIGINT)

   go func () {

      exitCode := 0

      for {

         signal := <- signalChannel

         if signal == syscall.SIGINT  && cleaner.sigInt  != IgnoreSignal {
            exitCode = cleaner.sigInt
            break
         }

         if signal == syscall.SIGTERM && cleaner.sigTerm != IgnoreSignal {
            exitCode = cleaner.sigTerm
            break
         }

      }

      cleaner.runHandlers ()

      if exitCode >= 0 {
         os.Exit (exitCode)
      }

   } ()

   return cleaner

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// SIGINT may be used as a parameter to New(…) to define the behaviour on SIGINT.
//
// The exitCode parameter may be an exit code to use when exiting from the SIGINT signal handler,
// an integer between 0 and 255, or one of the constants DontExit or IgnoreSignal. DontExit will
// cause the signal handler to not call os.Exit(…) after running the cleanup handlers. IgnoreSignal
// will cause the signal handler to ignore the SIGINT signal (note that the signal handler will
// still be installed when the instance is created, but SIGINT will not cause the cleanup handlers
// to be executed). The default value is 130.
//
// A usage example for SIGINT(…) can be found at the top of this documentation.
//
func SIGINT (exitCode int) (CleanerOption) {

   return func (cleaner *Cleaner) () {
      cleaner.sigInt = exitCode
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// SIGTERM may be used as a parameter to New(…) to define the behaviour on SIGTERM.
//
// This is basically the same as SIGINT, just for the SIGTERM signal. See SIGINT(…) above for more
// details. The default value / exit code for SIGTERM is 143.
//
func SIGTERM (exitCode int) (CleanerOption) {

   return func (cleaner *Cleaner) () {
      cleaner.sigTerm = exitCode
   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Instance() function provides access to a "global" cleaner instance.
//
// The instance will be created on demand, i.e. when Instance() is called for the first time, with
// the default settings. Once created, Instance() will always return the same cleaner instance.
// This is thread-safe.
//
func Instance () (*Cleaner) {

   once.Do (func () {
      instance = New ()
   })

   return instance

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Lock() function locks the cleaner instance.
//
// It may be used together with the UnlockedRegister(…) and/or the UnlockedUnregister(…) function
// if the caller wants to take care of locking itself. Register(…) and Unregister(…) will perform
// the locking themselves and Lock() must not be used with these!
//
func (cleaner *Cleaner) Lock () {
   cleaner.lock.Lock ()
}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Unlock() function unlocks the cleaner instance. See Lock() for more details.
//
func (cleaner *Cleaner) Unlock () {
   cleaner.lock.Unlock ()
}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Register(…) function registers a new cleanup handler.
//
// The specified name must be unique among all cleanup handlers, if a handler with the same name
// has already been registered, this function will not do anything and return false. Otherwise the
// handler will be registered and the function will return true. Register(…) is thread-safe, see
// UnlockedRegister(…) for a version that does not do any locking by itself.
//
func (cleaner *Cleaner) Register (name string, cleanupHandler func ()) (bool) {

   cleaner.Lock ()
   defer cleaner.Unlock ()

   return cleaner.UnlockedRegister (name, cleanupHandler)

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The UnlockedRegister(…) function registers a new cleanup handler without locking the cleaner
// instance. See Register(…) for more details. See also the Lock() and Unlock() functions.
//
func (cleaner *Cleaner) UnlockedRegister (name string, cleanupHandler func ()) (bool) {

   if _, exists := cleaner.register [name] ; exists {
      return false
   }

   handler := &Handler { name : name, handler : cleanupHandler }

   cleaner.register [name] = len    (cleaner.handlers)
   cleaner.handlers        = append (cleaner.handlers, handler)

   return true

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Unregister(…) function removes a registered cleanup handler.
//
// The name specifies the handler to remove (same name as used with Register(…)). If no handler
// with the specified name exists, this function will do nothing and return false. Otherwise the
// handler will be removed and true will be returned. This function is thread-safe, for a version
// that does not perform any locking by itself see UnlockedUnregister(…).
//
func (cleaner *Cleaner) Unregister (name string) (bool) {

   cleaner.lock.Lock ()
   defer cleaner.lock.Unlock ()

   return cleaner.UnlockedUnregister (name)

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The UnlockedUnregister(…) function removes a registered cleanup handler without locking the
// cleaner instance. See Unregister(…) for more details. See also the Lock() and Unlock() methods.
//
func (cleaner *Cleaner) UnlockedUnregister (name string) (bool) {

   index, exists := cleaner.register [name]

   if ! exists {
      return false
   }

   // The handler element in the handlers array will be set to nil, not deleted. This ensures that
   // any existing entry in the handler register will stay valid in regards to its handler index in
   // the handler list. It also means that the array's size will stay the same after deleting a
   // handler, i.e. the array can only get bigger.

   cleaner.handlers [index] = nil
   delete (cleaner.register, name)

   return true

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The Exit() function executes any registered handlers (in reverse registration order) and exits
// the program with the specified exit code.
//
func (cleaner *Cleaner) Exit (status int) {
   cleaner.runHandlers ()
   os.Exit (status)
}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The runHandlers() function executes all registered handlers (in LIFO order). This function is
// thread-safe. Note that the lock will not be held throughout the function, only during retrieving
// and removing the last handler. This means that it is possible that another goroutine adds more
// handlers while the cleanup handlers are being processed!
//
func (cleaner *Cleaner) runHandlers () {

   for {

      cleaner.lock.Lock ()

      numHandlers := len (cleaner.handlers)

      if numHandlers == 0 {
         cleaner.lock.Unlock ()
         break
      }

      handler          := cleaner.handlers [  numHandlers - 1]
      cleaner.handlers  = cleaner.handlers [: numHandlers - 1]

      if handler != nil {
         delete (cleaner.register, handler.name)
      }

      cleaner.lock.Unlock ()

      if handler != nil {
         runHandler (handler.handler)
      }

   }

}

//—————————————————————————————————————————————————————————————————————————————————————————————————

// The runHandler(…) function executes the specified cleanup handler.
//
// This function wraps the call to the handler in a function with a deferred recover, so any panic
// will be caught and remaining handlers will still be run. On panic, an error message will be
// printed to STDERR.
//
func runHandler (handler func ()) {

   defer func () {
      if err := recover () ; err != nil {
         fmt.Fprintf (os.Stderr, "Exit handler error: %s\n", err)
      }
   } ()

   handler ()

}

// :indentSize=3:tabSize=3:noTabs=true:mode=go:maxLineLen=99: —————————————————————————————————————