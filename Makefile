# Targets:
#
#     make snot           - Build snot, executable will be bin/snot. Default target.
#     make install        - Install snot on the system.
#     make uninstall      - Undo make install. Will also delete all config files!
#     make clean          - Remove all generated files.
#
# Documentation (requires docutils to generate the man page):
#
#     make docs           - Generate the man page.
#     make docs-install   - Install the man page, plus the README and LICENSE files.
#     make docs-uninstall - Undo make docs-uninstall.
#
# Install Arch Linux specific files:
#
#     make arch-install   - Install the Arch stuff, remove the LICENSE from docs-install.
#     make arch-uninstall - Undo make arch-install.
#
# For developers:
#
#     make run <options>  - Run snot with race detection enabled. See below.
#     make test           - Run tests.
#     make testall        - Run more tests.
#     make vet            - Run go vet for all source files.
#
# Unknown targets will be be ignored and not cause an error! This is required to support command line
# arguments for run, for example (note the dashes):
#
#     make run -- create --description something…
#
# The binary will be built as dynamically linked PIE by default. To change this, override GOFLAGS and
# GO_LDFLAGS, e.g.:
#
#     GOFLAGS='-trimpath' GO_LDFLAGS='-w -s' make

## Variables: ###############################################################################################

GO_PATH = $(shell pwd)
TMP_DIR = $(GO_PATH)/tmp
ALL_GOS = $(shell find . -type f -name '*.go')
pkglist = $(shell GOPATH="$(GO_PATH)" go list snot/lib/...)
nullvar =
a_space = $(nullvar) # <- Whitespace at the end (before this comment) won't be stripped.
a_comma = ,
ALL_PKG = $(subst $(a_space),$(a_comma),$(pkglist))

GO_LDFLAGS ?= -linkmode=external -w -s
ifndef SOURCE_DATE_EPOCH
   bldtime     = $(shell date +'%Y/%m/%d %H:%M:%S')
   GO_LDFLAGS += -X "snot/cmd/snot/common.BuildTime=$(bldtime)"
endif

export GOFLAGS ?= -buildmode=pie -trimpath
export GOPATH   = $(GO_PATH)
export TMPDIR   = $(TMP_DIR)

## Installation paths: ######################################################################################

PREFIX ?= /usr/local
SHARED ?= $(PREFIX)/share

BINDIR ?= $(DESTDIR)$(PREFIX)/bin
LIBDIR ?= $(DESTDIR)$(PREFIX)/lib

MANDIR ?= $(DESTDIR)$(SHARED)/man
SHRDIR ?= $(DESTDIR)$(SHARED)/snot
DOCDIR ?= $(DESTDIR)$(SHARED)/doc/snot

ETCDIR ?= $(DESTDIR)/etc

## Rules: ###################################################################################################

# Default target: -------------------------------------------------------------------------------------------

snot: bin/snot

# Create the temporary directory if it does not exist: ------------------------------------------------------

$(TMP_DIR):

	mkdir -p '$(TMP_DIR)'

# Build snot (executable will be placed in the bin folder): -------------------------------------------------

bin/snot: $(ALL_GOS) | $(TMP_DIR)

	go install -v -ldflags '$(GO_LDFLAGS)' snot/cmd/snot

# Install and uninstall the snot executable: ----------------------------------------------------------------

install: snot

	install -m 755 -D -s '$(GO_PATH)'/bin/snot '$(BINDIR)'/snot
	install -m 644 -D -t '$(ETCDIR)'/snot      '$(GO_PATH)'/doc/conf/*.conf

uninstall: docs-uninstall arch-uninstall

	rm  -f '$(BINDIR)'/snot
	rm -rf '$(ETCDIR)'/snot

# Generate the man page: ------------------------------------------------------------------------------------

docs: doc/snot.1.rst | $(TMP_DIR)

	rst2man doc/man/snot.1.rst >'$(TMP_DIR)'/snot.1

# Install and uninstall the man page, LICENSE and README files: ---------------------------------------------

docs-install: docs

	install -m 644 -D '$(TMP_DIR)'/snot.1  '$(MANDIR)'/man1/snot.1
	install -m 644 -D '$(GO_PATH)'/LICENSE '$(DOCDIR)'/LICENSE
	install -m 644 -D '$(GO_PATH)'/README  '$(DOCDIR)'/README

docs-uninstall:

	rm -f '$(MANDIR)'/man1/snot.1
	rm -f '$(DOCDIR)'/LICENSE
	rm -f '$(DOCDIR)'/README

	rmdir --ignore-fail-on-non-empty '$(MANDIR)'/man1 || true
	rmdir --ignore-fail-on-non-empty '$(DOCDIR)'      || true

# Install and uninstall the pacman and mkinitcpio hooks: ----------------------------------------------------

arch-install:

	install -m 644 -D '$(GO_PATH)'/arch/initcpio/snot.hook    '$(LIBDIR)'/initcpio/hooks/snot
	install -m 644 -D '$(GO_PATH)'/arch/initcpio/snot.install '$(LIBDIR)'/initcpio/install/snot
	install -m 755 -D '$(GO_PATH)'/arch/pacman/snot-pacman    '$(SHRDIR)'/snot-pacman
	install -m 644 -D '$(GO_PATH)'/arch/pacman/snot.hook      '$(ETCDIR)'/pacman.d/hooks/snot.hook

	install -m 644 -D '$(GO_PATH)'/doc/conf/create.conf       '$(ETCDIR)'/snot/create.init
	install -m 644 -D '$(GO_PATH)'/doc/conf/delete.conf       '$(ETCDIR)'/snot/delete.init
	install -m 644 -D '$(GO_PATH)'/doc/conf/list.conf         '$(ETCDIR)'/snot/list.init
	install -m 644 -D '$(GO_PATH)'/doc/conf/restore.conf      '$(ETCDIR)'/snot/restore.init

	sed                                                                          \
		-e "1i # This file is copied to the initramfs by snot's mkinitcpio hook." \
		-e "1i # Delete it to use the default config file.\n"                     \
		-i '$(ETCDIR)'/snot/*.init

	rm -f '$(DOCDIR)'/LICENSE

arch-uninstall:

	rm -f '$(LIBDIR)'/initcpio/hooks/snot
	rm -f '$(LIBDIR)'/initcpio/install/snot
	rm -f '$(SHRDIR)'/snot-pacman
	rm -f '$(ETCDIR)'/pacman.d/hooks/snot.hook
	rm -f '$(ETCDIR)'/snot/*.init

	rmdir --ignore-fail-on-non-empty '$(LIBDIR)'/initcpio/hooks   || true
	rmdir --ignore-fail-on-non-empty '$(LIBDIR)'/initcpio/install || true
	rmdir --ignore-fail-on-non-empty '$(LIBDIR)'/initcpio         || true

	rmdir --ignore-fail-on-non-empty '$(ETCDIR)'/pacman.d/hooks   || true
	rmdir --ignore-fail-on-non-empty '$(ETCDIR)'/pacman.d         || true
	rmdir --ignore-fail-on-non-empty '$(ETCDIR)'/snot             || true

	rmdir --ignore-fail-on-non-empty '$(SHRDIR)'                  || true

# Remove all that is generated by build/run/etc.: -----------------------------------------------------------

clean:

	rm -rf ./bin/ ./pkg/ ./tmp/

# Run tests: ------------------------------------------------------------------------------------------------

test: | $(TMP_DIR)

	go test -tags test -v -cover -coverprofile '$(TMP_DIR)'/coverage.txt -coverpkg $(ALL_PKG) ./...
	go tool cover -html='$(TMP_DIR)'/coverage.txt -o='$(TMP_DIR)'/coverage.html

# Run more tests: -------------------------------------------------------------------------------------------

testall: | $(TMP_DIR)

	go test -tags 'test testall' -v -cover -coverprofile '$(TMP_DIR)'/coverage.txt -coverpkg $(ALL_PKG) ./...
	go tool cover -html='$(TMP_DIR)'/coverage.txt -o='$(TMP_DIR)'/coverage.html

# Run go vet for all files: ---------------------------------------------------------------------------------

vet: | $(TMP_DIR)

	go vet ./...

# Run snot with race detection: -----------------------------------------------------------------------------

run: | $(TMP_DIR)

	go run -v -race src/snot/cmd/snot/snot.go $(filter-out $@,$(MAKECMDGOALS))

# This rule matches anything not yet covered, required to allow command line options for run: ---------------

%:
	@:

# Phony targets: --------------------------------------------------------------------------------------------

.PHONY: clean run snot test testall vet install docs docs-install arch-install

# :indentSize=3:tabSize=3:noTabs=false:mode=makefile:maxLineLen=109: ########################################
