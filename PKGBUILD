# Maintainer: Stefan Göbel < snot ʇɐ subtype ˙ de >

pkgname='snot'
pkgver='0.3'
pkgrel='1'
arch=('x86_64' 'i686')
pkgdesc='Basic Btrfs snapshot tool.'
url='https://gitlab.com/goeb/snot'
license=('GPL3')
depends=('btrfs-progs')
makedepends=('go' 'python-docutils')
backup=(
   'etc/pacman.d/hooks/snot.hook'
   'etc/snot/create.conf'
   'etc/snot/create.init'
   'etc/snot/delete.conf'
   'etc/snot/delete.init'
   'etc/snot/list.conf'
   'etc/snot/list.init'
   'etc/snot/restore.conf'
   'etc/snot/restore.init'
)

_pkgbuild_dir=${_pkgbuild_dir:-$PWD}
_source_files=(
   'arch'
   'doc'
   'src'
   'LICENSE'
   'Makefile'
   'PKGBUILD'
   'README'
)

export GOCACHE="${TMPDIR:-/tmp}/gocache/"

prepare() {

   local _dest="$srcdir/$pkgname"
   local _file=''

   mkdir -p "$_dest"

   for _file in "${_source_files[@]}" ; do
      cp -avx "$_pkgbuild_dir/$_file" "$_dest/"
   done

   cd "$_dest"
   make clean

}

build() {

   cd "$srcdir/$pkgname"

   export CGO_CPPFLAGS="${CPPFLAGS}"
   export CGO_CFLAGS="${CFLAGS}"
   export CGO_CXXFLAGS="${CXXFLAGS}"
   export CGO_LDFLAGS="${LDFLAGS}"

   make
   make docs

}

check() {

   cd "$srcdir/$pkgname"

   make testall

}

package() {

   cd "$srcdir/$pkgname"

   make DESTDIR="$pkgdir/" PREFIX="/usr" install
   make DESTDIR="$pkgdir/" PREFIX="/usr" docs-install
   make DESTDIR="$pkgdir/" PREFIX="/usr" arch-install

}

# :indentSize=3:tabSize=3:noTabs=true:mode=shellscript:maxLineLen=78: ########