=======================================================================================
snot
=======================================================================================

---------------------------------------------------------------------------------------
Create and restore Btrfs filesystem snapshots.
---------------------------------------------------------------------------------------

:Author:         Stefan Göbel <snot at subtype dot de>
:Date:           2019/03/15
:Version:        0.3
:Manual section: 1
:Manual group:   User Commands

SYNOPSIS
=======================================================================================

**snot** *<command>* *[options]* *[snapshots]*

DESCRIPTION
=======================================================================================

**snot** is a small utility to create and restore snapshots for Btrfs filesystems.
While it does require a certain filesystem layout, see *BTRFS FILESYSTEM LAYOUT* below,
it does not pollute your filesystems with hidden directories whose location can't even
be configured or put restrictions on which subvolumes to create snapshots of. **snot**
does not require a configuration file (although it has a very restricted configuration
file support, see the *CONFIGURATION FILES* section below), everything is configured on
the command line.

**snot** requires the `btrfs` executable to do the hard work, and there is nothing
special about the snapshots created. They can easily be managed manually without
**snot** if required.

**snot** requires the permission to mount filesystems, and create and delete subvolumes
and files in certain directories. In most cases, this means it has to be run as root!
**snot** will process all Btrfs filesystems it discovers, even those that are not
mounted when **snot** is run (this can be changed with the *--mounted-only* option).

Disclaimer: **snot** most likely requires root permissions, and, depending on the
command, it has to delete files, directories, subvolumes…. It should not cause any
unintentional damage, but sometimes shit happens. As stated in the license below,
"**snot** is distributed […] WITHOUT ANY WARRANTY"!

**ALWAYS MAKE SURE YOU HAVE RECENT BACKUPS OF ANY IMPORTANT DATA BEFORE USING SNOT!**

QUICK OVERVIEW
=======================================================================================

This is just a short explanation of how **snot** creates the snapshots. Check the other
sections of this man page for more information.

**snot** will first try to discover all Btrfs filesystems available on the system,
irregardless whether they're currently mounted or not. It will mount the top level
volumes of all filesystems found at a temporary location. It will then search for
subvolumes located in the `@live` folder (subvolumes in subfolders are allowed, but
nested subvolumes will be ignored). A snapshot will be created for all these subvolumes
that are not excluded, in the `@snap` folder. Restoring a snapshot works almost the
same way. That's it.

Note that if your filesystem layout matches the one descriped in this document,
**snot** does not require any configuration. However, the behaviour of **snot** can be
configured on the command line (or the config files, which basically only contain
command line options to use). The only thing that can't be configured is the fact that
nested subvolumes are not supported (though it would be possible to work around it, not
in one command, and left as an exercise to the user)!

COMMANDS
=======================================================================================

*create*

   Create a new snapshot. After successful creation of the snapshot its basic
   information will be printed, see *list* below for a description of the format.

*delete*

   Delete the specified snapshot(s). The IDs of the snapshots to delete have to be
   specified as command line arguments following the options, unless *--from* and/or
   *--until* are used to select the snapshots to delete. See the *SNAPSHOT IDS* section
   for more information.

*list*

   List all available snapshots. The list will be sorted by creation time, starting
   with the oldest snapshot. If snapshot IDs are specified on the command line the list
   will only contain matching snapshots. The output will be in the following format:

   `[12345678]  2012/12/21 12:21:12  Description.`

   It starts with the snapshot ID (in square brackets), followed by the creation time
   and the description. Fields are separated by two spaces. If no description is set, a
   dash (`-`) will be used instead.

*restore*

   Restore a snapshot. The ID of the snapshot has to be specified as command line
   argument following the options. See the *SNAPSHOT IDS* section for more information.
   If no full ID is specified and the prefix matches more than one snapshot, **snot**
   will exit with an error.

OPTIONS
=======================================================================================

--description <DESCRIPTION>   The description of the snapshot. This may contain
                              template variables, see below. Defaults to an empty
                              string.

                              Applies to *create*.

--from <TIME>                 Only include snapshots that have been created at or after
                              the specified time. See the *TIME OPTIONS* section below
                              for more information.

                              Applies to *delete* and *list*.

--ignore-errors               If set, **snot** will continue on certain non-fatal
                              errors. Using this option, especially with *restore*,
                              **IS NOT RECOMMENDED**, as it could leave the filesystem
                              in a broken state.

                              Applies to *create*, *delete*, *list* and *restore*.

--keep-orig                   Do not remove the old live subvolumes after a restore.
                              By default, they will be deleted. If this option is set,
                              they will be left in a hidden subdirectory in their
                              original location to be removed manually. This option is
                              also useful to prevent *restore* from failing if the live
                              subvolumes contain nested subvolumes.

                              Applies to *restore*.

--meta-json <FILE>            Specifies the file where the snapshot meta data will be
                              stored (one file per snapshot). It may contain template
                              variables, see below. The defaul is `<.SD>/snap.json`.

                              Applies to *create*, *delete*, *list* and *restore*.

--mounted-only                Ignore all filesystems that are not currently mounted.
                              Note that even with this option set **snot** will mount
                              all filesystems it may take snapshots from.

                              Applies to *create*, *delete*, *list* and *restore*.

--mount-options <OPTIONS>     Additional mount options to use when mounting the
                              filesystems, as a comma-separated list. Note that for
                              Btrfs filesystems the `subvol=/` option will be added
                              automatically and does not have to be specified (and if
                              you think this is wrong - which it usually isn't - take
                              a look at the *BTRFS FILESYSTEM LAYOUT* section below).
                              Defaults to an empty string.

                              Applies to *create*, *delete*, *list* and *restore*.

--no-parallel                 By default, **snot** will try to start all `btrfs`
                              processes that take the snapshots in parallel. With this
                              option set the snapshots will be created one after the
                              other.

                              Applies to *create*.

--norest-file <FILE>          The presence of this file (or directory etc.) will cause
                              a subvolume to **NOT** be restored. The filename may (and
                              most likely should) contain template variables. The
                              default value is `<.LF>.norest`, which will be expanded
                              to a file with the same name as the live subvolume plus
                              the `.norest` extension.

                              Applies *restore*.

--nosnap-file <FILE>          The presence of this file (or directory etc.) will cause
                              a subvolume to be excluded from the snapshots. The
                              filename may (and most likely should) contain template
                              variables. The default value is `<.LF>.nosnap`, which
                              will be expanded to a file with the same name as the live
                              subvolume plus the `.nosnap` extension.

                              Applies to *create*.

--no-umount                   If this is set, the filesystems mounted by **snot** will
                              not be unmounted when **snot** is done. See the options
                              *--temp-dir* and *--temp-prefix* for details on the
                              location of the mountpoints.

                              Applies to *create*, *delete*, *list* and *restore*.

--rw                          By default, snapshots will be created read-only. Using
                              this option will cause them to be created writable.

                              Applies to *create*.

--snap-dir <DIRECTORY>        The base directory of the snapshots, relative to the root
                              of the individual filesystems. This option does not
                              support template variables! The default is `@snap`. See
                              the *BTRFS FILESYSTEM LAYOUT* section for more details.

                              Applies to *create*, *delete*, *list* and *restore*.

--svol-dir <DIRECTORY>        The base directory of the live subvolumes, i.e. those
                              that snapshots will be taken from. This option does not
                              support template variables! The default is `@live`. See
                              the *BTRFS FILESYSTEM LAYOUT* section for more details.

                              Applies to *create* and *restore*.

--temp-dir <DIRECTORY>        The temporary directory in which to create the temporary
                              mount points. It default to an empty string, which means
                              the system default (usually `/tmp`) will be used.

                              Applies to *create*, *delete*, *list* and *restore*.

--temp-prefix <PREFIX>        The prefix to use for the temporary mount points,
                              defaults to `snot.`.

                              Applies to *create*, *delete*, *list* and *restore*.

--until <TIME>                Only include snapshots that have been created at or
                              before the specified time. See the *TIME OPTIONS* section
                              below for more information.

                              Applies to *delete* and *list*.

BTRFS FILESYSTEM LAYOUT
=======================================================================================

For **snot**, a flat Btrfs filesystem layout is strictly required! See the
Btrfs wiki (<https://btrfs.wiki.kernel.org/index.php/SysadminGuide#Flat>) for details.

All subvolumes that should be included in the snapshots must be located beneath a
common directory. This diretory can be set by the *--svol-dir* command line option, the
default is `@live`, specified relative to the filesystem root. Note that it is not
required that this directory is a direct child of the filesystem's root directory, and
it is also not required to be located in the top level subvolume. Subvolumes may be
organized into subfolders beneath this common directory, and that folder structure will
be reflected in the snapshot directory when snapshots are created by **snot**.

**IMPORTANT:** Only subvolumes that are direct children of the subvolume this directory
is located on will be included in the snapshots! Deeper nested subvolumes are not
supported! Creating a snapshot will still work, even if these nested subvolumes are
present. However, restoring a snapshot **WILL FAIL** if there are nested subvolumes,
because **snot** will not be able to remove the original subvolume. This behaviour may
change in a future version. Using the *--keep-orig* option will cause **snot** to not
try to delete the original subvolume, so *restore* will be able to finish without an
error, but the subvolumes have to be removed manually afterwards.

The snapshots will be created in the directory specified by the *--snap-dir* option,
which defaults to `@snap`. Again, this is specified relative to the filesystem root.
In this directory, a subdirectory will be created for every snapshot, the name will be
the Unix timestamp of the snapshot (in nanoseconds). The ID is not used because using
the timestamp makes it easier to sort these directories by snapshot creation time. The
snapshot subvolumes will be created in their snapshot's directory with the same
directory structure as in the live directory.

Note that restoring a snapshot will remove the subvolume in the live directory and
create a new writable snapshot of the snapshot subvolumes. This means the subvolume ID
of the live data will change. It is required to mount subvolumes by their path using
the `subvol=<path>` mount option instead of `subvolid=<ID>` to ensure the correct
volume will get mounted after restoring a snapshot.

An example, using non-default settings:

::

   toplevel                      The filesystem root.
   │
   └── arch                      Directory or subvolume.
       │
       ├── @live                 Option --svol-dir arch/@live.
       │   │
       │   └── host              Regular directory containing the subvolumes.
       │       │
       │       ├── home          Subvolume for /home.
       │       ├── root          … for /.
       │       └── usr           … for /usr.
       │
       └── @snap                 Option --snap-dir arch/@snap.
           │
           └── 1234567890…       Snapshot timestamp as directory name.
               │
               └── host          Same directory structure as in @live.
                   │
                   ├── home      Snapshot of arch/@live/host/home.
                   ├── root      … of arch/@live/host/root.
                   └── usr       … of arch/@live/host/usr.

To exclude subvolumes from the snapshots, either move them out of the live folder, or
use the *--nosnap-file* option. As an alternative, you can include subvolumes in the
snapshots, but exclude them from being restored with the *restore* command with the
*--norest-file* option.

Note that **snot** will search for all Btrfs filesystems available on the system, and
all will be included by default (there is the *--mounted-only* option to restrict the
search to mounted filesystems). Subvolume snapshots will be created on all filesystems.

*Tip:* If you want to include `/var` in the snapshots, make sure `/var/lib/machines` is
not a subvolume! `systemd`, in its glorious wisdom, creates this subvolume by default
if `/var` is on Btrfs. In that case, remove it and create `/var/lib/machines` as a
directory. Or just exclude `/var` from the snapshots (or from being restored).

SNAPSHOT IDS
=======================================================================================

Snapshot IDs are the eight character long hexadecimal representations of the FNV-1a
32bit hashes of the snapshots' timestamps (Unix timestamps, nanoseconds elapsed since
January 1, 1970 UTC). The IDs will be shown in the output of *list*, or for newly
created snapshots after creation. They will also be stored in the snapshot meta data.

Except for *create*, all of **snot**'s commands will filter the snapshot list by the
IDs specified after the command line options. IDs may be shortened, and will be
interpreted as prefixes in that case, matching all snapshots starting with the prefix.
Note that on the command line an arbitrary number of IDs or prefixes may be specified.

For *restore*, the IDs or prefixes must match exactly one snapshot.

If no IDs or prefixes are specified, *list* will show all available snapshots, while
*delete* will do nothing unless at least one ID or prefix is present on the command
line, or at least one of the *--from* or *--until* options is specified.

Note: When specifying snapshot IDs or prefixes and also using the *--from* and/or
*--until* option(s), only snapshots that match all filters will be included.

TEMPLATE VARIABLES
=======================================================================================

Some of the command line options support values containing template variables. Template
variables are specified as `<.NAME>`, e.g. `<.Year>` will be replaced by the year the
snapshot was created.

The following template variables are supported for the *--description* options. Note
that numeric variables are not padded with zeroes. This may change in future versions.
All these variables refer to the snapshot's creation date and time.

   * `Year`
   * `Month`         January = 1 … December = 12
   * `Day`           1 … 31
   * `Hour`          0 … 23
   * `Minute`
   * `Second`
   * `Nanosecond`
   * `Week`          ISO 8601 week number, 1 … 53
   * `DayOfWeek`     Day of the week, Sunday = 0 … Saturday = 6
   * `DayOfYear`     Day of the year, 1 … 365, or 1 … 366 for leap years
   * `UnixTime`      Seconds since January 1, 1970 UTC
   * `UnixNano`      Nanoseconds since January 1, 1970 UTC, used as snapshot directory
   * `NameOfDay`     English name of the day
   * `NameOfMonth`   English name of the month
   * `Timezone`      Abbreviated timezone name
   * `Offset`        Timezone offset in seconds
   * `OffsetStr`     Timezone offset as string, e.g. "+02:00"
   * `TS`            Same as UnixNano
   * `ID`            Snapshot ID, 32bit FNV-1a hash of the timestamp

In addition to the variables listed above, the *--meta-json* option supports the
following variables. These are specific to one filesystem. The meta data may be stored
(and by default is stored) on every filesystem that is part of the snapshot.

The following examples use the example filesystem structure shown in the filesystem
layout section above, with the temporary filesystem mount point being `/tmp/x`.

   * `UUID`          UUID of the filesystem being processed.
   * `Label`         Label of the filesystem.
   * `Type`          Filesystem type (always `btrfs`)
   * `MountPoint`    Full path to the current mount point, e.g. `/tmp/x`.
   * `MP`            Alias for `MountPoint`.
   * `LiveBase`      Relative path to the live subvolumes, e.g. `arch/@live`.
   * `LB`            Alias for `LiveBase`.
   * `SnapBase`      Relative path to the snapshots, e.g. `arch/@snap`.
   * `SB`            Alias for `SnapBase`.
   * `LiveRoot`      Full path to the live subvolumes, e.g. `/tmp/x/arch/@live`.
   * `LR`            Alias for `LiveRoot`.
   * `SnapRoot`      Full path to the snapshots, e.g. `/tmp/x/arch/@snap`.
   * `SR`            Alias for `SnapRoot`.
   * `LiveDir`       Same as `LiveRoot`.
   * `LD`            Alias for `LiveDir`.
   * `SnapDir`       Full path to current snapshot, e.g. `/tmp/x/arch/@snap/123…`.
   * `SD`            Alias for `SnapDir`.
   * `LiveRel`       Same as `LiveBase`.
   * `LL`            Alias for `LiveRel`.
   * `SnapRel`       Relative path to current snapshot, e.g. `arch/@snap/123…`.
   * `SL`            Alias for `SnapRel`.

In addition to all previous variables, the *--nosnap-file* and *--norest-file* options
support the following variables, specific to individual subvolumes that are processed.

   * `Path`          Subvolume parent relative live root, e.g. `host`. May be empty.
   * `VP`            Alias for `Path`.
   * `Name`          Basename of a subvolume, e.g. `root`.
   * `VN`            Alias for `Name`.
   * `FullName`      Full subvolume name, relative to live root, e.g. `host/root`.
   * `VF`            Alias for `FullName`.
   * `LivePath`      Full live subvolume parent, e.g. `/tmp/x/arch/@live/host`.
   * `LP`            Alias for `LivePath`.
   * `SnapPath`      Snapshot subvolume parent, e.g. `/tmp/x/arch/@snap/123…/host`.
   * `SP`            Alias for `SnapPath`.
   * `LiveFull`      Full path to live subvolume, e.g. `/tmp/x/arch/@live/host/root`.
   * `LF`            Alias for `LiveFull`.
   * `SnapFull`      Full snapshot subvolume, e.g. `/tmp/x/arch/@snap/123…/host/root`.
   * `SF`            Alias for `SnapFull`.

TIME OPTIONS
=======================================================================================

Both the *delete* and *list* commands support the two options *--from* and *--until* to
limit the affected snapshots by their creation time. The options may be used together,
if only one of them is specified no limit applies in the opposite direction (i.e. it
will just work as you would expect it).

Times are specified as `<year>-<month>-<day>-<hour>-<minute>-<second>`, although other
separators are supported, too (`(space)`, `.`, `:`, `/`, `_` and `-` are all the valid
separators). For example: `2010/11/21 13:14:15` is a valid time and it is equal to
`2010-11-21_13-14-15`. Separators must not be omitted, the `<year>` must be four digits
and all the other parts must always be specified as two digit numbers, hours are in the
range `0`-`23`.

Certain trailing parts may be omitted when specifying the time. The following is the
complete list of supported formats:

   * `<year>-<month>-<day>-<hour>-<minute>-<second>`
   * `<year>-<month>-<day>-<hour>-<minute>`
   * `<year>-<month>-<day>`
   * `<year>-<month>`
   * `<year>`

If an abbreviated time format is used with the *--from* option, the missing parts will
be filled with the smallest possible values, e.g. the time `2001-01` will be treated as
`2001-01-01 00:00:00`.

If used with *--until*, the missing parts will be filled with the highest possible (and
valid) value. For example, `2020-02` will be the same as `2020-02-29 23:59:59`.

The specified interval is a closed interval, i.e. snapshots taken at the *--from* or
*--until* times will be included.

Using the same value for both *--from* and *--until* will select all snapshots created
during this time span. For example, `--from 2018-05 --until 2018-05` will select all
snapshots created in May of 2018.

If - after the options have been parsed as described - the time specified for *--from*
is after the time specified for *--until*, snapshots that have been created before or
at the *--until* time **OR** after or at the *--from* time will be selected, snapshots
created between these two times will be omitted. Matching works as described before.
For example: `--until 2015 --from 2018` will select all snapshots created before
January 1st, 2016 and after or at January 1st, 2018.

Note: It is not possible to specify multiple time periods with one command. Empty
strings as arguments to *--from* or *--until* will be treated as if *--from* or
*--until* haven't been specified at all.

CONFIGURATION FILES
=======================================================================================

**snot** supports the following configuration files, one for each of the subcommands:

   * `/etc/snot/create.conf`  for *create*
   * `/etc/snot/delete.conf`  for *delete*
   * `/etc/snot/list.conf`    for *list*
   * `/etc/snot/restore.conf` for *restore*

These files may contain all options supported by the respective subcommand on the
command line, in the exact same format (i.e. options must start with a double dash),
one option per line. If an option requires an argument, it must be put on the next
line, too!

Empty lines in these files are ignored, as are lines starting with a `#`. Quoting or
escaping is not necessary, one line will always be interpreted as exactly one command
line parameter.

If a configuration file is present, options on the command line override the settings
in the file. However, this works only for options with arguments, if a boolean option
is set in a configuration file this setting is final.

LICENSE
=======================================================================================

Copyright © 2018-2019 Stefan Göbel < snot ʇɐ subtype ˙ de >.

**snot** is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

**snot** is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with **snot**.
If not, see <http://www.gnu.org/licenses/>.

.. :indentSize=3:tabSize=3:noTabs=true:mode=rest:maxLineLen=87: